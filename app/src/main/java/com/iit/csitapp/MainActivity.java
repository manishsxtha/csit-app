package com.iit.csitapp;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessaging;
import com.iit.csitapp.event_news.PostsFragment;
import com.iit.csitapp.home.HomeFragment;
import com.iit.csitapp.home.ProjectActivity;
import com.iit.csitapp.modelQues.ModelFragment;
import com.iit.csitapp.models.Subject;
import com.iit.csitapp.navBottomUtils.AboutUsActivity;
import com.iit.csitapp.navBottomUtils.ProfileActivity;
import com.iit.csitapp.notes.NotesFragment;
import com.iit.csitapp.questionNsolutions.QAMainFragment;
import com.iit.csitapp.semester.SemesterFragment;
import com.iit.csitapp.startup.LoginActivity;
import com.iit.csitapp.subjects.SubjectsFragment;
import com.iit.csitapp.syllabus.SyllabusFragmentV2;
import com.iit.csitapp.utils.Constants;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.SharedPreferenceHelper;
import com.iit.csitapp.utils.UniversalImageLoader;
import com.iit.csitapp.utils.UserTypes;
import com.nostra13.universalimageloader.core.ImageLoader;

import static com.iit.csitapp.utils.NavigationTabs.HOME;
import static com.iit.csitapp.utils.NavigationTabs.NOTES;
import static com.iit.csitapp.utils.NavigationTabs.POSTS;
import static com.iit.csitapp.utils.NavigationTabs.QNA;
import static com.iit.csitapp.utils.NavigationTabs.SYLLABUS;

public class MainActivity extends AppCompatActivity implements SemesterFragment.OnSemesterChange, NavigationView.OnNavigationItemSelectedListener{
    public static final String MESSAGE_PROGRESS = "message_progress";
    private static final String TAG = "MainActivity";
    public static boolean isAppRunning;
    private Context mContext = MainActivity.this;

    private boolean exitApp = false;
    private FragmentManager fragmentManager;
    private AHBottomNavigation bottomNavigation;
    private UniversalImageLoader universalImageLoader;
    private SyllabusFragmentV2 syllabusFragment;
    private FirebaseHelper firebaseHelper;

    private Toolbar toolbar;
    private HomeFragment homeFragment;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private boolean mToolBarNavigationListenerIsRegistered = false;
    private PostsFragment postsFragment;
    private SubjectsFragment subjectsNoteFragment;
    private SubjectsFragment subjectsQAFragment;
    private SubjectsFragment subjectsModelQFragment;
    private Drawable drawable;
    private Bundle postBundle;
    private int postsPageNavigate = 0;
    private SharedPreferenceHelper helper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);
        setFirstLaunchParameters();
        setupFirebase();
        initImageLoader();
        setupDrawer();

//        checkStoragePermissionNDownload();


        setupBottomNavigation();
    }




    public void setToolbar(String title) {
        getSupportActionBar().setTitle(title);

    }

    public void setToolbar(String title, boolean isBackEnabled) {
        getSupportActionBar().setTitle(title);

        // To keep states of ActionBar and ActionBarDrawerToggle synchronized,
        // when you enable on one, you disable on the other.
        // And as you may notice, the order for this operation is disable first, then enable - VERY VERY IMPORTANT.
        if (isBackEnabled) {
            //You may not want to open the drawer on swipe from the left in this case
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            // Remove hamburger
            toggle.setDrawerIndicatorEnabled(false);
            // Show back button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            // when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
            // clicks are disabled i.e. the UP button will not work.
            // We need to add a listener, as in below, so DrawerToggle will forward
            // click events to this listener.
            if (!mToolBarNavigationListenerIsRegistered) {
                toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Doesn't have to be onBackPressed
                        onBackPressed();
                        changeToNavigationDrawer();
                    }
                });

                mToolBarNavigationListenerIsRegistered = true;
            }

        }
        // So, one may think "Hmm why not simplify to:
        // .....
        // getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        // mDrawer.setDrawerIndicatorEnabled(!enable);
        // ......
        // To re-iterate, the order in which you enable and disable views IS important #dontSimplify.


    }

    public void changeToNavigationDrawer() {
        //You must regain the power of swipe for the drawer.
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);


        // Remove back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        // Show hamburger
        toggle.setHomeAsUpIndicator(drawable);
        // Remove the/any drawer toggle listener
        toggle.setToolbarNavigationClickListener(null);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        mToolBarNavigationListenerIsRegistered = false;
    }

    private void setupDrawer() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);
        drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_toggle, mContext.getTheme());
        toggle.setHomeAsUpIndicator(drawable);

        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    private void setupFirebase() {
        firebaseHelper = new FirebaseHelper(mContext);
        updateUI(firebaseHelper.getAuth().getCurrentUser());
    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser == null) {
            Log.d(TAG, "updateUI: User not signed in!");
            Intent in = new Intent(this, LoginActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(in);
            finish();
        }
    }
    private void initImageLoader() {
        UniversalImageLoader universalImageLoader = new UniversalImageLoader(mContext);
        ImageLoader.getInstance().init(universalImageLoader.getConfig());
    }

    private void setStatusBarThemeColor(int themeId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            if (themeId == Constants.THEME_RED)
                window.setStatusBarColor(Color.RED);
            else if (themeId == Constants.THEME_BLUE)
                window.setStatusBarColor(mContext.getResources().getColor(R.color.fb_blue_normal));
        }
    }

    private void setupBottomNavigation() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
        bottomNavigation = findViewById(R.id.bottom_navigation);

// Create items
        AHBottomNavigationItem item1, item2, item3, item4, item5;
        SharedPreferenceHelper sharedPreferenceHelper = SharedPreferenceHelper.getInstance(mContext);
        if (sharedPreferenceHelper.getThemeColor() == Constants.THEME_RED) {
            toolbar.setBackgroundColor(Color.RED);
            setStatusBarThemeColor(Constants.THEME_RED);
            item1 = new AHBottomNavigationItem(R.string.tab_1, R.drawable.ic_home, R.color.red_500);
            item2 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.ic_question_answer, R.color.red_500);
            item3 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.ic_syllabus, R.color.red_500);
            item4 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.ic_notes, R.color.red_500);
            item5 = new AHBottomNavigationItem(R.string.tab_5, R.drawable.ic_notifications_active_black_24dp, R.color.red_500);
            bottomNavigation.setDefaultBackgroundColor(Color.RED);
            bottomNavigation.setNotificationTextColor(Color.WHITE);
        } else if (sharedPreferenceHelper.getThemeColor() == Constants.THEME_BLUE) {
            setStatusBarThemeColor(Constants.THEME_BLUE);
            toolbar.setBackgroundColor(mContext.getResources().getColor(R.color.fb_blue_normal));
            item1 = new AHBottomNavigationItem(R.string.tab_1, R.drawable.ic_home, R.color.fb_blue_normal);
            item2 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.ic_question_answer, R.color.fb_blue_normal);
            item3 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.ic_syllabus, R.color.fb_blue_normal);
            item4 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.ic_notes, R.color.fb_blue_normal);
            item5 = new AHBottomNavigationItem(R.string.tab_5, R.drawable.ic_notifications_active_black_24dp, R.color.fb_blue_normal);
            bottomNavigation.setDefaultBackgroundColor(Color.BLUE);
            bottomNavigation.setNotificationTextColor(Color.WHITE);
        } else {
//            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            item1 = new AHBottomNavigationItem(R.string.tab_1, R.drawable.ic_home, R.color.black);
            item2 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.ic_question_answer, R.color.black);
            item3 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.ic_syllabus, R.color.black);
            item4 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.ic_notes, R.color.black);
            item5 = new AHBottomNavigationItem(R.string.tab_5, R.drawable.ic_notifications_active_black_24dp, R.color.black);
            bottomNavigation.setDefaultBackgroundColor(Color.BLACK);
            bottomNavigation.setNotificationTextColor(Color.WHITE);
        }
        
// Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);


        // Manage titles
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

        // Use colored navigation with circle reveal effect
        bottomNavigation.setColored(true);
        bottomNavigation.setUseElevation(true);
        bottomNavigation.setBehaviorTranslationEnabled(false);

// Set current item programmatically
        bottomNavigation.setCurrentItem(0);

        // Customize notification (title, background, typeface)
        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));

//        AHNotification notification = new AHNotification.Builder()
//                .setText("1")
//                .setBackgroundColor(ContextCompat.getColor(mContext, R.color.black))
//                .setTextColor(ContextCompat.getColor(mContext, R.color.red_400))
//                .build();
//        bottomNavigation.setNotification(notification, 4);


        fragmentManager = getSupportFragmentManager();
        postBundle = new Bundle();
//        syllabusFragment = new SyllabusFragment();
        syllabusFragment = new SyllabusFragmentV2();
        homeFragment = new HomeFragment();

        subjectsQAFragment = new SubjectsFragment();
        subjectsNoteFragment = new SubjectsFragment();
        subjectsModelQFragment = new SubjectsFragment();
        postsFragment = new PostsFragment();
        //set first default fragment into the framelayout
        fragmentManager
                .beginTransaction()
                .add(R.id.main_frame, homeFragment)
                .addToBackStack(mContext.getString(R.string.nav_home))
                .commit();
        // Set listeners
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                // Do something cool here...
                if (wasSelected) {
                    return true;
                }
                for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                    fragmentManager.popBackStack();
                }

                switch (position) {
                    case HOME:

                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.main_frame, homeFragment)
                                .addToBackStack(mContext.getString(R.string.nav_home))
                                .commit();
                        break;
                    case QNA:
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("type", QNA);
                        bundle1.putInt("callFrom", Constants.QA_CODE);
                        subjectsQAFragment.setArguments(bundle1);
                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.main_frame, subjectsQAFragment)
                                .addToBackStack(mContext.getString(R.string.nav_qa))
                                .commit();
                        break;
                    case SYLLABUS:

                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.main_frame, syllabusFragment)
                                .addToBackStack(mContext.getString(R.string.nav_home))
                                .commit();
                        break;
                    case NOTES:
                        Bundle bundle2 = new Bundle();
                        bundle2.putInt("type", NOTES);
                        bundle2.putInt("callFrom", Constants.NOTES_CODE);
                        subjectsNoteFragment.setArguments(bundle2);
                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.main_frame, subjectsNoteFragment)
                                .addToBackStack(mContext.getString(R.string.nav_notes))
                                .commit();
                        break;
                    case POSTS:

                        postBundle.putInt("page", postsPageNavigate);
                        postsFragment.setArguments(postBundle);
                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.main_frame, postsFragment)
                                .addToBackStack(mContext.getString(R.string.nav_home))
                                .commit();
                        break;
                    default:
                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.main_frame, homeFragment)
                                .addToBackStack(mContext.getString(R.string.nav_home))
                                .commit();
                        break;

                }
                return true;
            }
        });
    }

    public void loadQAFragment(Subject subject){
        setToolbar(mContext.getString(R.string.app_name), true);
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_frame, QAMainFragment.setInstance(subject))
                .addToBackStack(mContext.getString(R.string.nav_home))
                .commit();
    }


    public void loadNotesFragment(Subject subject) {
        setToolbar(mContext.getString(R.string.app_name), true);
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_frame, NotesFragment.setInstance(subject))
                .addToBackStack(mContext.getString(R.string.nav_home))
                .commit();
    }

    public void loadModelQFragment(Subject subject) {
        setToolbar(mContext.getString(R.string.app_name), true);
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_frame, ModelFragment.setInstance(subject))
                .addToBackStack(mContext.getString(R.string.nav_home))
                .commit();
    }
    /**
     * setting the double press back button during 2 sec to exit the app
     * the first fragment is set to the activity on start
     */
    @Override
    public void onBackPressed() {

        int fragments = getSupportFragmentManager().getBackStackEntryCount();

        if (fragments == 1) {
            if (exitApp) {
                finish();
            }
            Toast.makeText(this, "Press back again to exit.", Toast.LENGTH_SHORT).show();
            exitApp = true;
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    exitApp = false;
                }
            }, 2000);
        } else {

            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                changeToNavigationDrawer();
                getSupportFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }

    }

    @Override
    public void onClick(int page, int callFromFragment) {

        if (page == Constants.SYLLABUS_CODE)
            syllabusFragment.onClick();
        else if (page== Constants.SUBJECT_CODE)
            if (callFromFragment == Constants.NOTES_CODE) {
                subjectsNoteFragment.onClick();
            } else if (callFromFragment == Constants.QA_CODE) {
                subjectsQAFragment.onClick();
            } else {
                subjectsModelQFragment.onClick();
            }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        switch (id) {
            case R.id.nav_home:
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.main_frame, homeFragment)
                        .addToBackStack(mContext.getString(R.string.nav_home))
                        .commit();
                break;
            case R.id.nav_news:
                postsPageNavigate = 0;
                bottomNavigation.setCurrentItem(4);

                break;
            case R.id.nav_events:
                postsPageNavigate = 1;
                bottomNavigation.setCurrentItem(4);

                break;
            case R.id.nav_projects:
                startActivity(new Intent(mContext, ProjectActivity.class));
                break;
            case R.id.nav_model_q:
                Bundle bundle1 = new Bundle();
                bundle1.putInt("type", 3);
                bundle1.putInt("callFrom", Constants.QA_CODE);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.main_frame, subjectsModelQFragment)
                        .addToBackStack(mContext.getString(R.string.nav_home))
                        .commit();
                break;
            case R.id.nav_profile:

                startActivity(new Intent(mContext, ProfileActivity.class));
                break;
            case R.id.nav_about_us:

                startActivity(new Intent(mContext, AboutUsActivity.class));
                break;
            case R.id.nav_feedback:

                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "cygnusnepal@gmail.com"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "CSIT App Feedback");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(mContext, "Mail not found", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.nav_setting:

                startActivity(new Intent(mContext, SettingsActivity.class));
                break;
            case R.id.nav_invite:
                String appPackageName1 = getPackageName();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName1));
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.nav_like:
                Intent intent = null;
                try {
                    mContext.getPackageManager().getPackageInfo("com.facebook.katana", 0);
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/672758376267198/"));
                } catch (Exception e) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/iitnepal/"));
                }
                startActivity(intent);
                break;

            case R.id.nav_rate:

                Toast.makeText(mContext, "Navigating to the app store..", Toast.LENGTH_SHORT).show();
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                break;
            case R.id.nav_sign_out:

                firebaseHelper.signOut();
                break;

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public boolean hasInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            startActivity(new Intent(mContext, SettingsActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }


    private void setFirstLaunchParameters() {
        helper = SharedPreferenceHelper.getInstance(mContext);
        if (helper.getFirstLaunch()) {
            subscribeTo(FilePaths.EVENTS);
            subscribeTo(FilePaths.NEWS);

            if (helper.getUserInfo().getUser_type() == UserTypes.ADMIN)
                subscribeTo(FilePaths.REG_USERS);


        }
    }

    public void subscribeTo(String name) {
        FirebaseMessaging.getInstance().subscribeToTopic(name)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "Subscribed to " + name;
                        if (!task.isSuccessful()) {
                            msg = "Failed to subscribe to " + name;
                        } else {
                            helper.setFirstLaunch(false);
                        }

//                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

                    }
                });
    }
}

