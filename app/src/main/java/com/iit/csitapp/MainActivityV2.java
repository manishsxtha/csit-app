package com.iit.csitapp;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessaging;
import com.iit.csitapp.event_news.PostsFragment;
import com.iit.csitapp.home.HomeFragment;
import com.iit.csitapp.home.ProjectActivity;
import com.iit.csitapp.modelQues.ModelFragment;
import com.iit.csitapp.models.Subject;
import com.iit.csitapp.navBottomUtils.AboutUsActivity;
import com.iit.csitapp.navBottomUtils.ProfileActivity;
import com.iit.csitapp.notes.NotesFragment;
import com.iit.csitapp.questionNsolutions.QAMainFragment;
import com.iit.csitapp.semester.SemesterFragment;
import com.iit.csitapp.startup.LoginActivity;
import com.iit.csitapp.subjects.SubjectsFragment;
import com.iit.csitapp.syllabus.SyllabusFragmentV2;
import com.iit.csitapp.utils.Constants;
import com.iit.csitapp.utils.DexterPermissionCheck;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.SharedPreferenceHelper;
import com.iit.csitapp.utils.UniversalImageLoader;
import com.iit.csitapp.utils.UserTypes;
import com.nostra13.universalimageloader.core.ImageLoader;

import static com.iit.csitapp.utils.Constants.STORAGE_PERMISSION_CODE;
import static com.iit.csitapp.utils.NavigationTabs.HOME;
import static com.iit.csitapp.utils.NavigationTabs.NOTES;
import static com.iit.csitapp.utils.NavigationTabs.POSTS;
import static com.iit.csitapp.utils.NavigationTabs.QNA;
import static com.iit.csitapp.utils.NavigationTabs.SYLLABUS;

public class MainActivityV2 extends AppCompatActivity implements SemesterFragment.OnSemesterChange, NavigationView.OnNavigationItemSelectedListener {
    public static final String MESSAGE_PROGRESS = "message_progress";
    private static final String TAG = "MainActivity";
    public static boolean isAppRunning;
    private Context mContext = MainActivityV2.this;

    private boolean exitApp = false;
    private FragmentManager fragmentManager;
    private UniversalImageLoader universalImageLoader;
    private SyllabusFragmentV2 syllabusFragment;
    private FirebaseHelper firebaseHelper;

    private ImageView toolbar;
    private HomeFragment homeFragment;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private boolean mToolBarNavigationListenerIsRegistered = false;
    private PostsFragment postsFragment;
    private SubjectsFragment subjectsNoteFragment;
    private SubjectsFragment subjectsQAFragment;
    private SubjectsFragment subjectsModelQFragment;

    private Bundle postBundle;
    private SharedPreferenceHelper helper;
    private int selectedIndex = 0;
    private TextView nav1,nav2,nav3,nav4,nav5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout_v2);
        setFirstLaunchParameters();
        setupFirebase();
        initImageLoader();
        setupDrawer();

        setupToolbar();

        permissionCheck();
        setupCustomBottomNav();


    }

    private void permissionCheck() {
        DexterPermissionCheck permissionCheck = new DexterPermissionCheck();
        permissionCheck.permissionCheck(this, mContext);
    }


    private void setupToolbar() {
        ImageView toolbar_settings = findViewById(R.id.toolbar_settings);
        toolbar_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, SettingsActivity.class));
            }
        });
    }

    private void setupCustomBottomNav() {
        //initialize the components
        nav1 = findViewById(R.id.nav1);
        nav2 = findViewById(R.id.nav2);
        nav3 = findViewById(R.id.nav3);
        nav4 = findViewById(R.id.nav4);
        nav5 = findViewById(R.id.nav5);

        fragmentManager = getSupportFragmentManager();
        postBundle = new Bundle();
//        syllabusFragment = new SyllabusFragment();
        syllabusFragment = new SyllabusFragmentV2();
        homeFragment = new HomeFragment();

        subjectsQAFragment = new SubjectsFragment();
        subjectsNoteFragment = new SubjectsFragment();
        subjectsModelQFragment = new SubjectsFragment();
        postsFragment = new PostsFragment();
        //set first default fragment into the framelayout

        nav1.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_home_click,0,0);
        fragmentManager
                .beginTransaction()
                .add(R.id.main_frame, homeFragment)
                .addToBackStack(mContext.getString(R.string.nav_home))
                .commit();
    }

    private void checkStoragePermissionNDownload() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        } else {
            Toast.makeText(mContext, mContext.getString(R.string.error_permission), Toast.LENGTH_SHORT).show();
        }
    }


    public void setToolbar(String title) {
//        getSupportActionBar().setTitle(title);

    }

    public void setToolbar(String title, boolean isBackEnabled) {
//        getSupportActionBar().setTitle(title);

        if (isBackEnabled) {
            //You may not want to open the drawer on swipe from the left in this case
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            // Remove hamburger
            Glide.with(mContext)
                    .load(R.drawable.ic_arrow_back)
                    .apply(RequestOptions.circleCropTransform())
                    .into(toolbar);
            // Show back button

            // when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
            // clicks are disabled i.e. the UP button will not work.
            // We need to add a listener, as in below, so DrawerToggle will forward
            // click events to this listener.
            if (!mToolBarNavigationListenerIsRegistered) {
                toolbar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Doesn't have to be onBackPressed
                        onBackPressed();
                        changeToNavigationDrawer();
                    }
                });

                mToolBarNavigationListenerIsRegistered = true;
            }

        }
        // So, one may think "Hmm why not simplify to:
        // .....
        // getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        // mDrawer.setDrawerIndicatorEnabled(!enable);
        // ......
        // To re-iterate, the order in which you enable and disable views IS important #dontSimplify.


    }

    public void changeToNavigationDrawer() {
        //You must regain the power of swipe for the drawer.
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        Glide.with(mContext)
                .load(R.drawable.ic_hamburger)
                .apply(RequestOptions.circleCropTransform())
                .into(toolbar);


        // Remove the/any drawer toggle listener
        toolbar.setOnClickListener(null);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        mToolBarNavigationListenerIsRegistered = false;
    }

    private void setupDrawer() {
        toolbar = findViewById(R.id.toolbar);


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.addDrawerListener(toggle);

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    private void setupFirebase() {
        firebaseHelper = new FirebaseHelper(mContext);
        updateUI(firebaseHelper.getAuth().getCurrentUser());
    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser == null) {
            Log.d(TAG, "updateUI: User not signed in!");
            Intent in = new Intent(this, LoginActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(in);
            finish();
        }
    }

    private void initImageLoader() {
        universalImageLoader = new UniversalImageLoader(mContext);
        ImageLoader.getInstance().init(universalImageLoader.getConfig());
    }

    private void setStatusBarThemeColor(int themeId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            if (themeId == Constants.THEME_RED)
                window.setStatusBarColor(Color.RED);
            else if (themeId == Constants.THEME_BLUE)
                window.setStatusBarColor(mContext.getResources().getColor(R.color.fb_blue_normal));
        }
    }


    public void loadQAFragment(Subject subject) {
        setToolbar(mContext.getString(R.string.app_name), true);
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_frame, QAMainFragment.setInstance(subject))
                .addToBackStack(mContext.getString(R.string.nav_home))
                .commit();
    }


    public void loadNotesFragment(Subject subject) {
        setToolbar(mContext.getString(R.string.app_name), true);
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_frame, NotesFragment.setInstance(subject))
                .addToBackStack(mContext.getString(R.string.nav_home))
                .commit();
    }

    public void loadModelQFragment(Subject subject) {
        setToolbar(mContext.getString(R.string.app_name), true);
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_frame, ModelFragment.setInstance(subject))
                .addToBackStack(mContext.getString(R.string.nav_home))
                .commit();
    }

    /**
     * setting the double press back button during 2 sec to exit the app
     * the first fragment is set to the activity on start
     */
    @Override
    public void onBackPressed() {

        int fragments = getSupportFragmentManager().getBackStackEntryCount();

        if (fragments == 1) {
            if (exitApp) {
                finish();
                System.exit(0);
                return;
            }
            exitApp = true;
            Toast.makeText(this, "Press back again to exit.", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    exitApp = false;
                }
            }, 2000);
        } else {

            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                changeToNavigationDrawer();
                getSupportFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }

    }

    @Override
    public void onClick(int page, int callFromFragment) {

        if (page == Constants.SYLLABUS_CODE)
            syllabusFragment.onClick();
        else if (page == HOME) {
            homeFragment.onClick();
        } else if (page == Constants.SUBJECT_CODE)
            if (callFromFragment == Constants.NOTES_CODE) {
                subjectsNoteFragment.onClick();
            } else if (callFromFragment == Constants.QA_CODE) {
                subjectsQAFragment.onClick();
            } else {
                subjectsModelQFragment.onClick();
            }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        switch (id) {
            case R.id.nav_home:


                fragmentManager
                        .beginTransaction()
                        .replace(R.id.main_frame, homeFragment)
                        .addToBackStack(mContext.getString(R.string.nav_home))
                        .commit();
                break;
            case R.id.nav_news:
                Bundle bundle1 = new Bundle();
                bundle1.putInt("page", 0);
                postsFragment.setArguments(bundle1);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.main_frame, postsFragment)
                        .addToBackStack(mContext.getString(R.string.nav_home))
                        .commit();
                break;
            case R.id.nav_events:

                Bundle bundle2 = new Bundle();
                bundle2.putInt("page", 1);
                postsFragment.setArguments(bundle2);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.main_frame, postsFragment)
                        .addToBackStack(mContext.getString(R.string.nav_home))
                        .commit();
                break;
            case R.id.nav_projects:
                startActivity(new Intent(mContext, ProjectActivity.class));
                break;
            case R.id.nav_model_q:
                Bundle bundle3 = new Bundle();
                bundle3.putInt("type", 3);
                bundle3.putInt("callFrom", Constants.QA_CODE);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.main_frame, subjectsModelQFragment)
                        .addToBackStack(mContext.getString(R.string.nav_home))
                        .commit();
                break;
            case R.id.nav_profile:

                startActivity(new Intent(mContext, ProfileActivity.class));
                break;
            case R.id.nav_about_us:

                startActivity(new Intent(mContext, AboutUsActivity.class));
                break;
            case R.id.nav_feedback:

                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "cygnusnepal@gmail.com"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "CSIT App Feedback");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(mContext, "Mail not found", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.nav_setting:

                startActivity(new Intent(mContext, SettingsActivity.class));
                break;
            case R.id.nav_invite:
                String appPackageName1 = getPackageName();
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                // Add data to the intent, the receiving app will decide
                // what to do with it.
                share.putExtra(Intent.EXTRA_SUBJECT,
                        firebaseHelper.getAuth().getCurrentUser().getDisplayName() +
                                " has invited to you on CSIT APP");
                share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id="+appPackageName1);

                startActivity(Intent.createChooser(share, "Share link!"));

//                Intent sendIntent = new Intent();
//                sendIntent.setAction(Intent.ACTION_SEND);
//                sendIntent.putExtra(Intent.EXTRA_TEXT, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName1));
//                sendIntent.setType("text/plain");
//                startActivity(sendIntent);
                break;
            case R.id.nav_like:
                Intent intent = null;
                try {
                    mContext.getPackageManager().getPackageInfo("com.facebook.katana", 0);
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/672758376267198/"));
                } catch (Exception e) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/iitnepal/"));
                }
                startActivity(intent);
                break;

            case R.id.nav_rate:

                Toast.makeText(mContext, "Navigating to the app store..", Toast.LENGTH_SHORT).show();
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                break;
            case R.id.nav_sign_out:

                setupSignOut();
                break;

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setupSignOut() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialog);
        builder.setTitle("Confirm  Log out?");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                FirebaseHelper.getFirebaseInstance(mContext).signOut();
                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                dialog.cancel();


            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }


    public boolean hasInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            startActivity(new Intent(mContext, SettingsActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }


    private void setFirstLaunchParameters() {
        helper = SharedPreferenceHelper.getInstance(mContext);
        if (helper.getFirstLaunch()) {
            subscribeTo(FilePaths.EVENTS);
            subscribeTo(FilePaths.NEWS);

            if (helper.getUserInfo().getUser_type() == UserTypes.ADMIN)
                subscribeTo(FilePaths.REG_USERS);


        }
    }

    public void subscribeTo(String name) {
        FirebaseMessaging.getInstance().subscribeToTopic(name)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "Subscribed to " + name;
                        if (!task.isSuccessful()) {
                            msg = "Failed to subscribe to " + name;
                        } else {
                            helper.setFirstLaunch(false);
                        }
                        Log.e(TAG, "onComplete: "+msg);
//                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

                    }
                });
    }

    /**
     * CUSTOM NAV ON CLICK LISTENERS
     * custom on click listeners of navigation textviews all here
     *
     * @param view
     */
    public void onClickNavHome(View view) {


        if (selectedIndex != HOME) {
            selectedIndex = HOME;
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                fragmentManager.popBackStack();
            }

            flushDrawableToWhite();

            nav1.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_home_click,0,0);
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.main_frame, homeFragment)
                    .addToBackStack(mContext.getString(R.string.nav_home))
                    .commit();
        }

    }

    public void onClickNavQA(View view) {

        if (selectedIndex != QNA) {
            selectedIndex = QNA;
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                fragmentManager.popBackStack();
            }


            Bundle bundle1 = new Bundle();
            bundle1.putInt("type", QNA);
            bundle1.putInt("callFrom", Constants.QA_CODE);
            subjectsQAFragment.setArguments(bundle1);

            flushDrawableToWhite();

            nav2.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_question_answer_click,0,0);

            fragmentManager
                    .beginTransaction()
                    .replace(R.id.main_frame, subjectsQAFragment)
                    .addToBackStack(mContext.getString(R.string.nav_qa))
                    .commit();
        }

    }

    public void onClickNavSyllabus(View view) {
        if (selectedIndex != SYLLABUS) {
            selectedIndex = SYLLABUS;
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                fragmentManager.popBackStack();
            }

            flushDrawableToWhite();
            nav3.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_syllabus_click,0,0);
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.main_frame, syllabusFragment)
                    .addToBackStack(mContext.getString(R.string.nav_home))
                    .commit();
        }

    }

    public void onClickNavNotes(View view) {
        if (selectedIndex != NOTES) {
            selectedIndex = NOTES;
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                fragmentManager.popBackStack();
            }

            Bundle bundle2 = new Bundle();
            bundle2.putInt("type", NOTES);
            bundle2.putInt("callFrom", Constants.NOTES_CODE);
            subjectsNoteFragment.setArguments(bundle2);

            flushDrawableToWhite();

            nav4.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_notes_click,0,0);

            fragmentManager
                    .beginTransaction()
                    .replace(R.id.main_frame, subjectsNoteFragment)
                    .addToBackStack(mContext.getString(R.string.nav_notes))
                    .commit();
        }

    }

    public void onClickNavEvents(View view) {
        if (selectedIndex != POSTS) {
            selectedIndex = POSTS;
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                fragmentManager.popBackStack();
            }

            int postsPageNavigate = 0;
            postBundle.putInt("page", postsPageNavigate);
            postsFragment.setArguments(postBundle);

            flushDrawableToWhite();

            nav5.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_notifications_click,0,0);

            fragmentManager
                    .beginTransaction()
                    .replace(R.id.main_frame, postsFragment)
                    .addToBackStack(mContext.getString(R.string.nav_home))
                    .commit();
        }

    }

    private void flushDrawableToWhite() {

        nav1.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_home,0,0);
        nav2.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_question_answer,0,0);
        nav3.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_syllabus,0,0);
        nav4.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_notes,0,0);
        nav5.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_notifications,0,0);

    }
}

