package com.iit.csitapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.iit.csitapp.startup.LoginActivity;
import com.iit.csitapp.utils.Constants;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.io.File;
import java.text.DecimalFormat;

public class SettingsActivity extends AppCompatActivity {
    private SharedPreferenceHelper sharedPreferenceHelper;
    private Context mContext = SettingsActivity.this;
    private SwitchCompat switch_notify_news, switch_notify_event;
    private TextView tvCacheSize;
    private TextView tv_year;
    private String[] arr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        sharedPreferenceHelper = SharedPreferenceHelper.getInstance(mContext);
        arr = getResources().getStringArray(R.array.batches_array);
        tv_year = findViewById(R.id.tv_year);
        tv_year.setText(arr[sharedPreferenceHelper.getSyllabusFormat()]);
        setupToolbar();
        setupAppTheme();
        setupNotificationBroadcasts();
        setupCacheDelete();

        setupSignOut();

        setupBatchNewOrOld();
    }

    private void setupBatchNewOrOld() {

        findViewById(R.id.enrolled_batch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialog);
                builder.setTitle("Select your batch")
                        .setItems(R.array.batches_array, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // The 'which' argument contains the index position
                                // of the selected item


                                sharedPreferenceHelper.setSyllabusFormat(which);
                                tv_year.setText(arr[which]);

                            }
                        });
                builder.show();

            }
        });
    }

    private void setupSignOut() {
        findViewById(R.id.log_out).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialog);
                builder.setTitle("Confirm  Log out?");
                builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        FirebaseHelper.getFirebaseInstance(mContext).signOut();
                        Intent intent = new Intent(mContext, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        dialog.cancel();


                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Settings");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (sharedPreferenceHelper.getThemeColor() == Constants.THEME_RED){
            toolbar.setBackgroundColor(Color.RED);
        }else if (sharedPreferenceHelper.getThemeColor() == Constants.THEME_BLUE){
            toolbar.setBackgroundColor(mContext.getResources().getColor(R.color.fb_blue_normal));
        }
    }

    private void setupNotificationBroadcasts() {
        switch_notify_news = findViewById(R.id.switch_notify_news);
        switch_notify_event = findViewById(R.id.switch_notify_event);
        if (sharedPreferenceHelper.getNotifyNews()) {
            switch_notify_news.setChecked(true);
        } else {
            switch_notify_news.setChecked(false);
        }
        switch_notify_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hasInternetConnection()) {
                    if (switch_notify_news.isChecked()) {
                        subscribeTo(FilePaths.NEWS);
                    } else {
                        unsubscribeTo(FilePaths.NEWS);
                    }
                } else {
                    Toast.makeText(mContext, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }

            }
        });

        if (sharedPreferenceHelper.getNotifyevent()) {
            switch_notify_event.setChecked(true);
        } else {
            switch_notify_event.setChecked(false);
        }
        switch_notify_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hasInternetConnection()) {
                    if (switch_notify_event.isChecked()) {
                        subscribeTo(FilePaths.EVENTS);
                    } else {
                        unsubscribeTo(FilePaths.EVENTS);
                    }
                } else {
                    Toast.makeText(mContext, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    public void subscribeTo(String name) {
        FirebaseMessaging.getInstance().subscribeToTopic(name)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "Subscribed to " + name;
                        if (!task.isSuccessful()) {
                            msg = "Failed to subscribe to " + name;
                        } else {
                            if (name.equals(FilePaths.EVENTS)) {
                                sharedPreferenceHelper.setNotifyevent(true);
                                switch_notify_event.setChecked(true);
                            } else if (name.equals(FilePaths.NEWS)) {
                                sharedPreferenceHelper.setNotifyevent(true);
                                switch_notify_news.setChecked(true);
                            }
                        }

                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

                    }
                });
    }

    public void unsubscribeTo(String name) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(name)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "Unsubscribed from " + name;
                        if (!task.isSuccessful()) {
                            msg = "Failed to unsubscribe from " + name;
                        } else {
                            if (name.equals(FilePaths.EVENTS)) {
                                sharedPreferenceHelper.setNotifyevent(true);
                                switch_notify_event.setChecked(false);
                            } else if (name.equals(FilePaths.NEWS)) {
                                sharedPreferenceHelper.setNotifyevent(true);
                                switch_notify_news.setChecked(false);
                            }
                        }

                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

                    }
                });
    }

    private void setupAppTheme() {
        RadioGroup theme_radio_group = findViewById(R.id.theme_radio_group);
        RadioButton radio_theme_black = findViewById(R.id.radio_theme_black);
        RadioButton radio_theme_blue = findViewById(R.id.radio_theme_blue);
        RadioButton radio_theme_red = findViewById(R.id.radio_theme_red);

        if (sharedPreferenceHelper.getThemeColor() == Constants.THEME_BLACK) {
            radio_theme_black.setChecked(true);
        } else if (sharedPreferenceHelper.getThemeColor() == Constants.THEME_BLUE) {
            radio_theme_blue.setChecked(true);
        } else if (sharedPreferenceHelper.getThemeColor() == Constants.THEME_RED) {
            radio_theme_red.setChecked(true);
        }
        theme_radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                Toast.makeText(mContext, getString(R.string.theme_changed), Toast.LENGTH_SHORT).show();
                switch (checkedId) {
                    case R.id.radio_theme_black:
                        sharedPreferenceHelper.setThemeColor(Constants.THEME_BLACK);
                        break;
                    case R.id.radio_theme_blue:
                        sharedPreferenceHelper.setThemeColor(Constants.THEME_BLUE);
                        break;
                    case R.id.radio_theme_red:
                        sharedPreferenceHelper.setThemeColor(Constants.THEME_RED);
                        break;
                }
            }
        });
    }

    private void setupCacheDelete() {
        RelativeLayout btnClearCache = findViewById(R.id.btnClearCache);
        tvCacheSize = findViewById(R.id.tvCacheSize);
        initializeCache();
        btnClearCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popUpForCacheDel();
            }
        });
    }

    private void popUpForCacheDel() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        builder.setTitle("Delete cache ?");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                deleteCache(mContext);
                initializeCache();
                dialog.cancel();

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public static String readableFileSize(long size) {
        if (size <= 0) return "0 Bytes";
        final String[] units = new String[]{"Bytes", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }


    private void initializeCache() {
        long size = 0;
        size += getDirSize(this.getCacheDir());
        size += getDirSize(this.getExternalCacheDir());
        tvCacheSize.setText(readableFileSize(size));
    }

    public long getDirSize(File dir) {
        long size = 0;
        for (File file : dir.listFiles()) {
            if (file != null && file.isDirectory()) {
                size += getDirSize(file);
            } else if (file != null && file.isFile()) {
                size += file.length();
            }
        }
        return size;
    }

    public boolean hasInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
