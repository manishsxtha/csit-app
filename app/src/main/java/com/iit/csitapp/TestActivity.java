package com.iit.csitapp;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.models.Event;
import com.iit.csitapp.models.Project;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;

import java.util.List;

public class TestActivity extends AppCompatActivity {
    private static final String TAG = "TestActivity";
    private Context mContext = TestActivity.this;

    private Event event;
    private Project project;
    private List<Event> mList;


    public static boolean isAppRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        FirebaseHelper firebaseHelper = new FirebaseHelper(mContext);
        String keyId = firebaseHelper.getMyRef().child("events").push().getKey();
        event= new Event("2019/01/31" , "2019/01/31" , "https://" ,
                "https" , keyId , "sasd" , "sdas");
        firebaseHelper.getMyRef().child("events").child(keyId).setValue(event);

        String keyId1 = firebaseHelper.getMyRef().child("projects").push().getKey();
        project = new Project(keyId1 , "2019/01/31" , "asdasd" , "assa" , "https://");
        firebaseHelper.getMyRef().child("projects").child(keyId1).setValue(project);


        firebaseHelper.getMyRef().child(FilePaths.EVENTS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds :
                        dataSnapshot.getChildren()) {
                    Event event = ds.getValue(Event.class);

                    Log.d(TAG, "onDataChange: "+event.getEventImage());
                    Log.d(TAG, "onDataChange: "+event.getDate());
                    Log.d(TAG, "onDataChange: "+event.getDescription());
                    mList.add(event);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }

}