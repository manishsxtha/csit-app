package com.iit.csitapp.event_news;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.MainActivity;
import com.iit.csitapp.MainActivityV2;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Event;
import com.iit.csitapp.utils.DatabaseHelper;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

;


public class EventFragment extends Fragment {
    private static final String TAG = "Fragment";

    private View view;
    private Context mContext;
    private FirebaseHelper mFirebaseHelper;

    private List<Event> mList;
    private SwipeRefreshLayout refresh;
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private EventRecyclerAdapter adapter;
    private DatabaseHelper databaseHelper;
    private SharedPreferenceHelper preferenceHelper;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_news, container, false);
        mContext = getContext();

        databaseHelper = DatabaseHelper.getInstance(mContext);
        preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
        setupFirebase();
        setupEvents();


        return view;
    }

    private void setupFirebase() {
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
    }

    private void setupEvents() {
        mList = new ArrayList<>();

        refresh = view.findViewById(R.id.refresh);
        recyclerView = view.findViewById(R.id.recyclerView);
        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);


        recyclerView.setLayoutManager(manager);

        adapter = new EventRecyclerAdapter(mContext, mList, new EventRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Event post) {
                Intent in = new Intent(mContext, PostDetailActivity.class);
                in.putExtra(mContext.getString(R.string.calling_event_detail),post);
                startActivity(in);
            }
        });
        recyclerView.setAdapter(adapter);
        loadEvents();

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadEvents();
            }
        });
    }

    private void loadEvents() {
        refresh.setRefreshing(true);

        boolean valid;
        if (mContext instanceof MainActivity)
            valid = ((MainActivity) mContext).hasInternetConnection();
        else
            valid = ((MainActivityV2) mContext).hasInternetConnection();
        if (valid) {
            mFirebaseHelper.getMyRef().child(FilePaths.EVENTS).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    mList.clear();
                    databaseHelper.clearNewsData();
                    for (DataSnapshot ds :
                            dataSnapshot.getChildren()) {
                        Event post = ds.getValue(Event.class);

                        databaseHelper.setEvent(post);
                        mList.add(post);

                    }

                    preferenceHelper.setShareKeyNewsSize((int) dataSnapshot.getChildrenCount());

                    //sort list

                    adapter.notifyDataSetChanged();
                    refresh.setRefreshing(false);


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                    refresh.setRefreshing(false);
                    loadNewsFromDatabase();
                }
            });
        } else {

            loadNewsFromDatabase();
        }
    }

    private void loadNewsFromDatabase() {
        mList.clear();
        mList.addAll(databaseHelper.getAllEvents());

        adapter.notifyDataSetChanged();
        refresh.setRefreshing(false);
    }
}
