package com.iit.csitapp.event_news;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.MainActivity;
import com.iit.csitapp.MainActivityV2;
import com.iit.csitapp.R;
import com.iit.csitapp.models.News;
import com.iit.csitapp.utils.DatabaseHelper;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;


public class NewsFragment extends Fragment {
    private static final String TAG = "NewsFragment";

    private View view;
    private Context mContext;
    private FirebaseHelper mFirebaseHelper;
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private List<News> mNewsList;
    private SwipeRefreshLayout refresh;
    private RecyclerView newsRecyclerView;
    private LinearLayoutManager newsManager;
    private NewsRecyclerAdapter newsAdapter;
    private DatabaseHelper databaseHelper;
    private SharedPreferenceHelper preferenceHelper;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_news, container, false);
        mContext = getContext();

        databaseHelper = DatabaseHelper.getInstance(mContext);
        preferenceHelper = SharedPreferenceHelper.getInstance(mContext);

        setupFirebase();
        setupNews();

        return view;
    }

    private void setupFirebase() {
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
    }

    private void setupNews() {
        mNewsList = new ArrayList<>();

        refresh = view.findViewById(R.id.refresh);


        newsRecyclerView = view.findViewById(R.id.recyclerView);
        newsManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);


        newsRecyclerView.setLayoutManager(newsManager);

        newsAdapter = new NewsRecyclerAdapter(mContext, mNewsList, new NewsRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(News post) {
                Intent intent = new Intent(mContext, PostDetailActivity.class);
                intent.putExtra(mContext.getString(R.string.calling_news_detail),post);
                startActivity(intent);
            }
        });
        newsRecyclerView.setAdapter(newsAdapter);
        loadNews();

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadNews();
            }
        });
    }

    private void loadNews() {
        refresh.setRefreshing(true);
        boolean valid;
        if (mContext instanceof MainActivity)
            valid = ((MainActivity) mContext).hasInternetConnection();
        else
            valid = ((MainActivityV2) mContext).hasInternetConnection();
        if (valid) {
            mFirebaseHelper.getMyRef().child(FilePaths.NEWS).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    mNewsList.clear();
                    databaseHelper.clearNewsData();
                    for (DataSnapshot ds :
                            dataSnapshot.getChildren()) {
                        News post = ds.getValue(News.class);

                        databaseHelper.setNews(post);
                        mNewsList.add(post);

                    }

                    preferenceHelper.setShareKeyNewsSize((int) dataSnapshot.getChildrenCount());

                    //sort list

                    newsAdapter.notifyDataSetChanged();
                    refresh.setRefreshing(false);


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                    refresh.setRefreshing(false);
                    loadNewsFromDatabase();
                }
            });
        } else {

            loadNewsFromDatabase();
        }
    }

    private void loadNewsFromDatabase() {
        mNewsList.clear();
        mNewsList.addAll(databaseHelper.getAllNews());

        newsAdapter.notifyDataSetChanged();
        refresh.setRefreshing(false);
    }

}
