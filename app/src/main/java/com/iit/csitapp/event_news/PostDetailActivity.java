package com.iit.csitapp.event_news;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Event;
import com.iit.csitapp.models.News;
import com.iit.csitapp.models.Slider;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.UniversalImageLoader;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wang.avi.AVLoadingIndicatorView;

public class PostDetailActivity extends AppCompatActivity {

    private ImageView post_image;
    private TextView post_title, post_desc, tv_date, tv_url;
    private RelativeLayout relEventDate, relPostUrl;
    private Context mContext = PostDetailActivity.this;
    private AVLoadingIndicatorView avi;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        setupToolbar();
        setupWidgets();
        getIncomingIntent();
        initImageLoader();
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void getIncomingIntent() {
        Intent intent = getIntent();

        if (intent.hasExtra(getString(R.string.calling_news_detail))) {
            News post = intent.getParcelableExtra(getString(R.string.calling_news_detail));
            getSupportActionBar().setTitle(post.getTitle()+"");
            loadContentFromNews(post);
        } else if (intent.hasExtra(getString(R.string.calling_event_detail))) {
            Event post = intent.getParcelableExtra(getString(R.string.calling_event_detail));
            getSupportActionBar().setTitle(post.getTitle()+"");
            loadContentFromEvent(post);
        } else if (intent.hasExtra(getString(R.string.calling_notification))) {
            getSupportActionBar().setTitle("Notification");
            loadContentFromNotification(
                    intent.getStringExtra(getString(R.string.data_post_id)),
                    intent.getStringExtra(getString(R.string.data_post_type))
            );
        } else if (intent.hasExtra(getString(R.string.calling_slider_detail))) {
            getSupportActionBar().setTitle("Offer Detail");
            loadContentFromSlider(intent.getParcelableExtra(getString(R.string.calling_slider_detail)));
        } else {
            finish();
        }
    }

    private void initImageLoader() {
        UniversalImageLoader universalImageLoader = new UniversalImageLoader(mContext);
        ImageLoader.getInstance().init(universalImageLoader.getConfig());
    }

    private void loadContentFromSlider(Slider slider) {

        UniversalImageLoader.setImage(slider.getImageUrl(), post_image, null, "");
        post_title.setText(slider.getTitle());
        post_desc.setText(slider.getDesc());
    }

    private void loadContentFromNotification(String post_id, String postType) {
        avi.setVisibility(View.VISIBLE);
        startAnim();
        FirebaseHelper firebaseHelper = new FirebaseHelper(mContext);

        if (postType.equals(FilePaths.NEWS)) {

            firebaseHelper.getMyRef().child(FilePaths.NEWS).child(post_id)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.d("data", "onDataChange: " + dataSnapshot.toString());
                            if (dataSnapshot.exists()) {
                                News post = dataSnapshot.getValue(News.class);
                                UniversalImageLoader.setImage(post.getImageUrl(), post_image, null, "");
                                post_title.setText(String.format("%s", post.getTitle()));
                                post_desc.setText(String.format("%s", post.getDescription()));

                                if (URLUtil.isValidUrl(post.getNewsUrl())) {
                                    relPostUrl.setVisibility(View.VISIBLE);
                                    tv_url.setText(post.getNewsUrl());

                                    tv_url.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(post.getNewsUrl()));
                                            startActivity(browserIntent);
                                        }
                                    });
                                }
                                relEventDate.setVisibility(View.GONE);
                            }
                            stopAnim();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            stopAnim();
                        }
                    });
        }
        if (postType.equals(FilePaths.EVENTS)) {

            firebaseHelper.getMyRef().child(FilePaths.EVENTS).child(post_id)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.d("data", "onDataChange: " + dataSnapshot.toString());
                            if (dataSnapshot.exists()) {
                                Event post = dataSnapshot.getValue(Event.class);
                                UniversalImageLoader.setImage(post.getEventImage(), post_image, null, "");
                                post_title.setText(String.format("%s", post.getTitle()));
                                post_desc.setText(String.format("%s", post.getDescription()));

                                if (URLUtil.isValidUrl(post.getEventUrl())) {
                                    relPostUrl.setVisibility(View.VISIBLE);
                                    tv_url.setText(post.getEventUrl());

                                    tv_url.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(post.getEventUrl()));
                                            startActivity(browserIntent);
                                        }
                                    });
                                }
                                if (!post.getDate().isEmpty()) {
                                    relEventDate.setVisibility(View.VISIBLE);
                                    tv_date.setText(post.getDate());
                                }
                            }
                            stopAnim();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            stopAnim();
                        }
                    });
        }


    }

    private void loadContentFromNews(News post) {
        UniversalImageLoader.setImage(post.getImageUrl(), post_image, null, "");
        post_title.setText(String.format("%s", post.getTitle()));
        post_desc.setText(String.format("%s", post.getDescription()));

        if (URLUtil.isValidUrl(post.getImageUrl())) {
            relPostUrl.setVisibility(View.VISIBLE);
            tv_url.setText(post.getNewsUrl());

            tv_url.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(post.getNewsUrl()));
                    startActivity(browserIntent);
                }
            });
        }
    }

    private void loadContentFromEvent(Event post) {
        UniversalImageLoader.setImage(post.getEventImage(), post_image, null, "");
        post_title.setText(String.format("%s", post.getTitle()));
        post_desc.setText(String.format("%s", post.getDescription()));

        if (!post.getDate().isEmpty()) {
            relEventDate.setVisibility(View.VISIBLE);
            tv_date.setText(post.getDate());
        }
        if (URLUtil.isValidUrl(post.getEventUrl())) {
            relPostUrl.setVisibility(View.VISIBLE);
            tv_url.setText(post.getEventUrl());

            tv_url.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(post.getEventUrl()));
                    startActivity(browserIntent);
                }
            });
        }
    }


    private void setupWidgets() {
        post_image = findViewById(R.id.post_image);
        post_title = findViewById(R.id.post_title);
        post_desc = findViewById(R.id.post_desc);

        relEventDate = findViewById(R.id.relEventDate);
        tv_date = findViewById(R.id.tv_date);

        relPostUrl = findViewById(R.id.relPostUrl);
        tv_url = findViewById(R.id.tv_url);
        avi = findViewById(R.id.avi);
    }

    void startAnim() {
        avi.show();
        // or avi.smoothToShow();
    }

    void stopAnim() {
        avi.hide();
        // or avi.smoothToHide();
    }
}
