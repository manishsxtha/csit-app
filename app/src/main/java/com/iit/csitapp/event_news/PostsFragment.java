package com.iit.csitapp.event_news;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iit.csitapp.R;

public class PostsFragment extends Fragment {
    private static final String TAG = "PostsFragment";
    private Context mContext = getContext();
    private View view;

    public static final String[] POSTS = {
            "News",
            "Events"
    };
    private ViewPager viewPager;
    private int page=0;

    public PostsFragment() {
        super();
        setArguments(new Bundle());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_posts, container, false);

        getArgs();
        setupViewPager();

        return view;
    }

    private void getArgs() {
        Bundle bundle = getArguments();
        if (bundle!=null){
            page = bundle.getInt("page");
        }
    }

    private void setupViewPager() {

        PostPagerAdapter adapter = new PostPagerAdapter(getChildFragmentManager());

        adapter.addFragment(new NewsFragment()); //index 0
        adapter.addFragment(new EventFragment()); //index 1

        viewPager = (ViewPager) view.findViewById(R.id.htab_viewpager);
        viewPager.setAdapter(adapter);


        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.htab_tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setText(POSTS[0]);
        tabLayout.getTabAt(1).setText(POSTS[1]);

        if (page == 0){
            tabLayout.getTabAt(page).select();
        }else if (page == 1){
            tabLayout.getTabAt(page).select();
        }
    }

}
