package com.iit.csitapp.home;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.MainActivity;
import com.iit.csitapp.MainActivityV2;
import com.iit.csitapp.R;
import com.iit.csitapp.event_news.PostDetailActivity;
import com.iit.csitapp.home.adapters.EventRecyclerAdapter;
import com.iit.csitapp.home.adapters.NewsRecyclerAdapter;
import com.iit.csitapp.models.Event;
import com.iit.csitapp.models.News;
import com.iit.csitapp.models.Slider;
import com.iit.csitapp.models.Subject;
import com.iit.csitapp.semester.SemesterFragment;
import com.iit.csitapp.utils.DatabaseHelper;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.SharedPreferenceHelper;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.iit.csitapp.utils.Constants.STORAGE_PERMISSION_CODE;

public class HomeFragment extends Fragment {
    private static final String TAG = "HomeFragment";

    private View view;

    SliderPagerAdapter sliderPagerAdapter;
    private List<Slider> slider_image_list = new ArrayList<>();
    int page_position = 0;
    Timer timer;
    private ProgressBar sliderProgress;
    private ViewPager images_slider;
    private LinearLayout pages_dots;
    private TextView[] dots;
    private Context mContext;
    private List<Event> mList;
    private SwipeRefreshLayout refresh;
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private EventRecyclerAdapter adapter;
    private ProgressBar eventProgress;
    private FirebaseHelper mFirebaseHelper;
    private List<News> mNewsList;
    private ProgressBar newsProgress;
    private RecyclerView newsRecyclerView;
    private LinearLayoutManager newsManager;
    private NewsRecyclerAdapter newsAdapter;
    private ImageView tv_projects;
    private ProgressBar progressBar;
    private List<Subject> mSemSubList;
    private SemSubjectsPagerAdapter semSubjectsPagerAdapter;
    private FrameLayout frameLayout;
    private LinearLayout mainRel;
    private TextView tv_selected_sem;
    private ViewPager semSubViewPager;
    private String currentFormat;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        mContext = getContext();
//        checkStoragePermissionNDownload();
        setHelpers();
        setCurrentFormat();
        loadAds();
        

        setupToolbar();
        setupFirebase();

        mainRel = view.findViewById(R.id.mainRel);
        sliderProgress = view.findViewById(R.id.sliderProgress);
        images_slider = view.findViewById(R.id.image_page_slider);
        pages_dots = view.findViewById(R.id.image_page_dots);

        timer = new Timer();

        initProjects();
        initSlider();

        initUserProfile();
        initUserSelectedSem();


        setupEvents();
        setupNews();

        return view;
    }
    private void setCurrentFormat() {
        if (preferenceHelper.getSyllabusFormat() == 0)
            currentFormat = FilePaths.SUBJECTS;
        else
            currentFormat = FilePaths.SUBJECTS_NEW;
    }

    private void checkStoragePermissionNDownload() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        } else {
            Toast.makeText(mContext, mContext.getString(R.string.error_permission), Toast.LENGTH_SHORT).show();
        }
    }
    private void loadAds() {


        MobileAds.initialize(mContext, mContext.getString(R.string.ad_id));
        AdView mAdView = view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void initUserProfile() {
        TextView username = view.findViewById(R.id.username);
        ImageView profile_image = view.findViewById(R.id.profile_image);

        username.setText(preferenceHelper.getUserInfo().getUsername());
        Glide.with(view.getContext())
                .load(preferenceHelper.getUserInfo().getAvatar_img_link())
                .apply(RequestOptions.centerCropTransform())
                .into(profile_image);

    }

    private void initUserSelectedSem() {
        tv_selected_sem = view.findViewById(R.id.tv_selected_sem);

        frameLayout = view.findViewById(R.id.main_frame);
        Fragment semesterFragment= SemesterFragment.setInstance(0,0);
        tv_selected_sem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                frameLayout.setVisibility(View.VISIBLE);
                mainRel.setVisibility(View.GONE);
                ((MainActivityV2)mContext).getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_frame, semesterFragment)
                        .addToBackStack(mContext.getString(R.string.nav_qa))
                        .commit();
//                getChildFragmentManager()

            }
        });

        mSemSubList = new ArrayList<>();
         semSubjectsPagerAdapter = new SemSubjectsPagerAdapter(mContext, mSemSubList);
        ImageView leftNav = view.findViewById(R.id.left_nav);
        ImageView rightNav = view.findViewById(R.id.right_nav);

        progressBar = view.findViewById(R.id.progress_bar);

        SpringDotsIndicator springDotsIndicator = view.findViewById(R.id.spring_dots_indicator);
        semSubViewPager = view.findViewById(R.id.semSubViewPager);

        semSubViewPager.setAdapter(semSubjectsPagerAdapter);

// Images left navigation
        leftNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = semSubViewPager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    semSubViewPager.setCurrentItem(tab);
                } else if (tab == 0) {
                    semSubViewPager.setCurrentItem(tab);
                }
            }
        });

        // Images right navigatin
        rightNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = semSubViewPager.getCurrentItem();
                tab++;
                semSubViewPager.setCurrentItem(tab);
            }
        });


//        semSubViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        springDotsIndicator.setViewPager(semSubViewPager);

        loadSemSubjects();

    }

    private void loadSemSubjects() {
        progressBar.setVisibility(View.VISIBLE);
        tv_selected_sem.setText(String.format("Semester %s",
                preferenceHelper.getSemester()));
        mSemSubList.clear();
        semSubjectsPagerAdapter.notifyDataSetChanged();
        boolean valid;
        if (mContext instanceof MainActivity)
            valid = ((MainActivity) mContext).hasInternetConnection();
        else
            valid = ((MainActivityV2) mContext).hasInternetConnection();
        if (valid) {
            mFirebaseHelper.getMyRef().child(currentFormat).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    mSemSubList.clear();
                    if (preferenceHelper.getShareKeySubjectSize() == dataSnapshot.getChildrenCount()
                            && databaseHelper.isRemoteSubjectSizeSame((int) dataSnapshot.getChildrenCount())) {
//                    loadNewsFromDatabase
                        mSemSubList.addAll(databaseHelper.getSemesterSubjects(preferenceHelper.getSemester()));
                    } else {
                        databaseHelper.clearSubjectData();
                        for (DataSnapshot ds :
                                dataSnapshot.getChildren()) {

                            Subject subject = ds.getValue(Subject.class);
                            Log.d(TAG, "onDataChange: all subj" + subject.toString());

                            if (String.valueOf(subject.getSemester()).equals(preferenceHelper.getSemester())) {
                                mSemSubList.add(subject);
                                Log.d(TAG, "onDataChange: subjects: " + subject.toString());
                            }

                            //add to the internal database as well
                            databaseHelper.setSubject(subject);

                        }
                        preferenceHelper.setShareKeySubjectSize((int) dataSnapshot.getChildrenCount());
                    }
                    semSubjectsPagerAdapter.notifyDataSetChanged();

                    semSubViewPager.setCurrentItem(mSemSubList.size()-1);
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
                    mSemSubList.clear();
                    mSemSubList.addAll(databaseHelper.getSemesterSubjects(preferenceHelper.getSemester()));

                    semSubjectsPagerAdapter.notifyDataSetChanged();

                    semSubViewPager.setCurrentItem(mSemSubList.size()-1);
                    progressBar.setVisibility(View.GONE);
                }
            });

        }
//        else {
//            mSemSubList.clear();
//            mSemSubList.addAll(databaseHelper.getSemesterSubjects(preferenceHelper.getSemester()));
//            semSubjectsPagerAdapter.notifyDataSetChanged();
//
//            semSubViewPager.setCurrentItem(mSemSubList.size()-1);
//
//            progressBar.setVisibility(View.GONE);
//        }


    }

    private void initProjects() {
        tv_projects = view.findViewById(R.id.tv_projects);

        tv_projects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, ProjectActivity.class));
            }
        });
    }


    /**
     * HELPERS: database, shared preference
     */
    private SharedPreferenceHelper preferenceHelper;
    private DatabaseHelper databaseHelper;

    private void setHelpers() {
        preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
        databaseHelper = DatabaseHelper.getInstance(mContext);
    }


    public void initSlider() {
        sliderProgress.setVisibility(View.GONE);

        addBottomDots(0);

        slider_image_list = new ArrayList<>();
        sliderPagerAdapter = new SliderPagerAdapter(getActivity(), slider_image_list);
        images_slider.setAdapter(sliderPagerAdapter);
        images_slider.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //Add few items to slider_image_list ,this should contain url of images which should be displayed in slider
        // here i am adding few sample image links from drawable, we will replace it later

        if (((MainActivityV2) mContext).hasInternetConnection()) {
            mFirebaseHelper.getMyRef().child(FilePaths.SLIDER).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (preferenceHelper.getShareKeySliderSize() == dataSnapshot.getChildrenCount()
                            && databaseHelper.isRemoteSliderSizeSame((int) dataSnapshot.getChildrenCount())) {
                        Log.d(TAG, "onDataChange: Slider load from db");
                        loadSliderFromDatabase();
                    } else {
                        Log.d(TAG, "onDataChange: slider from net");
                        databaseHelper.clearSliderData();
                        for (DataSnapshot ds :
                                dataSnapshot.getChildren()) {
                            Slider slider = ds.getValue(Slider.class);

                            slider_image_list.add(slider);

                            databaseHelper.setSlider(slider);

                        }
                        sliderPagerAdapter.notifyDataSetChanged();
//                        images_slider.setCurrentItem(page_position, true);

                        preferenceHelper.setShareKeySliderSize((int) dataSnapshot.getChildrenCount());
                        loadSliderFromDatabase();
                    }


                    sliderProgress.setVisibility(View.VISIBLE);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    sliderProgress.setVisibility(View.VISIBLE);
                    loadSliderFromDatabase();
                }
            });
        } else {
            Log.d(TAG, "initSlider: no net load from db");
            loadSliderFromDatabase();

        }


    }

    private void loadSliderFromDatabase() {
        slider_image_list.clear();
        slider_image_list.addAll(databaseHelper.getAllSlider());

        sliderPagerAdapter.notifyDataSetChanged();
        images_slider.setCurrentItem(page_position, true);

       scheduleSlider();

    }

    public void scheduleSlider() {

        try {
            final Handler handler = new Handler();

            final Runnable update = new Runnable() {
                public void run() {
                    images_slider.setCurrentItem(page_position,true);
                    if (page_position == slider_image_list.size()) {
                        page_position = 0;
                    } else {
                        page_position = page_position + 1;
                    }

                }
            };

            timer.schedule(new TimerTask() {

                @Override
                public void run() {
                    handler.post(update);
                }
            }, 500, 4000);
        }catch (Exception e){
            Log.e(TAG, "scheduleSlider: Timer ended prematurely" );
        }


    }


    public void addBottomDots(int currentPage) {
        dots = new TextView[slider_image_list.size()];
        pages_dots.removeAllViews();
        pages_dots.setPadding(0, 0, 5, 20);
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(getContext());
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(ContextCompat.getColor(mContext, R.color.dark_grey)); // un selected
            pages_dots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(ContextCompat.getColor(mContext, R.color.white)); // selected
    }

    private void setupNews() {
        mNewsList = new ArrayList<>();

        newsProgress = view.findViewById(R.id.newsProgress);
        newsRecyclerView = view.findViewById(R.id.newsRecyclerView);
        newsManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);


        newsRecyclerView.setLayoutManager(newsManager);

        newsAdapter = new NewsRecyclerAdapter(mContext, mNewsList, new NewsRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(News post) {
                Intent intent = new Intent(mContext, PostDetailActivity.class);
                intent.putExtra(mContext.getString(R.string.calling_news_detail), post);
                startActivity(intent);
            }
        });
        newsRecyclerView.setAdapter(newsAdapter);
        loadNews();
    }

    private void loadNews() {
        newsProgress.setVisibility(View.VISIBLE);

        if (((MainActivityV2) mContext).hasInternetConnection()) {
            mFirebaseHelper.getMyRef().child(FilePaths.NEWS).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (preferenceHelper.getShareKeyNewsSize() == dataSnapshot.getChildrenCount()
                            && databaseHelper.isRemoteNewsSizeSame((int) dataSnapshot.getChildrenCount())) {
//                    loadNewsFromDatabase
                        loadNewsFromDatabase();
                    } else {
//                    load news from network
                        int i = 0;
                        databaseHelper.clearNewsData();
                        for (DataSnapshot ds :
                                dataSnapshot.getChildren()) {
                            News post = ds.getValue(News.class);

                            databaseHelper.setNews(post);

                            if (i < 3) {
                                mNewsList.add(post);
                                i++;
                            }
                        }

                        preferenceHelper.setShareKeyNewsSize((int) dataSnapshot.getChildrenCount());

                        //sort list

                        newsAdapter.notifyDataSetChanged();
                        newsProgress.setVisibility(View.GONE);
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                    loadNewsFromDatabase();
                }
            });
        } else {

            loadNewsFromDatabase();
        }
    }

    private void loadNewsFromDatabase() {
        mNewsList.addAll(databaseHelper.getAllNews());

        newsAdapter.notifyDataSetChanged();
        newsProgress.setVisibility(View.GONE);
    }

    private void setupEvents() {
        mList = new ArrayList<>();

        eventProgress = view.findViewById(R.id.eventProgress);
        recyclerView = view.findViewById(R.id.recyclerView);
        manager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);


        recyclerView.setLayoutManager(manager);

        adapter = new EventRecyclerAdapter(mContext, mList, new EventRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Event post) {
                Intent in = new Intent(mContext, PostDetailActivity.class);
                in.putExtra(mContext.getString(R.string.calling_event_detail), post);
                startActivity(in);
            }
        });
        recyclerView.setAdapter(adapter);
        loadEvents();
    }

    private void loadEvents() {
        eventProgress.setVisibility(View.VISIBLE);

        if (((MainActivityV2) mContext).hasInternetConnection()) {
            mFirebaseHelper.getMyRef().child(FilePaths.EVENTS).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (preferenceHelper.getShareKeyEventSize() == dataSnapshot.getChildrenCount()
                    && databaseHelper.isRemoteEventSizeSame((int) dataSnapshot.getChildrenCount())) {
//                    loadNewsFromDatabase
                        loadEventsFromDatabase();
                    } else {
//                    load event from network
                        int i = 0;
                        databaseHelper.clearEventsData();
                        for (DataSnapshot ds :
                                dataSnapshot.getChildren()) {
                            Event post = ds.getValue(Event.class);

                            databaseHelper.setEvent(post);

                            if (i < 3) {
                                mList.add(post);
                                i++;
                            }
                        }

                        preferenceHelper.setShareKeyEventSize((int) dataSnapshot.getChildrenCount());

                        //sort list

                        adapter.notifyDataSetChanged();
                        eventProgress.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    loadEventsFromDatabase();
                }
            });
        } else {

            loadEventsFromDatabase();
        }
    }

    private void loadEventsFromDatabase() {

        mList.addAll(databaseHelper.getAllEvents());

        adapter.notifyDataSetChanged();
        eventProgress.setVisibility(View.GONE);
    }

    private void setupFirebase() {
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
    }


    @Override
    public void onPause() {
        timer.cancel();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void setupToolbar() {
//        ((MainActivityV2) mContext).setToolbar(mContext.getString(R.string.app_name));

    }

    public void onClick() {

        mSemSubList.clear();
        semSubjectsPagerAdapter.notifyDataSetChanged();

        loadSemSubjects();
        frameLayout.setVisibility(View.GONE);
        mainRel.setVisibility(View.VISIBLE);

        ((MainActivityV2)mContext).changeToNavigationDrawer();
    }
}
