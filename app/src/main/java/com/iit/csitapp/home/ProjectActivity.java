package com.iit.csitapp.home;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.textfield.TextInputEditText;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.R;
import com.iit.csitapp.home.adapters.ProjectRecyclerAdapter;
import com.iit.csitapp.models.PotentialUser;
import com.iit.csitapp.models.Project;
import com.iit.csitapp.utils.DatabaseHelper;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.HelperUtilities;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

public class ProjectActivity extends AppCompatActivity {

    public static final String TITLE = "Projects";

    private Toolbar toolbar;
    private List<Project> mList;
    private SwipeRefreshLayout refresh;
    private RecyclerView recyclerView;
    private Context mContext = ProjectActivity.this;
    private LinearLayoutManager manager;
    private ProjectRecyclerAdapter adapter;
    private FirebaseHelper mFirebaseHelper;
    private SharedPreferenceHelper preferenceHelper;
    private DatabaseHelper databaseHelper;
    private TextInputEditText et_name,et_course_name,et_email,et_phone;
    private Button btn_send_user_form;
    private String name,email,phone,course_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);

        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
        preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
        databaseHelper = DatabaseHelper.getInstance(mContext);

        setupToolbar();
        setupProjects();

        setupUserForm();
    }

    private void setupUserForm() {
        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);
        et_phone = findViewById(R.id.et_phone);
        et_course_name = findViewById(R.id.et_course_name);
        btn_send_user_form = findViewById(R.id.btn_send_user_form);

        SharedPreferenceHelper helper=SharedPreferenceHelper.getInstance(mContext);
        et_email.setText(helper.getUserInfo().getEmail());


        btn_send_user_form.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = et_name.getText().toString();
                email = et_email.getText().toString();
                phone = et_phone.getText().toString();
                course_name = et_course_name.getText().toString();

                if (validateForm()){
                    saveUserToDatabase();
                }
            }
        });
    }

    private void saveUserToDatabase() {
        String keyId = mFirebaseHelper.getMyRef().child(FilePaths.REG_USERS).push().getKey();

        PotentialUser potentialUser = new PotentialUser(
                keyId,
                name,
                email,
                phone,
                course_name,
                false
        );
        mFirebaseHelper.getMyRef().child(FilePaths.REG_USERS)
                .child(keyId)
                .setValue(potentialUser);

    }

    private boolean validateForm() {
        if (name.isEmpty() || email.isEmpty() || phone.isEmpty()){
            et_name.setError(getString(R.string.empty_fields));
            return false;
        }
        if (!HelperUtilities.isValidEmail(email)){
            et_email.setError(getString(R.string.error_invalid_email));
        }


        return true;
    }


    private void setupProjects() {
        mList = new ArrayList<>();

//        refresh = findViewById(R.id.refresh);
        recyclerView = findViewById(R.id.recyclerView);
        manager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);


        recyclerView.setLayoutManager(manager);

        adapter = new ProjectRecyclerAdapter(mContext, mList, new ProjectRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Project post) {
                Intent intent = new Intent(mContext, ProjectDetailActivity.class);
                intent.putExtra(mContext.getString(R.string.project_detail), post);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);

        loadProjects();

//        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                loadProjects();
//            }
//        });
    }

    private void loadProjects() {
//        refresh.setRefreshing(true);

        if (hasInternetConnection()) {
            mFirebaseHelper.getMyRef().child(FilePaths.PROJECTS).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                    if (preferenceHelper.getShareKeyEventSize() == dataSnapshot.getChildrenCount()
//                            && databaseHelper.isRemoteEventSizeSame((int) dataSnapshot.getChildrenCount())) {
////                    loadNewsFromDatabase
//                        loadProjectsFromDatabase();
//                    } else {

                    databaseHelper.clearEventsData();
                    for (DataSnapshot ds :
                            dataSnapshot.getChildren()) {
                        Project post = ds.getValue(Project.class);

                        databaseHelper.setProject(post);

                        mList.add(post);

                    }

//                        preferenceHelper.setShareKeyEventSize((int) dataSnapshot.getChildrenCount());
//
//                        //sort list

                    adapter.notifyDataSetChanged();
//                    refresh.setRefreshing(false);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    loadProjectsFromDatabase();
//                    refresh.setRefreshing(false);
                }
            });
        } else {

            loadProjectsFromDatabase();
        }
    }

    private boolean hasInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private void loadProjectsFromDatabase() {

        mList.addAll(databaseHelper.getAllProjects());

        adapter.notifyDataSetChanged();
//        refresh.setRefreshing(false);
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("  ");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        showToolbarTitleOnCollapsed();
    }

    private void showToolbarTitleOnCollapsed() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(TITLE);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle("");
                    isShow = false;
                }
            }
        });
    }
}
