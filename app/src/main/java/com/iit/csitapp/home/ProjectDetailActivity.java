package com.iit.csitapp.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.iit.csitapp.R;
import com.iit.csitapp.models.Project;
import com.iit.csitapp.utils.UniversalImageLoader;

public class ProjectDetailActivity extends AppCompatActivity {
    private Context mContext = ProjectDetailActivity.this;

    private Intent in;
    private Project post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_detail);

        getIncomingIntent();
        setupToolbar();

        setupWidgets();

    }

    private void setupWidgets() {
        ImageView image = findViewById(R.id.image);
        TextView title = findViewById(R.id.title);
        TextView desc = findViewById(R.id.desc);

        UniversalImageLoader.setImage(post.getPic(), image, null, "");

        title.setText(post.getTitle());
        desc.setText(post.getDesc());
    }



    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        getSupportActionBar().setTitle(post.getTitle());

    }

    private void getIncomingIntent() {
        in = getIntent();

        if (!in.hasExtra(mContext.getString(R.string.project_detail))){
            finish();
        }

        post = in.getParcelableExtra(mContext.getString(R.string.project_detail));

    }
}
