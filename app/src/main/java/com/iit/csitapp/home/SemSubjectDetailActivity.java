package com.iit.csitapp.home;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.iit.csitapp.R;
import com.iit.csitapp.modelQues.ModelRecyclerAdapter;
import com.iit.csitapp.models.Download;
import com.iit.csitapp.models.ModelQuestion;
import com.iit.csitapp.models.Note;
import com.iit.csitapp.models.Question;
import com.iit.csitapp.models.Subject;
import com.iit.csitapp.models.Syllabus;
import com.iit.csitapp.notes.NotesRecyclerAdapter;
import com.iit.csitapp.questionNsolutions.fragments.QARecyclerAdapter;
import com.iit.csitapp.syllabus.SyllabusRecyclerAdapter;
import com.iit.csitapp.utils.BroadCastReceiverActivity;
import com.iit.csitapp.utils.DatabaseHelper;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.PDFViewerActivity;

import java.io.File;
import java.util.List;

import static com.iit.csitapp.MainActivity.MESSAGE_PROGRESS;

public class SemSubjectDetailActivity extends AppCompatActivity {

    private static final String TAG = "SemSubjectDetailActivit";
    private FirebaseHelper firebaseHelper;
    private Intent in;
    private Subject subject;
    private DatabaseHelper databaseHelper;
    private Context mContext = SemSubjectDetailActivity.this;
    private String filePath;
    private String fileName;
    private boolean isDownloadOnly;
    private ExpandableRelativeLayout expandableLayout1;
    private ExpandableRelativeLayout expandableLayout2;
    private ExpandableRelativeLayout expandableLayout3;
    private ExpandableRelativeLayout expandableLayout4;
    private ExpandableRelativeLayout expandableLayout5;
    private ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_sem);

        firebaseHelper = new FirebaseHelper(getApplicationContext());
        databaseHelper = new DatabaseHelper(getApplicationContext());


        getArgs();
        setupToolbar();
        loadSyllabus();
        loadNotes();
        loadQuestions();
        loadAns();
        loadModelQ();

        expandableLayouts();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(subject.getSubjectName());

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void loadModelQ() {
        List<ModelQuestion> mList = databaseHelper.getAllModelQ(subject.getSubjectName());

        if (mList.isEmpty()){
            TextView no_posts1 = findViewById(R.id.no_posts1);
            no_posts1.setVisibility(View.VISIBLE);
        }
        RecyclerView recyclerView = findViewById(R.id.recyclerModelQ);
        LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);


        ModelRecyclerAdapter adapter = new ModelRecyclerAdapter(mContext, mList, new ModelRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(ModelQuestion post) {

                fileName = post.getFileName();
                filePath = post.getFilePath();
                //check if file is in storage
                if (checkFileInStorage()) {
                    loadPDF();
                } else {

                    checkStoragePermissionNDownload();
                }


                checkStoragePermissionNDownload();
            }
        });

        recyclerView.setAdapter(adapter);
    }

    private void loadAns() {
        List<Question> mList = databaseHelper.getAllSoln(subject.getSubjectCode());

        if (mList.isEmpty()){
            TextView no_posts2 = findViewById(R.id.no_posts2);
            no_posts2.setVisibility(View.VISIBLE);
        }
        RecyclerView recyclerView = findViewById(R.id.recyclerSolutions);


        LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);


        QARecyclerAdapter adapter = new QARecyclerAdapter(mContext, mList, new QARecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Question post) {

                fileName = post.getFileName();
                filePath = post.getFilePath();
                if (checkFileInStorage()) {
                    loadPDF();
                } else {
                    checkStoragePermissionNDownload();
                }
            }
        });

        recyclerView.setAdapter(adapter);
    }

    private void loadQuestions() {
        List<Question> mList = databaseHelper.getAllQues(subject.getSubjectCode());

        if (mList.isEmpty()){
            TextView no_posts3 = findViewById(R.id.no_posts3);
            no_posts3.setVisibility(View.VISIBLE);
        }
        RecyclerView recyclerView = findViewById(R.id.recyclerQuestions);


        LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);


        QARecyclerAdapter adapter = new QARecyclerAdapter(mContext, mList, new QARecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Question post) {
                fileName = post.getFileName();
                filePath = post.getFilePath();
                if (checkFileInStorage()) {
                    loadPDF();
                } else {
                    checkStoragePermissionNDownload();
                }
            }
        });

        recyclerView.setAdapter(adapter);
    }

    private void loadNotes() {
        List<Note> mList = databaseHelper.getAllNotes(subject.getSubjectName());

        if (mList.isEmpty()){
            TextView no_posts4 = findViewById(R.id.no_posts4);
            no_posts4.setVisibility(View.VISIBLE);
        }
        RecyclerView recyclerView = findViewById(R.id.recyclerNotes);


        LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);


        NotesRecyclerAdapter adapter = new NotesRecyclerAdapter(mContext, mList, new NotesRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Note note) {
                fileName = note.getFileName();
                filePath = note.getFilePath();
                //check if file is in storage
                if (checkFileInStorage()) {
                    loadPDF();
                } else {
                    checkStoragePermissionNDownload();
                }

                isDownloadOnly = false;

                checkStoragePermissionNDownload();
            }
        }, new NotesRecyclerAdapter.OnDownloadClick() {
            @Override
            public void onClick(Note note) {
                filePath = note.getFilePath();
                fileName = note.getSubjectName();

                checkStoragePermissionNDownload();
                isDownloadOnly = true;
            }
        });
        recyclerView.setAdapter(adapter);

    }

    private void loadSyllabus() {
        List<Syllabus> mList = databaseHelper.getSubjectSyllabus(subject.getSubjectCode());

        if (mList.isEmpty()){
            TextView no_posts5 = findViewById(R.id.no_posts5);
            no_posts5.setVisibility(View.VISIBLE);
        }
        RecyclerView recyclerView = findViewById(R.id.recyclerSyllabus);


        LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);

        SyllabusRecyclerAdapter adapter = new SyllabusRecyclerAdapter(mContext, mList,
                new SyllabusRecyclerAdapter.OnPostClick() {
                    @Override
                    public void onClick(Syllabus syllabus) {
                        filePath = syllabus.getFilePath();
                        fileName = syllabus.getSubjectName();

                        //check if file is in storage
                        if (checkFileInStorage()) {
                            loadPDF();
                        } else {
                            checkStoragePermissionNDownload();
                        }

                        isDownloadOnly = false;


                    }
                }, new SyllabusRecyclerAdapter.OnDownloadClick() {
            @Override
            public void onClick(Syllabus syllabus) {
                filePath = syllabus.getFilePath();
                fileName = syllabus.getSubjectName();

                if (checkFileInStorage()) {
                    loadPDF();
                } else {
                    checkStoragePermissionNDownload();
                }

                isDownloadOnly = true;
            }
        });
        recyclerView.setAdapter(adapter);


    }


    private void getArgs() {
        in = getIntent();
        if (in.hasExtra(getString(R.string.calling_sem_sub_activity))) {
            subject = in.getParcelableExtra(getString(R.string.calling_sem_sub_activity));
        } else {
            Toast.makeText(this, getString(R.string.error_general), Toast.LENGTH_SHORT).show();
        }

    }


    private void checkStoragePermissionNDownload() {
        downloadFile();

    }


    public void expandableLayouts(){
        expandableLayout1 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout1);
        expandableLayout2 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout2);
        expandableLayout3 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout3);
        expandableLayout4 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout4);
        expandableLayout5 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout5);
        expandableLayout1.toggle();
        expandableLayout2.toggle();

    }
    /**
     * on click handlers
     *
     * @param view
     */
    public void expandableButton1(View view) {

        expandableLayout1.toggle(); // toggle expand and collapse

    }

    public void expandableButton2(View view) {
        expandableLayout2.toggle(); // toggle expand and collapse
    }

    public void expandableButton3(View view) {

        expandableLayout3.toggle(); // toggle expand and collapse
    }

    public void expandableButton4(View view) {

        expandableLayout4.toggle(); // toggle expand and collapse
    }

    public void expandableButton5(View view) {

        expandableLayout5.toggle(); // toggle expand and colla
    }

    //end click handlers


    private void loadPDF() {
        if (filePath.contains("doc") || filePath.contains("docx")) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(new File(FilePaths.getSaveDir() + FilePaths.getNameFromUrl(filePath)));

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
            intent.setDataAndType(uri, "application/msword");

        } else {
            Intent in = new Intent(mContext, PDFViewerActivity.class);

            in.putExtra(mContext.getString(R.string.pdf_view),
                    FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName) + ".pdf");
            startActivity(in);
        }

    }

    private boolean checkFileInStorage() {
        Log.d(TAG, "checkFileInStorage: " + fileName);
        Log.d(TAG, "checkFileInStorage: full path" + FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName));
        File file = new File(FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName) + ".pdf");
        return file.exists();
    }

    public boolean hasInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private void downloadFile() {

        if (hasInternetConnection()) {
            BroadCastReceiverActivity broadCastReceiverActivity = new BroadCastReceiverActivity(mContext, new BroadCastReceiverActivity.onClick() {
                @Override
                public void onResult(int progress) {

                }
            });
            broadCastReceiverActivity.registerReceiver();
            broadCastReceiverActivity.downloadFile(filePath, fileName);
        } else {
            Toast.makeText(mContext, "You are not connected to the internet.", Toast.LENGTH_SHORT).show();
        }


    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(MESSAGE_PROGRESS)) {

                Download download = intent.getParcelableExtra("download");
                progress.setMessage("Downloading..");
                progress.setProgress(download.getProgress());
                progress.setIndeterminate(true);
//              mProgressBar.setProgress(download.getProgress());
                if (download.getProgress() == 100) {
                    progress.cancel();
                    Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
//                    mProgressText.setText("File Download Complete");

                }

            }
        }
    };
}

