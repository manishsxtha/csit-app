package com.iit.csitapp.home;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.iit.csitapp.R;
import com.iit.csitapp.models.Subject;

import java.util.List;


public class SemSubjectsPagerAdapter extends PagerAdapter {
    private Context context;
    private List<Subject> mList;

    public SemSubjectsPagerAdapter(Context context, List<Subject> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.layout_sem_sub_home, container, false);

        TextView title= view.findViewById(R.id.title);
        TextView subject_code= view.findViewById(R.id.subject_code);
        TextView subject_type= view.findViewById(R.id.subject_type);

        title.setText(mList.get(position).getSubjectName());
        subject_code.setText(mList.get(position).getSubjectCode());
        subject_type.setText(mList.get(position).getType());

        container.addView(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(context, SemSubjectDetailActivity.class);
                in.putExtra(context.getString(R.string.calling_sem_sub_activity), mList.get(position));
                context.startActivity(in);
            }
        });

        return view;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}