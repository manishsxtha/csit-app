package com.iit.csitapp.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.iit.csitapp.R;
import com.iit.csitapp.models.Slider;
import com.iit.csitapp.utils.UniversalImageLoader;

public class SliderDetailActivity extends AppCompatActivity {

    private Context mContext = SliderDetailActivity.this;
    private boolean hasData;
    private Slider slider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider_detail);

        Intent in = getIntent();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (in.hasExtra(mContext.getString(R.string.slider))){
            slider = in.getParcelableExtra(mContext.getString(R.string.slider));
            hasData = true;
        }

        if (hasData){

            getSupportActionBar().setTitle(slider.getTitle());

            ImageView image = findViewById(R.id.image);
            TextView title = findViewById(R.id.title);
            TextView desc = findViewById(R.id.desc);

            UniversalImageLoader.setImage(slider.getImageUrl(), image, null, "");
            title.setText(slider.getTitle());
            desc.setText(slider.getDesc());

        }

    }
}
