package com.iit.csitapp.home;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iit.csitapp.R;
import com.iit.csitapp.event_news.PostDetailActivity;
import com.iit.csitapp.models.Slider;
import com.iit.csitapp.utils.UniversalImageLoader;

import java.util.List;


public class SliderPagerAdapter extends PagerAdapter {
    Context context;
    List<Slider> image_arraylist;
    private LayoutInflater layoutInflater;

    public SliderPagerAdapter(Context context, List<Slider> image_arraylist) {
        this.context = context;
        this.image_arraylist = image_arraylist;
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.layout_slider_home, container, false);

        ImageView im_slider = view.findViewById(R.id.im_slider);
        TextView title= view.findViewById(R.id.title);

        title.setText(image_arraylist.get(position).getTitle());
        UniversalImageLoader.setImage(image_arraylist.get(position).getImageUrl(),im_slider, null, "");
        im_slider.setScaleType(ImageView.ScaleType.FIT_XY);

        container.addView(view);

        im_slider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO which activity to navigate when slider click
                Intent in = new Intent(context, PostDetailActivity.class);
                in.putExtra(context.getString(R.string.calling_slider_detail), image_arraylist.get(position));
                context.startActivity(in);
            }
        });

        return view;
    }

    @Override
    public int getCount() {
        return image_arraylist.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}