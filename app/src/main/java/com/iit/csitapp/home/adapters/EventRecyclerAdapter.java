package com.iit.csitapp.home.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iit.csitapp.R;
import com.iit.csitapp.models.Event;
import com.iit.csitapp.utils.UniversalImageLoader;

import java.util.List;


public class EventRecyclerAdapter extends RecyclerView.Adapter<EventRecyclerAdapter.ViewHolder> {

    private static final String TAG = "EventRecyclerAdapter";


    private List<Event> mList;
    private Context context;
    private OnPostClick onClickListener;


    public EventRecyclerAdapter(Context context, List<Event> mList, OnPostClick onClickListener) {

        this.context = context;
        this.mList = mList;
        this.onClickListener = onClickListener;


    }

    @NonNull
    @Override
    public EventRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_home_events, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final EventRecyclerAdapter.ViewHolder holder, final int position) {
        final Event post = mList.get(holder.getAdapterPosition());


        UniversalImageLoader.setImage(post.getEventImage(), holder.image, null, "");
        holder.title.setText(mList.get(position).getTitle());

    }


    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView title;


        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);

//         
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (onClickListener != null && position != RecyclerView.NO_POSITION) {
                        onClickListener.onClick(mList.get(position));
                    }
                }
            });


        }


    }

    public interface OnPostClick {
        void onClick(Event Event);
    }

}
