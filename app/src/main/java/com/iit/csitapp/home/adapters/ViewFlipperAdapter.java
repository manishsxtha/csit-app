package com.iit.csitapp.home.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.iit.csitapp.R;
import com.iit.csitapp.home.SliderDetailActivity;
import com.iit.csitapp.models.Slider;
import com.iit.csitapp.utils.UniversalImageLoader;

import java.util.List;

public class ViewFlipperAdapter extends BaseAdapter {
    Context context;
    List<Slider> images;
    LayoutInflater inflater;

    public ViewFlipperAdapter(Context context, List<Slider> images) {
        this.context = context;
        this.images = images;
        this.inflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {


        view = inflater.inflate(R.layout.layout_slider_home, null);

        ImageView im_slider = view.findViewById(R.id.im_slider);

        UniversalImageLoader.setImage(images.get(position).getImageUrl(),im_slider, null, "");
        im_slider.setScaleType(ImageView.ScaleType.FIT_XY);


        im_slider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO which activity to navigate when slider click
                Intent in = new Intent(context, SliderDetailActivity.class);
                in.putExtra(context.getString(R.string.slider), images.get(position));
                context.startActivity(in);
            }
        });

        return view;
    }
}
