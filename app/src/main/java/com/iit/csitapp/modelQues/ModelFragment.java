package com.iit.csitapp.modelQues;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.MainActivityV2;
import com.iit.csitapp.R;
import com.iit.csitapp.models.ModelQuestion;
import com.iit.csitapp.models.Subject;
import com.iit.csitapp.utils.BroadCastReceiverActivity;
import com.iit.csitapp.utils.Constants;
import com.iit.csitapp.utils.DatabaseHelper;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.PDFViewerActivity;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ModelFragment extends Fragment {
    private static final String TAG = "SyllabusActivity";
    private static final int STORAGE_PERMISSION_CODE = 100;


    private View view;
    private Subject subject;
    private Bundle bundle;
    private SwipeRefreshLayout refresh;
    private Context mContext;
    private FirebaseHelper mFirebaseHelper;
    private List<ModelQuestion> mList;
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private ModelRecyclerAdapter adapter;
    private RelativeLayout empty_container;

    private List<ModelQuestion> mTempList;
    private SharedPreferenceHelper preferenceHelper;
    private DatabaseHelper databaseHelper;
    private String fileName;
    private String filePath;
    private String currentFormat;


    public static Fragment setInstance(Subject subject) {
        ModelFragment fragment = new ModelFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.SUBJECT, subject);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recycler_view_container, container, false);

        mContext = getContext();

        preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
        databaseHelper = DatabaseHelper.getInstance(mContext);

        getArgs();
        setupWidgets();

        setCurrentFormat();
        setupAdapter();
//        loadModelQ();
        checkInternetConnection();

        return view;

    }

    private void setCurrentFormat() {
        if (preferenceHelper.getSyllabusFormat() == 0)
            currentFormat = FilePaths.MODELQ;
        else
            currentFormat = FilePaths.MODELQ_NEW;
    }

    private void setupWidgets() {

        empty_container = view.findViewById(R.id.empty_container);

        refresh = view.findViewById(R.id.refresh);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                checkInternetConnection();
            }
        });

    }


    private void setupAdapter() {
        mList = new ArrayList<>();
        mTempList = new ArrayList<>();

        recyclerView = view.findViewById(R.id.recyclerView);

        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);

        adapter = new ModelRecyclerAdapter(mContext, mList, new ModelRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(ModelQuestion note) {

                filePath = note.getFilePath();
                //check if file is in storage
                if (checkFileInStorage()){
                    loadPDF();
                }else {
                    checkStoragePermissionNDownload();
                }



                checkStoragePermissionNDownload();
            }
        });

        recyclerView.setAdapter(adapter);


    }





    private void checkInternetConnection() {
        refresh.setRefreshing(true);
        if (((MainActivityV2) mContext).hasInternetConnection()) {
            mFirebaseHelper.getMyRef().child(currentFormat).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    Log.e(TAG, "onDataChange: "+dataSnapshot.toString() );
                    mTempList.clear();

//                    if (preferenceHelper.getShareKeyNotesSize() == dataSnapshot.getChildrenCount()) {
//                        loadNotesFromDatabase();
//                    } else {

                        databaseHelper.clearAllModelQ();
                        for (DataSnapshot ds :
                                dataSnapshot.getChildren()) {

                            ModelQuestion post = ds.getValue(ModelQuestion.class);
                            assert post != null;

                            if (post.getSubjectName().equals(subject.getSubjectName())) {
                                mTempList.add(post);
                            }

                            //add to the internal database as well
                            databaseHelper.setModelQ(post);

//                        }
                        preferenceHelper.setShareKeyNotesSize((int) dataSnapshot.getChildrenCount());
                        loadModelQ();
                    }


                    refresh.setRefreshing(false);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
                    loadNotesFromDatabase();
                    refresh.setRefreshing(false);

                }
            });

        }
//        else {
//            refresh.setRefreshing(false);
////            Toast.makeText(mContext, "From database", Toast.LENGTH_SHORT).show();
////            Toast.makeText(mContext, "You are not connected to the internet.", Toast.LENGTH_SHORT).show();
//            loadNotesFromDatabase();
//
//        }
    }

    private void loadNotesFromDatabase() {
        mList.clear();


        mList.addAll(databaseHelper.getAllModelQ(subject.getSubjectName()));
        checkContainerIsEmpty();
        sortList();
        adapter.notifyDataSetChanged();

    }

    private void sortList() {
        Collections.sort(mList, new Comparator<ModelQuestion>() {
            @Override
            public int compare(ModelQuestion o1, ModelQuestion o2) {
                return o2.getFileName().compareTo(o1.getFileName());
            }
        });
    }


    private void loadModelQ() {
        refresh.setRefreshing(true);
        mList.clear();

        //first clear all the subjects table in database
        mList.addAll(mTempList);

        checkContainerIsEmpty();
        sortList();
        adapter.notifyDataSetChanged();

    }

    private void checkContainerIsEmpty() {
        if (mList.size() == 0) {
            empty_container.setVisibility(View.VISIBLE);
        } else {
            empty_container.setVisibility(View.GONE);
        }
    }


    private void getArgs() {
        bundle = getArguments();
        assert bundle != null;
        subject = bundle.getParcelable(Constants.SUBJECT);
    }

    private void checkStoragePermissionNDownload() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        } else {
            downloadFile();
        }
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            downloadFile();
        } else {
            Toast.makeText(mContext, mContext.getString(R.string.error_permission), Toast.LENGTH_SHORT).show();
        }
    }


    private void loadPDF() {
        if (filePath.contains("doc") || filePath.contains("docx")) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(new File(FilePaths.getSaveDir() + FilePaths.getNameFromUrl(filePath)));

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
            intent.setDataAndType(uri, "application/msword");

        }else {
            Intent in = new Intent(mContext, PDFViewerActivity.class);

            in.putExtra(mContext.getString(R.string.pdf_view),
                    FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName)+".pdf");
            startActivity(in);
        }

    }

    private boolean checkFileInStorage() {
        Log.d(TAG, "checkFileInStorage: " + fileName);
        Log.d(TAG, "checkFileInStorage: full path" + FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName));
        File file = new File(FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName) + ".pdf");
        return file.exists();
    }
    private void downloadFile() {

        if (((MainActivityV2) mContext).hasInternetConnection()) {
            BroadCastReceiverActivity broadCastReceiverActivity = new BroadCastReceiverActivity(mContext, new BroadCastReceiverActivity.onClick() {
                @Override
                public void onResult(int progress) {

                }
            });
            broadCastReceiverActivity.registerReceiver();
            broadCastReceiverActivity.downloadFile(filePath, fileName);
        } else {
            Toast.makeText(mContext, "You are not connected to the internet.", Toast.LENGTH_SHORT).show();
        }


    }

}
