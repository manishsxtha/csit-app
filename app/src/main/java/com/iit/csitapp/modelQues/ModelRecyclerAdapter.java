package com.iit.csitapp.modelQues;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.pavlospt.roundedletterview.RoundedLetterView;
import com.iit.csitapp.R;
import com.iit.csitapp.models.ModelQuestion;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.RandomColor;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.util.List;

import static com.iit.csitapp.utils.SharedPreferenceHelper.getInstance;


public class ModelRecyclerAdapter extends RecyclerView.Adapter<ModelRecyclerAdapter.ViewHolder> {

    private static final String TAG = "NoteRecyclerAdapter";
    public final SharedPreferenceHelper sharedPreferences;
    private final FirebaseHelper mFirebaseHelper;


    private List<ModelQuestion> mList;
    private Context context;
    private OnPostClick onClickListener;


    public ModelRecyclerAdapter(Context context, List<ModelQuestion> mList, OnPostClick onClickListener
                                ) {

        this.context = context;
        this.mList = mList;
        this.onClickListener = onClickListener;

        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(context);
        sharedPreferences = getInstance(context);

    }

    @NonNull
    @Override
    public ModelRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_syllabus, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ModelRecyclerAdapter.ViewHolder holder, final int position) {
        final ModelQuestion post = mList.get(holder.getAdapterPosition());


        holder.rlv_name_view.setTitleText(mList.get(position).getSubjectName().substring(0, 1));

        holder.rlv_name_view.setBackgroundColor(RandomColor.getRandomMaterialColor(context,"400"));

        holder.title.setText(mList.get(position).getSubjectName());

    }


    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RoundedLetterView rlv_name_view;
        private TextView title;


        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            rlv_name_view = itemView.findViewById(R.id.rlv_name_view);
            title = itemView.findViewById(R.id.title);

//         
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (onClickListener != null && position != RecyclerView.NO_POSITION) {
                        onClickListener.onClick(mList.get(position));
                        Log.d(TAG, "onClick: model recycler");

                    }
                }
            });





        }


    }

    public interface OnPostClick {
        void onClick(ModelQuestion note);
    }


}
