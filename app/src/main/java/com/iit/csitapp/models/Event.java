package com.iit.csitapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Event implements Parcelable {
    public String id;
    public String createdDate;
    public String date;
    public String eventImage;
    public String eventUrl;
    public String title;
    public String description;

    protected Event(Parcel in) {
        createdDate = in.readString();
        date = in.readString();
        eventUrl = in.readString();
        eventImage = in.readString();
        id = in.readString();
        title = in.readString();
        description = in.readString();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEventUrl() {
        return eventUrl;
    }

    public void setEventUrl(String eventUrl) {
        this.eventUrl = eventUrl;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Event() {

    }

    public Event(String createdDate, String date, String eventUrl, String eventImage, String id, String title, String description) {

        this.createdDate = createdDate;
        this.date = date;
        this.eventUrl = eventUrl;
        this.eventImage = eventImage;
        this.id = id;
        this.title = title;
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(createdDate);
        parcel.writeString(date);
        parcel.writeString(eventUrl);
        parcel.writeString(eventImage);
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(description);
    }
}