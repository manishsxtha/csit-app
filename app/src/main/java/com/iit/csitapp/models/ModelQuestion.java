package com.iit.csitapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelQuestion implements Parcelable {
    public String createdDate;
    public String filePath;
    public String fileName;
    public String id;
    public String semester;
    public String subjectName;
    public String subjectCode;
    public String subjectType;
    public String year;
    public int isOldFormat;

    protected ModelQuestion(Parcel in) {
        createdDate = in.readString();
        filePath = in.readString();
        fileName = in.readString();
        id = in.readString();
        semester = in.readString();
        subjectName = in.readString();
        subjectCode = in.readString();
        subjectType = in.readString();
        year = in.readString();
        isOldFormat = in.readInt();
    }

    public static final Creator<ModelQuestion> CREATOR = new Creator<ModelQuestion>() {
        @Override
        public ModelQuestion createFromParcel(Parcel in) {
            return new ModelQuestion(in);
        }

        @Override
        public ModelQuestion[] newArray(int size) {
            return new ModelQuestion[size];
        }
    };

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getIsOldFormat() {
        return isOldFormat;
    }

    public void setIsOldFormat(int isOldFormat) {
        this.isOldFormat = isOldFormat;
    }

    public ModelQuestion() {
    }

    public ModelQuestion(String createdDate, String filePath, String fileName, String id, String semester, String subjectName, String subjectCode, String subjectType, String year, int isOldFormat) {
        this.createdDate = createdDate;
        this.filePath = filePath;
        this.fileName = fileName;
        this.id = id;
        this.semester = semester;
        this.subjectName = subjectName;
        this.subjectCode = subjectCode;
        this.subjectType = subjectType;
        this.year = year;
        this.isOldFormat = isOldFormat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(createdDate);
        parcel.writeString(filePath);
        parcel.writeString(fileName);
        parcel.writeString(id);
        parcel.writeString(semester);
        parcel.writeString(subjectName);
        parcel.writeString(subjectCode);
        parcel.writeString(subjectType);
        parcel.writeString(year);
        parcel.writeInt(isOldFormat);
    }
}