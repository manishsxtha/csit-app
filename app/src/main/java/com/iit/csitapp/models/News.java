package com.iit.csitapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class News implements Parcelable {
    public String id;
    public String createdDate;
    public String imageUrl;
    public String newsUrl;
    public String title;
    public String description;

    protected News(Parcel in) {
        createdDate = in.readString();
        newsUrl = in.readString();
        imageUrl = in.readString();
        id = in.readString();
        title = in.readString();
        description = in.readString();
    }

    public static final Creator<News> CREATOR = new Creator<News>() {
        @Override
        public News createFromParcel(Parcel in) {
            return new News(in);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getNewsUrl() {
        return newsUrl;
    }

    public void setNewsUrl(String newsUrl) {
        this.newsUrl = newsUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public News() {

    }

    public News(String createdDate, String newsUrl, String imageUrl, String id, String title, String description) {

        this.createdDate = createdDate;
        this.newsUrl = newsUrl;
        this.imageUrl = imageUrl;
        this.id = id;
        this.title = title;
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(createdDate);
        parcel.writeString(newsUrl);
        parcel.writeString(imageUrl);
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(description);
    }
}