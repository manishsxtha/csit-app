package com.iit.csitapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Note implements Parcelable {
    public String id;
    public String createdDate;
    public String fileName;
    public String filePath;
    public String createdBy;
    public String subjectName;
    public String subjectCode;
    public String subjectType;
    public String semester;
    public int isOldFormat;

    public Note() {
    }

    protected Note(Parcel in) {
        id = in.readString();
        createdDate = in.readString();
        fileName = in.readString();
        filePath = in.readString();
        createdBy = in.readString();
        subjectName = in.readString();
        subjectCode = in.readString();
        subjectType = in.readString();
        semester = in.readString();
        isOldFormat = in.readInt();
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public int getIsOldFormat() {
        return isOldFormat;
    }

    public void setIsOldFormat(int isOldFormat) {
        this.isOldFormat = isOldFormat;
    }

    public Note(String id, String createdDate, String fileName, String filePath, String createdBy, String subjectName, String subjectCode, String subjectType, String semester, int isOldFormat) {
        this.id = id;
        this.createdDate = createdDate;
        this.fileName = fileName;
        this.filePath = filePath;
        this.createdBy = createdBy;
        this.subjectName = subjectName;
        this.subjectCode = subjectCode;
        this.subjectType = subjectType;
        this.semester = semester;
        this.isOldFormat = isOldFormat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(createdDate);
        parcel.writeString(fileName);
        parcel.writeString(filePath);
        parcel.writeString(createdBy);
        parcel.writeString(subjectName);
        parcel.writeString(subjectCode);
        parcel.writeString(subjectType);
        parcel.writeString(semester);
        parcel.writeInt(isOldFormat);
    }
}
