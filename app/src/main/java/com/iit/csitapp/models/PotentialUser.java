package com.iit.csitapp.models;

public class PotentialUser {
    public String id;
    public String name;
    public String email;
    public String phone;
    public String interestedCourse;
    public boolean seenByAdmin;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInterestedCourse() {
        return interestedCourse;
    }

    public void setInterestedCourse(String interestedCourse) {
        this.interestedCourse = interestedCourse;
    }

    public boolean isSeenByAdmin() {
        return seenByAdmin;
    }

    public void setSeenByAdmin(boolean seenByAdmin) {
        this.seenByAdmin = seenByAdmin;
    }

    public PotentialUser() {
    }

    public PotentialUser(String id, String name, String email, String phone, String interestedCourse, boolean seenByAdmin) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.interestedCourse = interestedCourse;
        this.seenByAdmin = seenByAdmin;
    }
}
