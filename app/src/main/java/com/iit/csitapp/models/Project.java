package com.iit.csitapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Project implements Parcelable {
    public String id;
    public String createdDate;
    public String title;
    public String desc;
    public String pic;

    public Project() {
    }

    public Project(String id, String createdDate, String desc, String title, String pic) {
        this.id = id;
        this.createdDate = createdDate;
        this.desc = desc;
        this.title = title;
        this.pic = pic;
    }

    protected Project(Parcel in) {
        id = in.readString();
        createdDate = in.readString();
        desc = in.readString();
        title = in.readString();
        pic = in.readString();
    }

    public static final Creator<Project> CREATOR = new Creator<Project>() {
        @Override
        public Project createFromParcel(Parcel in) {
            return new Project(in);
        }

        @Override
        public Project[] newArray(int size) {
            return new Project[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(createdDate);
        parcel.writeString(desc);
        parcel.writeString(title);
        parcel.writeString(pic);
    }
}