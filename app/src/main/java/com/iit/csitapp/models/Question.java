package com.iit.csitapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Question implements Parcelable {
    public String id;
    public String createdDate;
    public String filePath;
    public String fileName;
    public String semester;
    public String subjectName;
    public String subjectCode;
    public String subjectType;
    public String year;
    public int isOldFormat;

    protected Question(Parcel in) {
        id = in.readString();
        createdDate = in.readString();
        filePath = in.readString();
        fileName = in.readString();
        semester = in.readString();
        subjectName = in.readString();
        subjectCode = in.readString();
        subjectType = in.readString();
        year = in.readString();
        isOldFormat = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(createdDate);
        dest.writeString(filePath);
        dest.writeString(fileName);
        dest.writeString(semester);
        dest.writeString(subjectName);
        dest.writeString(subjectCode);
        dest.writeString(subjectType);
        dest.writeString(year);
        dest.writeInt(isOldFormat);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getIsOldFormat() {
        return isOldFormat;
    }

    public void setIsOldFormat(int isOldFormat) {
        this.isOldFormat = isOldFormat;
    }

    public Question() {
    }

    public Question(String id, String createdDate, String filePath, String fileName, String semester, String subjectName, String subjectCode, String subjectType, String year, int isOldFormat) {
        this.id = id;
        this.createdDate = createdDate;
        this.filePath = filePath;
        this.fileName = fileName;
        this.semester = semester;
        this.subjectName = subjectName;
        this.subjectCode = subjectCode;
        this.subjectType = subjectType;
        this.year = year;
        this.isOldFormat = isOldFormat;
    }
}