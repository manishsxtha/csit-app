package com.iit.csitapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Semester implements Parcelable {
    private String semester_id;
    private String semester_name;
//    private String education_type_id;

    protected Semester(Parcel in) {
        semester_id = in.readString();
        semester_name = in.readString();
//        education_type_id = in.readString();
    }

    public static final Creator<Semester> CREATOR = new Creator<Semester>() {
        @Override
        public Semester createFromParcel(Parcel in) {
            return new Semester(in);
        }

        @Override
        public Semester[] newArray(int size) {
            return new Semester[size];
        }
    };

    public String getSemester_id() {
        return semester_id;
    }

    public void setSemester_id(String semester_id) {
        this.semester_id = semester_id;
    }

    public String getSemester_name() {
        return semester_name;
    }

    public void setSemester_name(String semester_name) {
        this.semester_name = semester_name;
    }

//    public String getEducation_type_id() {
//        return education_type_id;
//    }
//
//    public void setEducation_type_id(String education_type_id) {
//        this.education_type_id = education_type_id;
//    }

    public Semester() {


    }

    public Semester(String semester_id, String semester_name) {

        this.semester_id = semester_id;
        this.semester_name = semester_name;
//        this.education_type_id = education_type_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(semester_id);
        dest.writeString(semester_name);
//        dest.writeString(education_type_id);
    }
}
