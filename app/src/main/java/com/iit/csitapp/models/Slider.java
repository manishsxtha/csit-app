package com.iit.csitapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Slider implements Parcelable {
    public String id;
    public String createdDate;
    public String desc;
    public String imageUrl;
    public String title;
    public String newsUrl;

    public Slider() {
    }

    protected Slider(Parcel in) {
        id = in.readString();
        createdDate = in.readString();
        desc = in.readString();
        imageUrl = in.readString();
        title = in.readString();
        newsUrl = in.readString();
    }

    public static final Creator<Slider> CREATOR = new Creator<Slider>() {
        @Override
        public Slider createFromParcel(Parcel in) {
            return new Slider(in);
        }

        @Override
        public Slider[] newArray(int size) {
            return new Slider[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNewsUrl() {
        return newsUrl;
    }

    public void setNewsUrl(String newsUrl) {
        this.newsUrl = newsUrl;
    }

    public Slider(String id, String createdDate, String desc, String imageUrl, String title, String newsUrl) {
        this.id = id;
        this.createdDate = createdDate;
        this.desc = desc;
        this.imageUrl = imageUrl;
        this.title = title;
        this.newsUrl = newsUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(createdDate);
        parcel.writeString(desc);
        parcel.writeString(imageUrl);
        parcel.writeString(title);
        parcel.writeString(newsUrl);
    }
}