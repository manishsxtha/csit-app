package com.iit.csitapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Subject implements Parcelable {

    public String semester;
    public String subjectName;
    public String subjectCode;
    public String type;
    public int isOldFormat;

    protected Subject(Parcel in) {
        semester = in.readString();
        subjectName = in.readString();
        subjectCode = in.readString();
        type = in.readString();
        isOldFormat = in.readInt();
    }

    public static final Creator<Subject> CREATOR = new Creator<Subject>() {
        @Override
        public Subject createFromParcel(Parcel in) {
            return new Subject(in);
        }

        @Override
        public Subject[] newArray(int size) {
            return new Subject[size];
        }
    };

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIsOldFormat() {
        return isOldFormat;
    }

    public void setIsOldFormat(int isOldFormat) {
        this.isOldFormat = isOldFormat;
    }

    public Subject(String semester, String subjectName, String subjectCode, String type, int isOldFormat) {
        this.semester = semester;
        this.subjectName = subjectName;
        this.subjectCode = subjectCode;
        this.type = type;
        this.isOldFormat = isOldFormat;
    }

    public Subject() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(semester);
        parcel.writeString(subjectName);
        parcel.writeString(subjectCode);
        parcel.writeString(type);
        parcel.writeInt(isOldFormat);
    }
}