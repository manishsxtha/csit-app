package com.iit.csitapp.models;

import android.os.Parcel;
import android.os.Parcelable;


public class Syllabus implements Parcelable {


    public String id;
    public String createdDate;
    public String filePath;
    public String semester;
    public String subjectName;
    public String subjectCode;
    public String subjectType;
    public int isOldFormat;

    public Syllabus() {
    }

    protected Syllabus(Parcel in) {
        id = in.readString();
        createdDate = in.readString();
        filePath = in.readString();
        semester = in.readString();
        subjectName = in.readString();
        subjectCode = in.readString();
        subjectType = in.readString();
        isOldFormat = in.readInt();
    }

    public static final Creator<Syllabus> CREATOR = new Creator<Syllabus>() {
        @Override
        public Syllabus createFromParcel(Parcel in) {
            return new Syllabus(in);
        }

        @Override
        public Syllabus[] newArray(int size) {
            return new Syllabus[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }

    public int getIsOldFormat() {
        return isOldFormat;
    }

    public void setIsOldFormat(int isOldFormat) {
        this.isOldFormat = isOldFormat;
    }

    public Syllabus(String id, String createdDate, String filePath, String semester, String subjectName, String subjectCode, String subjectType, int isOldFormat) {
        this.id = id;
        this.createdDate = createdDate;
        this.filePath = filePath;
        this.semester = semester;
        this.subjectName = subjectName;
        this.subjectCode = subjectCode;
        this.subjectType = subjectType;
        this.isOldFormat = isOldFormat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(createdDate);
        parcel.writeString(filePath);
        parcel.writeString(semester);
        parcel.writeString(subjectName);
        parcel.writeString(subjectCode);
        parcel.writeString(subjectType);
        parcel.writeInt(isOldFormat);
    }
}
