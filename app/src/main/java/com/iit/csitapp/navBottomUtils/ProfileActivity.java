package com.iit.csitapp.navBottomUtils;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import com.iit.csitapp.R;
import com.iit.csitapp.models.User;
import com.iit.csitapp.utils.SharedPreferenceHelper;

public class ProfileActivity extends AppCompatActivity {
    private SharedPreferenceHelper helper;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //get the user data from shared preferences
        helper= SharedPreferenceHelper.getInstance(getApplicationContext());

        user = helper.getUserInfo();


        setupToolbar();
        bindUserDataToView();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void bindUserDataToView() {
//        ActivityProfileBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_profile);
//
//        binding.setUser(user);
    }

    public void onEditClick(View view) {
        startActivity(new Intent(getApplicationContext(), ProfileEditActivity.class));
    }


}
