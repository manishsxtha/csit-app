package com.iit.csitapp.navBottomUtils;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.iit.csitapp.R;
import com.iit.csitapp.models.User;
import com.iit.csitapp.utils.SharedPreferenceHelper;

public class ProfileEditActivity extends AppCompatActivity {

    private SharedPreferenceHelper helper;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        //get the user data from shared preferences
        helper= SharedPreferenceHelper.getInstance(getApplicationContext());

        user = helper.getUserInfo();
//        user.obUsername.set(user.username);
//        user.obAvatar.set(user.avatar_img_link);
//        user.obEmail.set(user.email);

        setupToolbar();
        bindUserDataToView();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(user.getUsername());

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void bindUserDataToView() {
//        ActivityProfileEditBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_profile_edit);
//
//        binding.setUser(user);
    }

    public void onSaveClick(View view) {
        EditText username = findViewById(R.id.tv_name);


        if (!username.getText().toString().equals("")){
            DatabaseReference mRef= FirebaseDatabase.getInstance().getReference();
            SharedPreferenceHelper helper = SharedPreferenceHelper.getInstance(getApplicationContext());

            mRef.child(helper.getUserInfo().getUser_id())
                    .child("username")
                    .setValue(username.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()){
                        Toast.makeText(ProfileEditActivity.this, "Success", Toast.LENGTH_SHORT).show();

                        User user = helper.getUserInfo();

                        User editUser = user;
                        editUser.setUsername(username.getText().toString());

                        helper.saveUserInfo(editUser);

                        finish();

                    }else {
                        Toast.makeText(ProfileEditActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else{
            Toast.makeText(this, "Fields cannot be empty", Toast.LENGTH_SHORT).show();
        }


    }
}
