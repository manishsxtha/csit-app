package com.iit.csitapp.network;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.iit.csitapp.MainActivity;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Download;
import com.iit.csitapp.utils.BroadCastReceiverActivity;
import com.iit.csitapp.utils.FilePaths;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;


public class DownloadService extends IntentService {
    private static final String TAG = "DownloadService";

    private static final String ADMIN_CHANNEL_ID = "id1";
    private String filePath;
    private String fileName;

    public String GROUP_KEY = "com.iit.csitapp.GROUP_NOTIFICATION";

    public DownloadService() {
        super("Download Service");
    }

    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;
    private int totalFileSize;


    @Override
    protected void onHandleIntent(Intent intent) {

        int notificationId = new Random().nextInt(60000);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels();
        } else {

            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        notificationBuilder = new NotificationCompat.Builder(this,
                ADMIN_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_download)
                .setContentTitle("Download")
                .setContentText("Downloading File")
                .setOnlyAlertOnce(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSound(null)
                .setGroup(GROUP_KEY)
                .setAutoCancel(false);

        notificationManager.notify(0, notificationBuilder.build());

        filePath = intent.getStringExtra("dFilePath");
        fileName = intent.getStringExtra("dFileName");

        Log.e(TAG, "onHandleIntent: " + filePath+", "+fileName);
        initDownload(fileName,filePath);

    }

    private void initDownload(String fileName, String url) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://csitstore.cygnusnepal.com.np/")
                .build();

        RetrofitInstance retrofitInterface = retrofit.create(RetrofitInstance.class);

        Call<ResponseBody> request = retrofitInterface.downloadFileWithDynamicUrlSync(filePath);
        try {

            Log.e(TAG, "initDownload: downloading file");
            downloadFile(request.execute().body());

        } catch (IOException e) {

            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        Log.e(TAG, "setupChannels: Orea setting up channels");
        CharSequence adminChannelName = getString(R.string.notifications_admin_channel_name);
        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_DEFAULT);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.setImportance(NotificationManager.IMPORTANCE_LOW);
//        adminChannel.enableLights(true);
//        adminChannel.setLightColor(Color.RED);
//        adminChannel.enableVibration(true);

        notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(adminChannel);


    }

    private void downloadFile(ResponseBody body) throws IOException {
        File folder = new File(Environment.getExternalStorageDirectory(), FilePaths.SAVEFOLDER);
        boolean success = true;
        if (!folder.exists()) {
            //Toast.makeText(MainActivity.this, "Directory Does Not Exist, Create It", Toast.LENGTH_SHORT).show();
            success = folder.mkdirs();
            File oldFolder = new File(Environment.getExternalStorageDirectory() + "/" + FilePaths.SAVEFOLDER, "old");
            File newFolder = new File(Environment.getExternalStorageDirectory() + "/" + FilePaths.SAVEFOLDER, "new");
            oldFolder.mkdir();
            newFolder.mkdir();

        }
        if (success) {
            Log.e(TAG, "downloadFile: directory created success");
        } else {
            Log.e(TAG, "downloadFile: directory created failed");

        }
        Log.e(TAG, "downloadFile: actuall downloading...");

        long total = 0;
        long startTime = System.currentTimeMillis();
        int timeCount = 1;
        BufferedInputStream bis;
        FileOutputStream output;
        File outputFile;
        int count;
        byte data[] = new byte[1024 * 4];
        long fileSize = body.contentLength();
        bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
        if (filePath.contains("drive"))
            outputFile = new File(FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName) + ".pdf");
        else
            outputFile = new File(FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName));
        output = new FileOutputStream(outputFile);
        try {

            while ((count = bis.read(data)) != -1) {

                total += count;
                totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
                double current = Math.round(total / (Math.pow(1024, 2)));

                int progress = (int) ((total * 100) / fileSize);

                long currentTime = System.currentTimeMillis() - startTime;

                Download download = new Download();
                download.setTotalFileSize(totalFileSize);

                if (currentTime > 1000 * timeCount) {

                    download.setCurrentFileSize((int) current);
                    download.setProgress(progress);
                    sendNotification(download);
                    timeCount++;
                }

                output.write(data, 0, count);
            }
            onDownloadComplete();
        } catch (Exception e) {
            Log.e(TAG, "downloadFile: error log"+e.getMessage() );
            Toast.makeText(this, "Error occurred!", Toast.LENGTH_SHORT).show();
            notificationManager.cancel(0);
            stopSelf();
        } finally {
            output.flush();
            output.close();
            bis.close();
        }



    }

    private void sendNotification(Download download) {
        sendIntent(download);
        Log.e(TAG, "sendNotification: progress: "+download.getProgress() );
        notificationBuilder.setProgress(100, download.getProgress(), false);
//        notificationBuilder.setContentText(String.format("Downloaded (%d/%d) MB", download.getCurrentFileSize(), download.getTotalFileSize()));
        notificationManager.notify(0, notificationBuilder.build());
    }


    private void sendIntent(Download download) {

        Intent intent = new Intent(MainActivity.MESSAGE_PROGRESS);
        intent.putExtra("download", download);
        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
    }

    private void onDownloadComplete() {

        Log.e(TAG, "onDownloadComplete: complete");

        Download download = new Download();
        download.setProgress(100);


        sendIntent(download);

        notificationManager.cancel(0);
        notificationBuilder.setProgress(0, 0, false);
        notificationBuilder.setContentText("File Downloaded");
        notificationManager.notify(0, notificationBuilder.build());

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        notificationManager.cancel(0);
        stopSelf();
        BroadCastReceiverActivity receiverActivity = new BroadCastReceiverActivity(getApplicationContext(), new BroadCastReceiverActivity.onClick() {
            @Override
            public void onResult(int progress) {

            }
        });
        receiverActivity.unregisterReceiver();
    }


}
