package com.iit.csitapp.notes;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.MainActivityV2;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Note;
import com.iit.csitapp.models.Subject;
import com.iit.csitapp.utils.BroadCastReceiverActivity;
import com.iit.csitapp.utils.Constants;
import com.iit.csitapp.utils.DatabaseHelper;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.PDFViewerActivity;
import com.iit.csitapp.utils.SharedPreferenceHelper;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.Request;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class NotesFragment extends Fragment {
    private static final String TAG = "SyllabusActivity";
    private static final int STORAGE_PERMISSION_CODE = 100;


    private View view;
    private Subject subject;
    private Bundle bundle;
    private SwipeRefreshLayout refresh;
    private Context mContext;
    private FirebaseHelper mFirebaseHelper;
    private List<Note> mList;
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private NotesRecyclerAdapter adapter;
    private RelativeLayout empty_container;
    private Request request;
    private Fetch fetch;
    private List<Note> mTempList;
    private SharedPreferenceHelper preferenceHelper;
    private DatabaseHelper databaseHelper;

    private String fileName;
    private String filePath;
    private boolean isDownloadOnly;
    private String currentFormat;
    private ValueEventListener eventListener;


    public static Fragment setInstance(Subject subject) {
        NotesFragment fragment = new NotesFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.SUBJECT, subject);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recycler_view_container, container, false);

        mContext = getContext();

        preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
        databaseHelper = DatabaseHelper.getInstance(mContext);

        setCurrentFormat();
        getArgs();
        setupWidgets();

        setupAdapter();
//        loadNotes();
        checkInternetConnection();


        return view;

    }


    private void setCurrentFormat() {
        if (preferenceHelper.getSyllabusFormat() == 0)
            currentFormat = FilePaths.NOTES;
        else
            currentFormat  =FilePaths.NOTES_NEW;
    }

    private void setupWidgets() {

        empty_container = view.findViewById(R.id.empty_container);

        refresh = view.findViewById(R.id.refresh);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                checkInternetConnection();
            }
        });

    }


    private void setupAdapter() {
        mList = new ArrayList<>();
        mTempList = new ArrayList<>();

        recyclerView = view.findViewById(R.id.recyclerView);

        manager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);



        adapter = new NotesRecyclerAdapter(mContext, mList, new NotesRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Note note) {

                fileName = note.getFileName();

                filePath = note.getFilePath();
                //check if file is in storage
                if (checkFileInStorage()){
                    loadPDF();
                }
//                else {
//                    checkStoragePermissionNDownload();
//                }
//
//                isDownloadOnly = false;
//
//                checkStoragePermissionNDownload();
            }
        }, new NotesRecyclerAdapter.OnDownloadClick() {
            @Override
            public void onClick(Note note) {
                filePath = note.getFilePath();
                fileName = note.getSubjectName();

//                checkStoragePermissionNDownload();

            }
        });

        recyclerView.setAdapter(adapter);


    }





    private void checkInternetConnection() {
        refresh.setRefreshing(true);

        Toast.makeText(mContext, currentFormat, Toast.LENGTH_SHORT).show();
        eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Log.e(TAG, "onDataChange: "+dataSnapshot.toString() );
                mTempList.clear();

//                        if (databaseHelper.isRemoteNotesSizeSame((int) dataSnapshot.getChildrenCount())) {
//                            loadNotesFromDatabase();
//                        } else {

                databaseHelper.clearNotesData();
                for (DataSnapshot ds :
                        dataSnapshot.getChildren()) {

                    Note post = ds.getValue(Note.class);
                    assert post != null;

                    if (post.getSubjectName().equals(subject.getSubjectName())) {
                        mTempList.add(post);
                    }

                    //add to the internal database as well


                    //                        }

                    loadNotes();
                }
                databaseHelper.setNotes(mTempList);

                refresh.setRefreshing(false);
            }
//                    }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();

                refresh.setRefreshing(false);

            }
        };
        mFirebaseHelper.getMyRef().child(currentFormat).addListenerForSingleValueEvent(eventListener);



    }

    private void loadNotesFromDatabase() {
        mList.clear();


        mList.addAll(databaseHelper.getAllNotes(subject.getSubjectName()));
        checkContainerIsEmpty();
        sortList();
        refresh.setRefreshing(false);
        adapter.notifyDataSetChanged();

    }

    private void sortList() {
        Collections.sort(mList, new Comparator<Note>() {
            @Override
            public int compare(Note o1, Note o2) {
                return o2.getFileName().compareTo(o1.getFileName());
            }
        });
    }


    private void loadNotes() {
        refresh.setRefreshing(true);
        mList.clear();

        //first clear all the subjects table in database
        mList.addAll(mTempList);

        checkContainerIsEmpty();
        sortList();
        adapter.notifyDataSetChanged();

    }

    private void checkContainerIsEmpty() {
        if (mList.size() == 0) {
            empty_container.setVisibility(View.VISIBLE);
        } else {
            empty_container.setVisibility(View.GONE);
        }
    }


    private void getArgs() {
        bundle = getArguments();
        assert bundle != null;
        subject = bundle.getParcelable(Constants.SUBJECT);
    }


    private void loadPDF() {
        if (filePath.contains("doc") || filePath.contains("docx")) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(new File(FilePaths.getSaveDir() + FilePaths.getNameFromUrl(filePath)));

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
            intent.setDataAndType(uri, "application/msword");

        } else {
            Intent in = new Intent(mContext, PDFViewerActivity.class);

            in.putExtra(mContext.getString(R.string.pdf_view),
                    FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName) + ".pdf");
            startActivity(in);
        }

    }

    private boolean checkFileInStorage() {
        Log.d(TAG, "checkFileInStorage: " + fileName);
        Log.d(TAG, "checkFileInStorage: full path" + FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName));
        File file = new File(FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName) + ".pdf");
        return file.exists();
    }

}
