package com.iit.csitapp.notes;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.pavlospt.roundedletterview.RoundedLetterView;
import com.iit.csitapp.MainActivityV2;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Note;
import com.iit.csitapp.utils.BroadCastReceiverActivity;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.RandomColor;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.io.File;
import java.util.List;

import static com.iit.csitapp.utils.SharedPreferenceHelper.getInstance;


public class NotesRecyclerAdapter extends RecyclerView.Adapter<NotesRecyclerAdapter.ViewHolder> {

    private static final String TAG = "NoteRecyclerAdapter";
    public final SharedPreferenceHelper sharedPreferences;
    private final FirebaseHelper mFirebaseHelper;

    private List<Note> mList;
    private Context context;
    private OnPostClick onClickListener;
    private OnDownloadClick onDownloadClick;


    public NotesRecyclerAdapter(Context context, List<Note> mList, OnPostClick onClickListener,
                                OnDownloadClick onDownloadClick) {

        this.context = context;
        this.mList = mList;
        this.onClickListener = onClickListener;
        this.onDownloadClick = onDownloadClick;


        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(context);
        sharedPreferences = getInstance(context);

    }

    @NonNull
    @Override
    public NotesRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_syllabus, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final NotesRecyclerAdapter.ViewHolder holder, final int position) {
        final Note post = mList.get(holder.getAdapterPosition());


        holder.rlv_name_view.setTitleText(mList.get(position).getSubjectName().substring(0, 1));

        holder.rlv_name_view.setBackgroundColor(RandomColor.getRandomMaterialColor(context,"400"));

        String name = FilePaths.getNameFromUrl(post.getFileName());
        holder.title.setText(name);

    }


    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RoundedLetterView rlv_name_view;
        private TextView title;
        private ImageView iv_download;
        private ProgressBar progressBar;


        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            rlv_name_view = itemView.findViewById(R.id.rlv_name_view);
            title = itemView.findViewById(R.id.title);
            iv_download = itemView.findViewById(R.id.iv_download);
            progressBar = itemView.findViewById(R.id.progressBar);

//         
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (onClickListener != null && position != RecyclerView.NO_POSITION) {

                        if (checkFileInStorage(mList.get(position).getFilePath(), mList.get(position).getFileName()))
                            downloadFile(mList.get(position).getFilePath(), mList.get(position).getFileName(), progressBar);
                        onClickListener.onClick(mList.get(position));
                    }
                }
            });


//            iv_download.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    int position = getAdapterPosition();
//                    if (onClickListener != null && position != RecyclerView.NO_POSITION) {
//                        //download the image from the drive
//                        Log.d(TAG, "onClick: downloading "+mList.get(position).getFilePath());
//                        onDownloadClick.onClick(mList.get(position));
//
//                    }
//                }
//            });



        }


    }

    public interface OnPostClick {
        void onClick(Note note);
    }

    public interface OnDownloadClick {
        void onClick(Note note);
    }

    private void downloadFile(String filePath, String fileName, ProgressBar itemView) {

        itemView.setVisibility(View.VISIBLE);

        if (((MainActivityV2) context).hasInternetConnection()) {
            BroadCastReceiverActivity broadCastReceiverActivity = new BroadCastReceiverActivity(context, new BroadCastReceiverActivity.onClick() {
                @Override
                public void onResult(int progress) {
                    itemView.setProgress(progress);
                }
            });
            broadCastReceiverActivity.registerReceiver();
            broadCastReceiverActivity.downloadFile(filePath, fileName);
        } else {
            Toast.makeText(context, "You are not connected to the internet.", Toast.LENGTH_SHORT).show();
        }


    }

    private boolean checkFileInStorage(String filePath, String fileName) {
        Log.d(TAG, "checkFileInStorage: " + fileName);
        Log.d(TAG, "checkFileInStorage: full path" + FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName));
        File file = new File(FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName) + ".pdf");
        return file.exists();
    }
}
