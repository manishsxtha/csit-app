package com.iit.csitapp.questionNsolutions;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iit.csitapp.R;
import com.iit.csitapp.models.Subject;
import com.iit.csitapp.questionNsolutions.fragments.OldQuestionsFragment;
import com.iit.csitapp.utils.Constants;

import java.util.ArrayList;

public class QAMainFragment extends Fragment {
    private static final String TAG = "SyllabusActivity";


    private ArrayList<Fragment> mFragments;

    public static final String[] POSTS = {
            "Old Questions",
            "Answers"
    };
    private ViewPager viewPager;
    private View view;
    private Subject subject;
    private Bundle bundle;


    public static Fragment setInstance(Subject subject) {
        QAMainFragment fragment = new QAMainFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.SUBJECT, subject);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ques_ans,container,false);

        getArgs();
        setupViewPager();

        return view;

    }


    private void getArgs() {
        bundle = getArguments();
        assert bundle != null;
        subject = bundle.getParcelable(Constants.SUBJECT);
    }


    private void setupViewPager() {
        mFragments = new ArrayList<>();

        PostPagerAdapter adapter = new PostPagerAdapter(getChildFragmentManager());
        adapter.addFragment(OldQuestionsFragment.setInstance(true, subject)); //index 0
        adapter.addFragment(OldQuestionsFragment.setInstance(false, subject)); //index 1


        viewPager = (ViewPager) view.findViewById(R.id.htab_viewpager);
        viewPager.setAdapter(adapter);


        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.htab_tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setText(POSTS[0]);
        tabLayout.getTabAt(1).setText(POSTS[1]);
    }


}
