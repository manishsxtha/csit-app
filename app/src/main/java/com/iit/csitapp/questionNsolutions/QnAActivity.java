package com.iit.csitapp.questionNsolutions;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.firebase.auth.FirebaseUser;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Subject;
import com.iit.csitapp.questionNsolutions.fragments.OldQuestionsFragment;
import com.iit.csitapp.startup.LoginActivity;
import com.iit.csitapp.subjects.SubjectRecyclerAdapter;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.NavigationTabs;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class QnAActivity extends AppCompatActivity {
    private static final String TAG = "SyllabusActivity";
    private static final int WRITE_REQUEST_CODE = 300;

    private AHBottomNavigation bottomNavigation;
    private Context mContext = QnAActivity.this;
    private RecyclerView recyclerView;
    private List<Subject> mList;
    private LinearLayoutManager manager;
    private SubjectRecyclerAdapter adapter;
    private FirebaseHelper firebaseHelper;
    private SwipeRefreshLayout refresh;
    private TextView tv_change_sem;
    private String filePath;


    private ArrayList<Fragment> mFragments;

    public static final String[] POSTS = {
            "Old Questions",
            "Answers"
    };
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ques_ans);


        setupToolbar();
        setupFirebase();

        setupWidgets();
        setupViewPager();


        setupBottomNavigation(NavigationTabs.QNA);
    }



    private void setupViewPager() {
        mFragments = new ArrayList<>();

        PostPagerAdapter adapter = new PostPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new OldQuestionsFragment()); //index 0
        adapter.addFragment(new OldQuestionsFragment()); //index 1

        viewPager = (ViewPager) findViewById(R.id.htab_viewpager);
        viewPager.setAdapter(adapter);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.htab_tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setText(POSTS[0]);
        tabLayout.getTabAt(1).setText(POSTS[1]);
    }

    private void setupWidgets() {

    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setTitle("Q/A");
        } else {
            getSupportActionBar().setTitle("Q/A");
        }


    }

    private void setupFirebase() {
        firebaseHelper = new FirebaseHelper(mContext);
        updateUI(firebaseHelper.getAuth().getCurrentUser());
    }

//    private void loadSubjectData() {
//        refresh.setRefreshing(true);
//        final SharedPreferenceHelper preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
//        firebaseHelper.getMyRef().child(FilePaths.SUBJECTS)
//                .addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                        mList.clear();
//                        for (DataSnapshot ds :
//                                dataSnapshot.getChildren()) {
//
//                            Subject subject= ds.getValue(Subject.class);
//                            if (subject.getSemester().equals(preferenceHelper.getSemester()))
//                                mList.add(subject);
//                        }
//                        adapter.notifyDataSetChanged();
//                        refresh.setRefreshing(false);
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//                        Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
//                        refresh.setRefreshing(false);
//                    }
//                });
//    }


    private void setupBottomNavigation(int position) {


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);

// Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_1, R.drawable.ic_settings_white, R.color.black);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.ic_settings_white, R.color.black);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.ic_settings_white, R.color.black);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.ic_settings_white, R.color.light_grey);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.tab_5, R.drawable.ic_settings_white, R.color.light_grey);

// Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);

        // Set background color
        bottomNavigation.setDefaultBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
        // Manage titles
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

        // Use colored navigation with circle reveal effect
        bottomNavigation.setColored(false);
        bottomNavigation.setUseElevation(false);
        bottomNavigation.setBehaviorTranslationEnabled(false);

// Set current item programmatically
        bottomNavigation.setCurrentItem(position);

        // Customize notification (title, background, typeface)
        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                // Do something cool here...

                switch (position) {
                    case NavigationTabs
                            .HOME:


                        break;
                    case NavigationTabs.SYLLABUS:

                        break;
                    case NavigationTabs.QNA:

                        break;
                    case NavigationTabs.POSTS:

                        break;
//                    case NavigationTabs.NOTIFICATION:
//
//                        break;
                    default:

                        break;

                }
                return true;
            }
        });
    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser == null) {
            Log.d(TAG, "updateUI: User not signed in!");
            Intent in = new Intent(this, LoginActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(in);
            finish();
        }
    }


}
