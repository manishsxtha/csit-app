package com.iit.csitapp.questionNsolutions.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.MainActivityV2;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Question;
import com.iit.csitapp.models.Subject;
import com.iit.csitapp.utils.BroadCastReceiverActivity;
import com.iit.csitapp.utils.Constants;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.PDFViewerActivity;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class OldQuestionsFragment extends Fragment {


    private static final String TAG = "BlogFragment";
    private static final int STORAGE_PERMISSION_CODE = 100;


    private View view;
    private Context mContext;
    private FirebaseHelper mFirebaseHelper;
    private List<Question> mList;
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private QARecyclerAdapter adapter;
    private SwipeRefreshLayout refresh;
    private boolean isQues;

    private Subject selected_subject;
    private RelativeLayout empty_container;
    private String filePath;
    private String fileName;

    private String currentformatSol;
    private String currentformatQues;
    private SharedPreferenceHelper preferenceHelper;


    public OldQuestionsFragment() {
        super();
        setArguments(new Bundle());
    }

    public static Fragment setInstance(boolean isQues, Subject subject) {
        OldQuestionsFragment fragment = new OldQuestionsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.ISQUES, isQues);
        bundle.putParcelable(Constants.SUBJECT, subject);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recycler_view_container, container, false);
        mContext = getContext();

        preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
        getArgs();
        setupFirebase();
        setupWidgets();

        setCurrentFormat();

        setupAdapter();


        if (isQues)
            loadQuestions();
        else
            loadSolutions();



        return view;
    }

    private void setCurrentFormat() {

        if (preferenceHelper.getSyllabusFormat() == 0){

            Log.d(TAG, "setCurrentFormat: "+FilePaths.SOLUTIONS);
            currentformatSol = FilePaths.SOLUTIONS;
            currentformatQues = FilePaths.QUESTIONS;
        }else {
            Log.d(TAG, "setCurrentFormat: "+FilePaths.SOLUTIONS_NEW);
            currentformatSol = FilePaths.SOLUTIONS_NEW;
            currentformatQues = FilePaths.QUESTIONS_NEW;

        }
    }

    private void getArgs() {
        Bundle bundle = getArguments();
        isQues = bundle.getBoolean(Constants.ISQUES);
        selected_subject = bundle.getParcelable(Constants.SUBJECT);
    }

    private void loadQuestions() {


        refresh.setRefreshing(true);
        Log.d(TAG, "loadQuestions: loading...");
        mFirebaseHelper.getMyRef().child(currentformatQues)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        mList.clear();
                        Log.d(TAG, "onDataChange: "+dataSnapshot.toString());
                        for (DataSnapshot ds :
                                dataSnapshot.getChildren()) {

                            Question question = ds.getValue(Question.class);
                            assert question != null;
                            if (question.getSubjectName().equals(selected_subject.getSubjectName()))
                                mList.add(question);
                        }
                        adapter.notifyDataSetChanged();
                        refresh.setRefreshing(false);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
                        refresh.setRefreshing(false);
                    }
                });
    }

    private void loadSolutions() {


        refresh.setRefreshing(true);

        mFirebaseHelper.getMyRef().child(currentformatSol)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        mList.clear();
                        for (DataSnapshot ds :
                                dataSnapshot.getChildren()) {

                            Question solution = ds.getValue(Question.class);
                            assert solution != null;
                            if (String.valueOf(solution.getSubjectName()).equals(selected_subject.getSubjectName()))
                                mList.add(solution);
                        }
                        if (mList.size() == 0){
                            empty_container.setVisibility(View.VISIBLE);
                        }else {
                            empty_container.setVisibility(View.GONE);
                        }
                        adapter.notifyDataSetChanged();

                        Log.d(TAG, "onDataChange: false");
                        refresh.setRefreshing(false);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
                        refresh.setRefreshing(false);
                    }
                });


    }

    private void setupTempData() {

    }


    private void setupWidgets() {

        empty_container = view.findViewById(R.id.empty_container);

        refresh = view.findViewById(R.id.refresh);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (isQues)
                    loadQuestions();
                else
                    loadSolutions();
            }
        });

    }


    private void setupAdapter() {
        mList = new ArrayList<>();


        recyclerView = view.findViewById(R.id.recyclerView);

        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);


        adapter = new QARecyclerAdapter(mContext, mList, new QARecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Question question) {

                filePath = question.getFilePath();
                fileName = question.getFileName();
                if (checkFileInStorage()){
                    loadPDF();
                }else {
                    checkStoragePermissionNDownload();
                }
            }
        });

        recyclerView.setAdapter(adapter);
    }




    private void checkStoragePermissionNDownload() {

        downloadFile();

    }





    private void setupFirebase() {
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
    }

    private void loadPDF() {
        if (filePath.contains("doc") || filePath.contains("docx")) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(new File(FilePaths.getSaveDir() + FilePaths.getNameFromUrl(filePath)));

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
            intent.setDataAndType(uri, "application/msword");

        } else {
            Intent in = new Intent(mContext, PDFViewerActivity.class);

            in.putExtra(mContext.getString(R.string.pdf_view),
                    FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName) + ".pdf");
            startActivity(in);
        }

    }

    private boolean checkFileInStorage() {
        Log.d(TAG, "checkFileInStorage: " + fileName);
        Log.d(TAG, "checkFileInStorage: full path" + FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName));
        File file = new File(FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName) + ".pdf");
        return file.exists();
    }

    private void downloadFile() {

        if (((MainActivityV2) mContext).hasInternetConnection()) {
            BroadCastReceiverActivity broadCastReceiverActivity = new BroadCastReceiverActivity(mContext, new BroadCastReceiverActivity.onClick() {
                @Override
                public void onResult(int progress) {

                }
            });
            broadCastReceiverActivity.registerReceiver();
            broadCastReceiverActivity.downloadFile(filePath, fileName);
        } else {
            Toast.makeText(mContext, "You are not connected to the internet.", Toast.LENGTH_SHORT).show();
        }


    }

}
