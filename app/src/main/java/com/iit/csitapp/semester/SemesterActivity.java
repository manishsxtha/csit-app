package com.iit.csitapp.semester;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Semester;
import com.iit.csitapp.startup.LoginActivity;
import com.iit.csitapp.syllabus.SyllabusActivity;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SemesterActivity extends AppCompatActivity {
    private static final String TAG = "SemesterActivity";
    public static final String TOOLBAR_TITLE = "Semesters";

    private Context mContext = SemesterActivity.this;
    private RecyclerView recyclerView;

    private LinearLayoutManager manager;
    private SemesterRecyclerAdapter adapter;
    private FirebaseHelper firebaseHelper;
    private SwipeRefreshLayout refresh;
    private List<Semester> mSemesters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_semester);

        setupToolbar();
        setupFirebase();
        setupAdapter();
        populateSemesters();

    }

    private void populateSemesters() {
        for (int i = 1; i < 9; i++) {
            mSemesters.add(new Semester(String.valueOf(i), "Semester " + i));
        }
        adapter.notifyDataSetChanged();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setTitle(TOOLBAR_TITLE);
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        } else {
            getSupportActionBar().setTitle(TOOLBAR_TITLE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
//                startActivity(new Intent(mContext, SyllabusActivity.class));
            }
        });


    }

    private void setupFirebase() {
        firebaseHelper = new FirebaseHelper(mContext);
        updateUI(firebaseHelper.getAuth().getCurrentUser());
    }

    private void setupAdapter() {
        mSemesters = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView);
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, 2);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        adapter = new SemesterRecyclerAdapter(mContext, mSemesters, new SemesterRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Semester sem) {
                Toast.makeText(mContext, sem.getSemester_name() + " selected.", Toast.LENGTH_SHORT).show();

                SharedPreferenceHelper sharedPreferenceHelper = SharedPreferenceHelper.getInstance(mContext);
                sharedPreferenceHelper.setSemester(Integer.parseInt(sem.getSemester_id()));
                finish();
                startActivity(new Intent(mContext, SyllabusActivity.class));
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser == null) {
            Log.d(TAG, "updateUI: User not signed in!");
            Intent in = new Intent(this, LoginActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(in);
            finish();
        }
    }
}
