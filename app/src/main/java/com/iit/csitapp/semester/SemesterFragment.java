package com.iit.csitapp.semester;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.iit.csitapp.MainActivity;
import com.iit.csitapp.MainActivityV2;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Semester;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

public class SemesterFragment extends Fragment {
    private static final String TAG = "SemesterActivity";
    public static final String TOOLBAR_TITLE = "Semesters";

    private Context mContext;
    private RecyclerView recyclerView;

    private LinearLayoutManager manager;
    private SemesterRecyclerAdapter adapter;
    private FirebaseHelper firebaseHelper;
    private SwipeRefreshLayout refresh;
    private List<Semester> mSemesters;
    private View view;
    private FragmentManager fragmentManager;
    private OnSemesterChange mCallback;


    public static SemesterFragment instance = null;
    private int page;
    private int callFromFragment;

    public SemesterFragment() {

    }

    public static SemesterFragment getInstance() {
        if (instance == null) {
            instance = new SemesterFragment();

        }
        return instance;
    }


    public static Fragment setInstance(int page, int callFromFragment) {
        SemesterFragment semesterFragment = new SemesterFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("page", page);
        bundle.putInt("callFrom", callFromFragment);
        semesterFragment.setArguments(bundle);

        return semesterFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_semester, container, false);
        mContext = getContext();

        getArgs();
        setupToolbar();
        setupFirebase();
        setupAdapter();
        populateSemesters();

        return view;
    }

    private void getArgs() {
        Bundle bundle = getArguments();
        page = bundle.getInt("page");

        callFromFragment = bundle.getInt("callFrom");

    }


    private void populateSemesters() {
        for (int i = 1; i < 9; i++) {
            mSemesters.add(new Semester(String.valueOf(i), "Semester " + i));
        }
        adapter.notifyDataSetChanged();
    }

    private void setupToolbar() {
        if (mContext instanceof MainActivity)
            ((MainActivity)mContext).setToolbar(mContext.getString(R.string.semester), true);
        else
            ((MainActivityV2)mContext).setToolbar(mContext.getString(R.string.semester), true);

    }

    private void setupFirebase() {
        firebaseHelper = new FirebaseHelper(mContext);
    }

    private void setupAdapter() {
        mSemesters = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, 2);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        adapter = new SemesterRecyclerAdapter(mContext, mSemesters, new SemesterRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Semester sem) {
                Toast.makeText(mContext, sem.getSemester_name() + " selected.", Toast.LENGTH_SHORT).show();

                SharedPreferenceHelper sharedPreferenceHelper = SharedPreferenceHelper.getInstance(mContext);
                sharedPreferenceHelper.setSemester(Integer.parseInt(sem.getSemester_id()));

                mCallback.onClick(page, callFromFragment);
                getFragmentManager().popBackStackImmediate();
            }
        });
        recyclerView.setAdapter(adapter);
    }

    public interface OnSemesterChange {
             void onClick(int page, int callFromFrament);
    }

    // This method insures that the Activity has actually implemented our
    // listener and that it isn't null.
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSemesterChange) {
            mCallback = (OnSemesterChange) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGreenFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

}
