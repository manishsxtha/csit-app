package com.iit.csitapp.semester;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.pavlospt.roundedletterview.RoundedLetterView;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Semester;
import com.iit.csitapp.utils.RandomColor;
import com.iit.csitapp.utils.RomanNumber;

import java.util.List;


public class SemesterRecyclerAdapter extends RecyclerView.Adapter<SemesterRecyclerAdapter.ViewHolder> {


    private List<Semester> semesters;
    private Context context;
    private OnPostClick onPostClick;


    public SemesterRecyclerAdapter(Context context, List<Semester> semesters, OnPostClick onPostClick){

        this.context = context;
        this.semesters = semesters;
        this.onPostClick = onPostClick;

    }
    @NonNull
    @Override
    public SemesterRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup
                                                                             parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_semester, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SemesterRecyclerAdapter.ViewHolder holder, final int position) {
        holder.rlv_name_view.setTitleText(RomanNumber.toRoman(position+1));
        holder.rlv_name_view.setBackgroundColor(RandomColor.getRandomMaterialColor(context,"400"));

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onPostClick.onClick(semesters.get(position));

            }
        });
        holder.tv_semester_name.setText(semesters.get(position).getSemester_name());
    }

    @Override
    public int getItemCount() {
        if (semesters != null){
            return semesters.size();
        }
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;
        private TextView tv_semester_name;
        private RoundedLetterView rlv_name_view;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.cardview);
            rlv_name_view = itemView.findViewById(R.id.rlv_name_view);
            tv_semester_name = itemView.findViewById(R.id.semester_name);
        }
    }

    public interface OnPostClick{
        public void onClick(Semester sem);
    }

}
