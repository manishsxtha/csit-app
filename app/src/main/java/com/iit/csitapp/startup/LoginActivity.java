package com.iit.csitapp.startup;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.iit.csitapp.MainActivityV2;
import com.iit.csitapp.R;
import com.iit.csitapp.models.User;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.SharedPreferenceHelper;
import com.iit.csitapp.utils.UserTypes;

import java.util.Arrays;


public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int RC_SIGN_IN = 1;
    private Context mContext = LoginActivity.this;

    //widgets
    private ProgressBar mProgressBar;

    //vars
    private FirebaseHelper mFirebaseHelper;
    private View view;
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager mCallbackManager;
    private LoginManager fbLoginManager;
    private SharedPreferenceHelper sharedHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setupWidgets();
        setupFirebase();


    }

    private void setupFirebase() {
        mFirebaseHelper = new FirebaseHelper(mContext);

        sharedHelper = SharedPreferenceHelper.getInstance(mContext);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // [END config_signin]

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void setupWidgets() {


        mProgressBar = findViewById(R.id.progressBar);

        mCallbackManager = CallbackManager.Factory.create();
        fbLoginManager = com.facebook.login.LoginManager.getInstance();

        findViewById(R.id.btn_google_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
//        facebook login
//        AppCompatButton buttonFbLogin = findViewById(R.id.btn_fb_login);
        findViewById(R.id.btn_fb_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fbLoginManager.logInWithReadPermissions(LoginActivity.this, Arrays.asList("email", "public_profile"));
            }
        });

        fbLoginManager.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                // here write code When login successfully
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                updateUI(null);
            }

            @Override
            public void onError(FacebookException e) {
                Log.d(TAG, "facebook:onError", e);
                // here write code when get error
                updateUI(null);
            }
        });

        hideProgressBar();
    }


    public void hideProgressBar() {
        mProgressBar.setVisibility(view.GONE);
    }

    public void showProgressBar() {
        mProgressBar.setVisibility(view.VISIBLE);
    }


    // [START auth_with_facebook]
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        // [START_EXCLUDE silent]
        showProgressBar();
        // [END_EXCLUDE]

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mFirebaseHelper.getAuth().signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            final FirebaseUser user = mFirebaseHelper.getAuth().getCurrentUser();

                            mFirebaseHelper.getMyRef().child("users")
                                    .orderByChild("user_id")
                                    .equalTo(user.getUid())
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {

                                                for (DataSnapshot ds :
                                                        dataSnapshot.getChildren()) {

                                                    User user1 = new User(user.getUid(),
                                                            user.getDisplayName(),
                                                            user.getPhotoUrl().toString(),
                                                            ds.getValue(User.class).getUser_type(),
                                                            user.getEmail()
                                                    );

                                                    sharedHelper.saveUserInfo(user1);

                                                }
                                            } else {
                                                User user1 = new User(user.getUid(),
                                                        user.getDisplayName(),
                                                        user.getPhotoUrl().toString() + "?type=large",
                                                        UserTypes.NORMAL,
                                                        user.getEmail()
                                                );
                                                sharedHelper.saveUserInfo(user1);
                                                mFirebaseHelper.getMyRef().child("users").child(user.getUid())
                                                        .setValue(user1);
                                            }
                                            updateUI(user);
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // [START_EXCLUDE]
                        hideProgressBar();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END auth_with_facebook]

    // [START onactivityresult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // [START_EXCLUDE]
                updateUI(null);
                // [END_EXCLUDE]
            }
        }
    }

    // [END onactivityresult]

    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        // [START_EXCLUDE silent]
        showProgressBar();
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mFirebaseHelper.getAuth().signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");



                            final FirebaseUser user = mFirebaseHelper.getAuth().getCurrentUser();
                            Log.e(TAG, "onComplete: firebase user:" + user.getUid());
                            //store the information from google sign-in in firebase database
                            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(mContext);

                            if (acct != null) {
                                final String personName = acct.getDisplayName();
                                final String personEmail = acct.getEmail();
                                final Uri personPhoto = acct.getPhotoUrl();
                                mFirebaseHelper.getMyRef().child("users")
                                        .child(user.getUid())
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                if (dataSnapshot.exists()) {
                                                    Log.e(TAG, "onDataChange: datasnap" + dataSnapshot);
//                                                    for (DataSnapshot ds :
//                                                            dataSnapshot.getChildren()) {
//                                                        Log.e(TAG, "onDataChange: children"+ds.toString() );
                                                    User user1 = new User(user.getUid(),
                                                            personName,
                                                            personPhoto.toString(),
                                                            dataSnapshot.getValue(User.class).getUser_type(),
                                                            personEmail
                                                    );
                                                    sharedHelper.saveUserInfo(user1);

//                                                    }
                                                } else {
                                                    Log.d(TAG, "onDataChange: Creating new user");
                                                    User user1 = new User(user.getUid(),
                                                            personName,
                                                            personPhoto.toString(),
                                                            UserTypes.NORMAL,
                                                            personEmail
                                                    );
                                                    sharedHelper.saveUserInfo(user1);
                                                    mFirebaseHelper.getMyRef().child("users").child(user.getUid())
                                                            .setValue(user1);

                                                }
                                                updateUI(user);
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });


                            }


                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // [START_EXCLUDE]
                        hideProgressBar();
                        // [END_EXCLUDE]
                    }
                });
    }

    // [END auth_with_google]

    private void updateUI(FirebaseUser user) {
        hideProgressBar();
        if (user != null) {
            initFCM();
            Intent intent = new Intent(this, MainActivityV2.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }



    private void sendRegistrationToServer(String token) {
        Log.d(TAG, "sendRegistrationToServer: sending token to server: " + token);
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child(FilePaths.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(getString(R.string.field_messaging_token))
                .setValue(token);
    }


    private void initFCM(){
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "initFCM: token: " + token);
        sendRegistrationToServer(token);

    }




}
