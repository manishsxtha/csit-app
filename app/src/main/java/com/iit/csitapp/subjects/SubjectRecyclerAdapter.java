package com.iit.csitapp.subjects;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.pavlospt.roundedletterview.RoundedLetterView;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Subject;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.RandomColor;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.util.List;

import static com.iit.csitapp.utils.SharedPreferenceHelper.getInstance;


public class SubjectRecyclerAdapter extends RecyclerView.Adapter<SubjectRecyclerAdapter.ViewHolder> {

    private static final String TAG = "SyllabusRecyclerAdapter";
    public final SharedPreferenceHelper sharedPreferences;
    private final FirebaseHelper mFirebaseHelper;

    private List<Subject> mList;
    private Context context;
    private OnPostClick onClickListener;


    public SubjectRecyclerAdapter(Context context, List<Subject> mList, OnPostClick listener) {

        this.context = context;
        this.mList = mList;
        onClickListener = listener;


        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(context);
        sharedPreferences = getInstance(context);

    }

    @NonNull
    @Override
    public SubjectRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_subjects, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SubjectRecyclerAdapter.ViewHolder holder, final int position) {
        final Subject post = mList.get(holder.getAdapterPosition());


        holder.rlv_name_view.setTitleText(post.getSubjectName().substring(0, 1));

        holder.rlv_name_view.setBackgroundColor(RandomColor.getRandomMaterialColor(context, "400"));

        holder.title.setText(post.getSubjectName());
        holder.semester_name.setText("Sem "+mList.get(position).getSemester());
        holder.subject_code.setText(mList.get(position).getSubjectCode());
        holder.subject_type.setText(mList.get(position).getType());

    }


    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RoundedLetterView rlv_name_view;
        private TextView title, semester_name,subject_code, subject_type;


        public ViewHolder(@NonNull final View itemView) {
            super(itemView);


            rlv_name_view = itemView.findViewById(R.id.rlv_name_view);
            title = itemView.findViewById(R.id.title);
            semester_name = itemView.findViewById(R.id.semester_name);
            subject_code = itemView.findViewById(R.id.subject_code);
            subject_type = itemView.findViewById(R.id.subject_type);

//         
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (onClickListener != null && position != RecyclerView.NO_POSITION) {
                        onClickListener.onClick(mList.get(position));
                    }
                }
            });
        }
    }

    public interface OnPostClick {
        void onClick(Subject subject);
    }


}
