package com.iit.csitapp.subjects;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.MainActivity;
import com.iit.csitapp.MainActivityV2;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Subject;
import com.iit.csitapp.questionNsolutions.fragments.OldQuestionsFragment;
import com.iit.csitapp.semester.SemesterFragment;
import com.iit.csitapp.utils.Constants;
import com.iit.csitapp.utils.DatabaseHelper;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.iit.csitapp.utils.NavigationTabs.NOTES;
import static com.iit.csitapp.utils.NavigationTabs.QNA;

public class SubjectsFragment extends Fragment {


    private static final String TAG = "SubjectsFragment";


    private View view;
    private Context mContext;
    private FirebaseHelper mFirebaseHelper;

    private List<Subject> mList;
    private List<Subject> mTempList;

    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private SubjectRecyclerAdapter adapter;
    private SwipeRefreshLayout refresh;
    private TextView tv_change_sem;
    private Bundle bundle;
    private int type;
    private DatabaseHelper databaseHelper;
    private SharedPreferenceHelper preferenceHelper;
    private int callFrom;
    private String currentFormat;
    private TextView semester_header;


    public SubjectsFragment() {
        super();
        setArguments(new Bundle());
    }

    public static Fragment newInstance() {
        OldQuestionsFragment fragment = new OldQuestionsFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_subjects, container, false);
        mContext = getContext();


        databaseHelper = DatabaseHelper.getInstance(mContext);
        preferenceHelper = SharedPreferenceHelper.getInstance(mContext);

        setupToolbar();
        setupFirebase();


        getArgs();
        setupWidgets();

        setCurrentFormat();
        setupAdapter();
        //check internet connection, if no connection get it from database
        checkInternetConnection();


        return view;
    }

    private void setCurrentFormat() {
        if (preferenceHelper.getSyllabusFormat() == 0)
            currentFormat = FilePaths.SUBJECTS;
        else
            currentFormat = FilePaths.SUBJECTS_NEW;
    }


    private void getArgs() {
        bundle = getArguments();

        type = bundle.getInt("type");
        callFrom = bundle.getInt("callFrom");
    }

    private void setupToolbar() {


        if (mContext instanceof MainActivity)
            ((MainActivity) mContext).setToolbar("Semester " + SharedPreferenceHelper.getInstance(mContext).getSemester());

    }

    private void checkInternetConnection() {
        refresh.setRefreshing(true);
//        boolean valid;
//        if (mContext instanceof MainActivity)
//            valid = ((MainActivity) mContext).hasInternetConnection();
//        else
//            valid = ((MainActivityV2) mContext).hasInternetConnection();
//        if (valid) {


                mFirebaseHelper.getMyRef().child(currentFormat).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    mTempList.clear();




                        for (DataSnapshot ds :
                                dataSnapshot.getChildren()) {

                            Subject subject = ds.getValue(Subject.class);
                            Log.d(TAG, "onDataChange: all subj" + subject.toString());
                            assert subject != null;

                            if (String.valueOf(subject.getSemester()).equals(preferenceHelper.getSemester())) {
                                mTempList.add(subject);
                                Log.d(TAG, "onDataChange: subjects: " + subject.toString());
                            }

                            //add to the internal database as well
                            databaseHelper.setSubject(subject);

                        }
                        preferenceHelper.setShareKeySubjectSize((int) dataSnapshot.getChildrenCount());
                        loadSubjectsData();
//                    }

                    refresh.setRefreshing(false);
                }


                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
                    loadSubjectFromDatabase();
                    refresh.setRefreshing(false);

                }
            });

    }

    private void loadSubjectFromDatabase() {
        mList.clear();


        mList.addAll(databaseHelper.getSemesterSubjects(preferenceHelper.getSemester()));
        sortList();
        adapter.notifyDataSetChanged();

    }

    private void sortList() {
        Collections.sort(mList, new Comparator<Subject>() {
            @Override
            public int compare(Subject o1, Subject o2) {
                return o2.getSubjectName().compareTo(o1.getSubjectName());
            }
        });
    }


    private void loadSubjectsData() {


        semester_header.setText("Semester "+preferenceHelper.getSemester());
        refresh.setRefreshing(true);
        mList.clear();

        //first clear all the subjects table in database
        mList.addAll(mTempList);
        sortList();
        adapter.notifyDataSetChanged();

    }

    private void setupWidgets() {
        semester_header = view.findViewById(R.id.semester_header);
        semester_header.setText("Semester "+preferenceHelper.getSemester());
        refresh = view.findViewById(R.id.refresh);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                checkInternetConnection();
            }
        });

        tv_change_sem = view.findViewById(R.id.tv_change_sem);

        tv_change_sem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frame, SemesterFragment.setInstance(Constants.SUBJECT_CODE, callFrom))
                        .addToBackStack(mContext.getString(R.string.calling_subject))
                        .commit();
            }
        });

    }


    private void setupAdapter() {
        mList = new ArrayList<>();
        mTempList = new ArrayList<>();

        recyclerView = view.findViewById(R.id.recyclerView);

        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);

        adapter = new SubjectRecyclerAdapter(mContext, mList, new SubjectRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Subject subject) {
                if (type == NOTES) {
                    if (mContext instanceof MainActivity)
                    ((MainActivity) mContext).loadNotesFragment(subject);
                    else
                        ((MainActivityV2) mContext).loadNotesFragment(subject);
                } else if (type == QNA) {
                    if (mContext instanceof MainActivity)
                        ((MainActivity) mContext).loadQAFragment(subject);
                    else
                        ((MainActivityV2) mContext).loadQAFragment(subject);
                } else {
                    if (mContext instanceof MainActivity)
                        ((MainActivity) mContext).loadModelQFragment(subject);
                    else
                        ((MainActivityV2) mContext).loadModelQFragment(subject);
                }
            }
        });

        recyclerView.setAdapter(adapter);
    }


    private void setupFirebase() {
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
    }

    public void onClick() {
        if (mContext instanceof MainActivity)
            ((MainActivity) mContext).changeToNavigationDrawer();
        else
            ((MainActivityV2) mContext).changeToNavigationDrawer();
        loadSubjectsData();
    }


}
