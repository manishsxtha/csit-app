package com.iit.csitapp.syllabus;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Syllabus;
import com.iit.csitapp.questionNsolutions.QnAActivity;
import com.iit.csitapp.semester.SemesterActivity;
import com.iit.csitapp.startup.LoginActivity;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.NavigationTabs;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class SyllabusActivity extends AppCompatActivity {
    private static final String TAG = "SyllabusActivity";
    private static final int WRITE_REQUEST_CODE = 300;

    private AHBottomNavigation bottomNavigation;
    private Context mContext = SyllabusActivity.this;
    private RecyclerView recyclerView;
    private List<Syllabus> mList;
    private LinearLayoutManager manager;
    private SyllabusRecyclerAdapter adapter;
    private FirebaseHelper firebaseHelper;
    private SwipeRefreshLayout refresh;
    private TextView tv_change_sem;
    private String filePath;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syllabus);


        setupToolbar();
        setupFirebase();

        setupWidgets();
        setupAdapter();
        loadSyllabusData();

        setupBottomNavigation(NavigationTabs.SYLLABUS);
    }

    private void setupWidgets() {
        webView = findViewById(R.id.webView);
        tv_change_sem = findViewById(R.id.tv_change_sem);

        tv_change_sem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(mContext, SemesterActivity.class));
            }
        });
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setTitle("Syllabus");
        } else {
            getSupportActionBar().setTitle("Syllabus");
        }


    }

    private void setupFirebase() {
        firebaseHelper = new FirebaseHelper(mContext);
        updateUI(firebaseHelper.getAuth().getCurrentUser());
    }

    /**
     * get semester id from shared preferences and filter the datasnapshot by showing only the syllabus
     * of that semester
     */
    private void loadSyllabusData() {
        refresh.setRefreshing(true);
        final SharedPreferenceHelper preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
        firebaseHelper.getMyRef().child(FilePaths.SYLLABUS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        mList.clear();
                        for (DataSnapshot ds :
                                dataSnapshot.getChildren()) {

                            Syllabus syllabus = ds.getValue(Syllabus.class);
                            if (syllabus.getSemester().equals(preferenceHelper.getSemester()))
                                mList.add(syllabus);
                        }
                        adapter.notifyDataSetChanged();
                        refresh.setRefreshing(false);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
                        refresh.setRefreshing(false);
                    }
                });
    }

    private void setupAdapter() {

        mList = new ArrayList<>();

        refresh = findViewById(R.id.refresh);

        recyclerView = findViewById(R.id.recyclerView);
        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);


        recyclerView.setLayoutManager(manager);

        adapter = new SyllabusRecyclerAdapter(mContext, mList, new SyllabusRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Syllabus syllabus) {

            }
        }, new SyllabusRecyclerAdapter.OnDownloadClick() {
            @Override
            public void onClick(Syllabus syllabus) {
                filePath = syllabus.getFilePath();
                GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(mContext);
//
//                GoogleAccountCredential credential =
//                        GoogleAccountCredential.usingOAuth2(
//                                mContext, Collections.singleton(DriveScopes.DRIVE_READONLY));
//                credential.setSelectedAccount(account.getAccount());
//                com.google.api.services.drive.Drive googleDriveService =
//                        new com.google.api.services.drive.Drive.Builder(
//                                AndroidHttp.newCompatibleTransport(),
//                                new GsonFactory(),
//                                credential)
//                                .setApplicationName("AppName")
//                                .build();
//                DriveServiceHelper mDriveServiceHelper = new DriveServiceHelper(googleDriveService);
//                String fileId = "1ZdR3L3qP4Bkq8noWLJHSr_iBau0DNT4Kli4SxNc2YEo";
//                mDriveServiceHelper.exportDoc(fileId);

//                webView.loadUrl(filePath);
//                downloadFile();
//                downloadFileV3();
//                downloadFileV2(syllabus.getFilePath());
            }
        });
        recyclerView.setAdapter(adapter);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadSyllabusData();
            }
        });
    }

    private void downloadFileV3() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(filePath));
        startActivity(browserIntent);
    }


    private void setupBottomNavigation(int position) {


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);

// Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_1, R.drawable.ic_settings_white, R.color.black);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.ic_settings_white, R.color.black);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.ic_settings_white, R.color.black);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.ic_settings_white, R.color.light_grey);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.tab_5, R.drawable.ic_settings_white, R.color.light_grey);

// Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);

        // Set background color
        bottomNavigation.setDefaultBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
        // Manage titles
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

        // Use colored navigation with circle reveal effect
        bottomNavigation.setColored(false);
        bottomNavigation.setUseElevation(false);
        bottomNavigation.setBehaviorTranslationEnabled(false);

// Set current item programmatically
        bottomNavigation.setCurrentItem(position);

        // Customize notification (title, background, typeface)
        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                // Do something cool here...

                if (wasSelected){
                    return true;
                }
                switch (position) {
                    case NavigationTabs
                            .HOME:


                        break;

                    case NavigationTabs.QNA:
                        startActivity(new Intent(mContext, QnAActivity.class));
                        break;
                    case NavigationTabs.SYLLABUS:
                        startActivity(new Intent(mContext, Syllabus.class));
                        break;
                    case NavigationTabs.POSTS:

                        break;
//                    case NavigationTabs.NOTIFICATION:
//
//                        break;
                    default:

                        break;

                }
                return true;
            }
        });
    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser == null) {
            Log.d(TAG, "updateUI: User not signed in!");
            Intent in = new Intent(this, LoginActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(in);
            finish();
        }
    }



}
