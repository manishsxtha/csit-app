package com.iit.csitapp.syllabus;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.MainActivity;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Syllabus;
import com.iit.csitapp.semester.SemesterFragment;
import com.iit.csitapp.utils.Constants;
import com.iit.csitapp.utils.DatabaseHelper;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.NotificationDetailActivity;
import com.iit.csitapp.utils.PDFViewerActivity;
import com.iit.csitapp.utils.SharedPreferenceHelper;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2core.DownloadBlock;
import com.tonyodev.fetch2core.Extras;
import com.tonyodev.fetch2core.MutableExtras;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SyllabusFragment extends Fragment implements FetchListener {
    private static final String TAG = "SyllabusActivity";
    private static final int WRITE_REQUEST_CODE = 300;
    private static final int STORAGE_PERMISSION_CODE = 100;


    private AHBottomNavigation bottomNavigation;
    private Context mContext;
    private RecyclerView recyclerView;
    private List<Syllabus> mList;
    private LinearLayoutManager manager;
    private SyllabusRecyclerAdapter adapter;
    private FirebaseHelper firebaseHelper;
    private SwipeRefreshLayout refresh;
    private TextView tv_change_sem;
    private WebView webView;
    private View view;

    public static SyllabusFragment instance = null;
    private Fetch fetch;
    private Request request;
    private DatabaseHelper databaseHelper;
    private SharedPreferenceHelper preferenceHelper;
    private List<Syllabus> mTempList;
    private RelativeLayout empty_container;
    private boolean isDownloadOnly;
    private String fileName;
    private String filePath;


    public SyllabusFragment(){

    }

    public static SyllabusFragment getInstance(){
        if (instance == null){
            instance = new SyllabusFragment();
        }
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_syllabus, container, false);
        mContext = getContext();

        firebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
        databaseHelper = DatabaseHelper.getInstance(mContext);
        preferenceHelper = SharedPreferenceHelper.getInstance(mContext);

        FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(mContext )
                .setDownloadConcurrentLimit(3)
                .build();
        fetch = Fetch.Impl.getInstance(fetchConfiguration);


        setupToolbar();
        setupWidgets();
        setupAdapter();

        checkInternetConnection();
//        loadSyllabusData();


        return view;
    }


    private void checkStoragePermissionNDownload() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        } else {
            downloadFile(filePath);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            downloadFile(filePath);
        } else {
            Toast.makeText(mContext, mContext.getString(R.string.error_permission), Toast.LENGTH_SHORT).show();
        }
    }

    private void setupWidgets() {
        webView = view.findViewById(R.id.webView);
        tv_change_sem = view.findViewById(R.id.tv_change_sem);

        empty_container = view.findViewById(R.id.empty_container);

        tv_change_sem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frame, SemesterFragment.setInstance(Constants.SYLLABUS_CODE, Constants.SEMESTER_CODE))
                        .addToBackStack(mContext.getString(R.string.nav_sem))
                        .commit();

            }
        });

    }

    private void setupToolbar() {
        ((MainActivity) mContext).setToolbar(mContext.getString(R.string.syllabus));

    }


    private void checkInternetConnection() {
        refresh.setRefreshing(true);
        Log.e(TAG, "checkInternetConnection: hehje" );
        if (((MainActivity) mContext).hasInternetConnection()) {
            firebaseHelper.getMyRef().child(FilePaths.SYLLABUS).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    mTempList.clear();
//
//                    if (preferenceHelper.getShareKeySyllabusSize() == dataSnapshot.getChildrenCount()) {
//                        loadSyllabusFromDatabase();
//                    } else {

                    Log.e(TAG, "onDataChange: "+dataSnapshot.toString() );

                        databaseHelper.clearSubjectData();
                        for (DataSnapshot ds :
                                dataSnapshot.getChildren()) {

                            Syllabus post= ds.getValue(Syllabus.class);
                            assert post != null;

                            if (String.valueOf(post.getSemester()).equals(preferenceHelper.getSemester())) {
                                mTempList.add(post);
                            }

                            //add to the internal database as well
                            databaseHelper.setSyllabus(post);

                        }
                        preferenceHelper.setShareKeySyllabusSize((int) dataSnapshot.getChildrenCount());
                        loadSyllabusData();
//                    }


                    refresh.setRefreshing(false);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
                    loadSyllabusFromDatabase();
                    refresh.setRefreshing(false);

                }
            });

        } else {
            refresh.setRefreshing(false);
            loadSyllabusFromDatabase();

        }
    }

    private void loadSyllabusFromDatabase() {
        mList.clear();


        mList.addAll(databaseHelper.getAllSyllabus(preferenceHelper.getSemester()));
        checkContainerIsEmpty();
        sortList();
        adapter.notifyDataSetChanged();

    }

    private void sortList() {
        Collections.sort(mList, new Comparator<Syllabus>() {
            @Override
            public int compare(Syllabus o1, Syllabus o2) {
                return o2.getSubjectName().compareTo(o1.getSubjectName());
            }
        });
    }

    private void loadSyllabusData() {
        refresh.setRefreshing(true);
        mList.clear();

        //first clear all the subjects table in database
        mList.addAll(mTempList);
        checkContainerIsEmpty();
        sortList();
        adapter.notifyDataSetChanged();

    }

    private void checkContainerIsEmpty() {
        if (mList.size() == 0) {
            empty_container.setVisibility(View.VISIBLE);
        } else {
            empty_container.setVisibility(View.GONE);
        }
    }

//
//    private void loadSyllabusData() {
//        refresh.setRefreshing(true);
//        final SharedPreferenceHelper preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
//        firebaseHelper.getMyRef().child(FilePaths.SYLLABUS)
//                .addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                        mList.clear();
//                        for (DataSnapshot ds :
//                                dataSnapshot.getChildren()) {
//
//                            Syllabus syllabus = ds.getValue(Syllabus.class);
//                            if (syllabus.getSemester().equals(preferenceHelper.getSemester()))
//                                mList.add(syllabus);
//                        }
//                        adapter.notifyDataSetChanged();
//                        refresh.setRefreshing(false);
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//                        Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
//                        refresh.setRefreshing(false);
//                    }
//                });
//    }

    private void setupAdapter() {

        mList = new ArrayList<>();
        mTempList = new ArrayList<>();

        refresh = view.findViewById(R.id.refresh);

        recyclerView = view.findViewById(R.id.recyclerView);
        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);


        recyclerView.setLayoutManager(manager);

        adapter = new SyllabusRecyclerAdapter(mContext, mList, new SyllabusRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Syllabus syllabus) {
                filePath = syllabus.getFilePath();
                fileName = syllabus.getSubjectName();

                //check if file is in storage
                if (checkFileInStorage()){
                    loadPDF();
                }else {
                    checkStoragePermissionNDownload();
                }

                isDownloadOnly = false;


            }
        }, new SyllabusRecyclerAdapter.OnDownloadClick() {
            @Override
            public void onClick(Syllabus syllabus) {
                filePath = syllabus.getFilePath();
                fileName = syllabus.getSubjectName();

                checkStoragePermissionNDownload();
                isDownloadOnly = true;
            }
        });
        recyclerView.setAdapter(adapter);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                checkInternetConnection();
            }
        });
    }

    private void loadPDF() {
        Intent in = new Intent(mContext, PDFViewerActivity.class);
        in.putExtra(mContext.getString(R.string.pdf_view),
                FilePaths.getSaveDir()+FilePaths.getNameFromUrl(filePath));
        startActivity(in);
    }

    private boolean checkFileInStorage() {
        File file = new File(FilePaths.getSaveDir()+FilePaths.getNameFromUrl(filePath));

        return file.exists();
    }

    private void downloadFile(String  url) {

        final String filePath = FilePaths.getSaveDir()+ FilePaths.getNameFromUrl(url);
        request = new Request(url,filePath);

        request.setExtras(getExtrasForRequest(request));

        fetch.enqueue(request, updatedRequest -> {
            //Request was successfully enqueued for download.
            request = updatedRequest;
            Toast.makeText(mContext, mContext.getString(R.string.dowload_start), Toast.LENGTH_SHORT).show();
        }, error -> {
            //An error occurred enqueuing the request.
            Log.e(TAG, "downloadFile: "+error.toString() );
            Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();

        });

        fetch.addListener(this);


    }



    private Extras getExtrasForRequest(Request request) {
        final MutableExtras extras = new MutableExtras();
        extras.putBoolean("testBoolean", true);
        extras.putString("testString", "test");
        extras.putFloat("testFloat", Float.MIN_VALUE);
        extras.putDouble("testDouble",Double.MIN_VALUE);
        extras.putInt("testInt", Integer.MAX_VALUE);
        extras.putLong("testLong", Long.MAX_VALUE);
        return extras;
    }

    private void downloadFileV3() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(filePath));
        startActivity(browserIntent);
    }

    public void onClick() {
        ((MainActivity) mContext).changeToNavigationDrawer();
        loadSyllabusData();
    }

    @Override
    public void onAdded(@NotNull Download download) {
        showNotification(download.getId());
    }


    Notification.Builder builder;
    NotificationManagerCompat managerCompat;
    private void showNotification(int id) {


        NotificationChannel channel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            channel = new NotificationChannel("channel1","001",NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Downloading..");

            NotificationManager manager = (NotificationManager) ((MainActivity)mContext).getSystemService(Context.NOTIFICATION_SERVICE);
            manager.createNotificationChannel(channel);

            builder = new Notification.Builder(mContext, "channel1");
            builder.setContentTitle("Downloading file");
            builder.setAutoCancel(false);

            builder.setWhen(System.currentTimeMillis());
            builder.setProgress(100,0,false);
            builder.setSmallIcon(R.drawable.ic_download);

            managerCompat = NotificationManagerCompat.from(mContext);
            managerCompat.notify(id, builder.build());

        }else {
            builder = new Notification.Builder(mContext);
            builder.setContentTitle("Downloading file");
            builder.setSmallIcon(R.drawable.ic_download);
            builder.setAutoCancel(false);

            builder.setWhen(System.currentTimeMillis());
            builder.setPriority(Notification.PRIORITY_DEFAULT);
            builder.setProgress(100,0,false);

            managerCompat = NotificationManagerCompat.from(mContext);
            managerCompat.notify(id, builder.build());
        }

    }

    @Override
    public void onCancelled(@NotNull Download download) {

    }

    @Override
    public void onCompleted(@NotNull Download download) {
        Toast.makeText(mContext, "File downloaded.", Toast.LENGTH_SHORT).show();
        if (builder!=null){
            builder.setContentText("Download Finished");

            //set the activity when clicked
            // Create an Intent for the activity you want to start
            Intent resultIntent = new Intent(mContext, NotificationDetailActivity.class);
// Create the TaskStackBuilder and add the intent, which inflates the back stack
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
            stackBuilder.addNextIntentWithParentStack(resultIntent);
// Get the PendingIntent containing the entire back stack
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(resultPendingIntent);

            builder.setProgress(0,0,false);
            managerCompat.notify(download.getId(), builder.build());
        }

        if (!isDownloadOnly){

            loadPDF();

        }
    }

    @Override
    public void onDeleted(@NotNull Download download) {

    }

    @Override
    public void onDownloadBlockUpdated(@NotNull Download download, @NotNull DownloadBlock downloadBlock, int i) {

    }

    @Override
    public void onError(@NotNull Download download, @NotNull Error error, @org.jetbrains.annotations.Nullable Throwable throwable) {

    }

    @Override
    public void onPaused(@NotNull Download download) {

    }

    @Override
    public void onProgress(@NotNull Download download, long l, long l1) {
        int progress = download.getProgress();

        builder.setProgress(100, progress, false);
        managerCompat.notify(download.getId(), builder.build());
        Log.d(TAG, "onProgress: "+progress);
    }

    @Override
    public void onQueued(@NotNull Download download, boolean b) {

    }

    @Override
    public void onRemoved(@NotNull Download download) {

    }

    @Override
    public void onResumed(@NotNull Download download) {

    }

    @Override
    public void onStarted(@NotNull Download download, @NotNull List<? extends DownloadBlock> list, int i) {

    }

    @Override
    public void onWaitingNetwork(@NotNull Download download) {

    }
}
