package com.iit.csitapp.syllabus;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.MainActivity;
import com.iit.csitapp.MainActivityV2;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Syllabus;
import com.iit.csitapp.semester.SemesterFragment;
import com.iit.csitapp.utils.BroadCastReceiverActivity;
import com.iit.csitapp.utils.Constants;
import com.iit.csitapp.utils.DatabaseHelper;
import com.iit.csitapp.utils.FilePaths;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.PDFViewerActivity;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SyllabusFragmentV2 extends Fragment {
    private static final String TAG = "SyllabusActivity";

    private Context mContext;
    private RecyclerView recyclerView;
    private List<Syllabus> mList;
    private LinearLayoutManager manager;
    private SyllabusRecyclerAdapter adapter;
    private FirebaseHelper firebaseHelper;
    private SwipeRefreshLayout refresh;
    private TextView tv_change_sem;

    private View view;

    public static SyllabusFragmentV2 instance = null;

    private DatabaseHelper databaseHelper;
    private SharedPreferenceHelper preferenceHelper;
    private List<Syllabus> mTempList;
    private RelativeLayout empty_container;
    private boolean isDownloadOnly;
    private String fileName;
    private String filePath;
    private TextView semester_header;

    public SyllabusFragmentV2() {

    }

    public static SyllabusFragmentV2 getInstance() {
        if (instance == null) {
            instance = new SyllabusFragmentV2();
        }
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_syllabus, container, false);
        mContext = getContext();

        firebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
        databaseHelper = DatabaseHelper.getInstance(mContext);
        preferenceHelper = SharedPreferenceHelper.getInstance(mContext);

        setupToolbar();
        setupWidgets();
        setupAdapter();

        checkInternetConnection();
//        loadSyllabusData();


        return view;
    }


    private void checkStoragePermissionNDownload() {

            downloadFile();

    }

    private void setupWidgets() {
        semester_header = view.findViewById(R.id.semester_header);
        semester_header.setText("Semester "+preferenceHelper.getSemester());
        tv_change_sem = view.findViewById(R.id.tv_change_sem);

        empty_container = view.findViewById(R.id.empty_container);

        tv_change_sem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frame, SemesterFragment.setInstance(Constants.SYLLABUS_CODE, Constants.SEMESTER_CODE))
                        .addToBackStack(mContext.getString(R.string.nav_sem))
                        .commit();

            }
        });
    }

    private void setupToolbar() {
        if (mContext instanceof MainActivity)
            ((MainActivity) mContext).setToolbar(mContext.getString(R.string.syllabus));
        else
            ((MainActivityV2) mContext).setToolbar(mContext.getString(R.string.syllabus));

    }


    private void checkInternetConnection() {
        refresh.setRefreshing(true);
        boolean valid;
        if (mContext instanceof MainActivity)
            valid = ((MainActivity) mContext).hasInternetConnection();
        else
            valid = ((MainActivityV2) mContext).hasInternetConnection();
        if (valid) {
            if (preferenceHelper.getSyllabusFormat() == 0)
                firebaseHelper.getMyRef().child(FilePaths.SYLLABUS).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    mTempList.clear();
//
//                    if (preferenceHelper.getShareKeySyllabusSize() == dataSnapshot.getChildrenCount()) {
//                        loadSyllabusFromDatabase();
//                    } else {

                    Log.e(TAG, "onDataChange: " + dataSnapshot.toString());

                    databaseHelper.clearSyllabusData();
                    for (DataSnapshot ds :
                            dataSnapshot.getChildren()) {

                        Syllabus post = ds.getValue(Syllabus.class);
                        assert post != null;

                        if (String.valueOf(post.getSemester()).equals(preferenceHelper.getSemester())) {
                            mTempList.add(post);
                        }

                        //add to the internal database as well
                        databaseHelper.setSyllabus(post);

                    }
                    preferenceHelper.setShareKeySyllabusSize((int) dataSnapshot.getChildrenCount());
                    loadSyllabusData();
//                    }


                    refresh.setRefreshing(false);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
                    loadSyllabusFromDatabase();
                    refresh.setRefreshing(false);

                }
            });
            else
                firebaseHelper.getMyRef().child(FilePaths.SYLLABUS_NEW).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        mTempList.clear();
//
//                    if (preferenceHelper.getShareKeySyllabusSize() == dataSnapshot.getChildrenCount()) {
//                        loadSyllabusFromDatabase();
//                    } else {

                        Log.e(TAG, "onDataChange: " + dataSnapshot.toString());

                        databaseHelper.clearSyllabusData();
                        for (DataSnapshot ds :
                                dataSnapshot.getChildren()) {

                            Syllabus post = ds.getValue(Syllabus.class);
                            assert post != null;

                            if (String.valueOf(post.getSemester()).equals(preferenceHelper.getSemester())) {
                                mTempList.add(post);
                            }

                            //add to the internal database as well
                            databaseHelper.setSyllabus(post);

                        }
                        preferenceHelper.setShareKeySyllabusSize((int) dataSnapshot.getChildrenCount());
                        loadSyllabusData();
//                    }


                        refresh.setRefreshing(false);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
                        loadSyllabusFromDatabase();
                        refresh.setRefreshing(false);

                    }
                });

        } else {
            refresh.setRefreshing(false);
            loadSyllabusFromDatabase();

        }
    }

    private void loadSyllabusFromDatabase() {
        mList.clear();


        mList.addAll(databaseHelper.getAllSyllabus(preferenceHelper.getSemester()));
        checkContainerIsEmpty();
        sortList();
        adapter.notifyDataSetChanged();

    }

    private void sortList() {
        Collections.sort(mList, new Comparator<Syllabus>() {
            @Override
            public int compare(Syllabus o1, Syllabus o2) {
                return o2.getSubjectName().compareTo(o1.getSubjectName());
            }
        });
    }

    private void loadSyllabusData() {
        refresh.setRefreshing(true);
        mList.clear();
        semester_header.setText("Semester "+preferenceHelper.getSemester());
        //first clear all the subjects table in database
        mList.addAll(mTempList);
        checkContainerIsEmpty();
        sortList();
        adapter.notifyDataSetChanged();

    }

    private void checkContainerIsEmpty() {
        if (mList.size() == 0) {
            empty_container.setVisibility(View.VISIBLE);
        } else {
            empty_container.setVisibility(View.GONE);
        }
    }


    private void setupAdapter() {

        mList = new ArrayList<>();
        mTempList = new ArrayList<>();

        refresh = view.findViewById(R.id.refresh);

        recyclerView = view.findViewById(R.id.recyclerView);
        manager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);


        recyclerView.setLayoutManager(manager);

        adapter = new SyllabusRecyclerAdapter(mContext, mList, new SyllabusRecyclerAdapter.OnPostClick() {
            @Override
            public void onClick(Syllabus syllabus) {
                filePath = syllabus.getFilePath();
                fileName = syllabus.getSubjectName();

                //check if file is in storage
                if (checkFileInStorage()) {
                    loadPDF();
                } else {
                    checkStoragePermissionNDownload();
                }

                isDownloadOnly = false;


            }
        }, new SyllabusRecyclerAdapter.OnDownloadClick() {
            @Override
            public void onClick(Syllabus syllabus) {
                filePath = syllabus.getFilePath();
                fileName = syllabus.getSubjectName();

                if (checkFileInStorage()) {
                    loadPDF();
                } else {
                    checkStoragePermissionNDownload();
                }

                isDownloadOnly = true;
            }
        });
        recyclerView.setAdapter(adapter);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                checkInternetConnection();
            }
        });
    }

    private void loadPDF() {
        if (filePath.contains("doc") || filePath.contains("docx")) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(new File(FilePaths.getSaveDir() + FilePaths.getNameFromUrl(filePath)));

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
            intent.setDataAndType(uri, "application/msword");

        }else {
            Intent in = new Intent(mContext, PDFViewerActivity.class);

            in.putExtra(mContext.getString(R.string.pdf_view),
                    FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName)+".pdf");
            startActivity(in);
        }

    }

    private boolean checkFileInStorage() {
        Log.d(TAG, "checkFileInStorage: "+fileName);
        Log.d(TAG, "checkFileInStorage: full path"+FilePaths.getSaveDir()+FilePaths.getNameFromUrl(fileName));
        File file = new File(FilePaths.getSaveDir() + FilePaths.getNameFromUrl(fileName)+".pdf");
        return file.exists();
    }


    public void onClick() {
        if (mContext instanceof MainActivity)
            ((MainActivity) mContext).changeToNavigationDrawer();
        else
            ((MainActivityV2) mContext).changeToNavigationDrawer();
        loadSyllabusData();
    }


    private void downloadFile() {

        if (((MainActivityV2) mContext).hasInternetConnection()) {
            BroadCastReceiverActivity broadCastReceiverActivity = new BroadCastReceiverActivity(mContext, new BroadCastReceiverActivity.onClick() {
                @Override
                public void onResult(int progress) {

                }
            });
            broadCastReceiverActivity.registerReceiver();
            broadCastReceiverActivity.downloadFile(filePath, fileName);
        } else {
            Toast.makeText(mContext, "You are not connected to the internet.", Toast.LENGTH_SHORT).show();
        }


    }




}
