package com.iit.csitapp.syllabus;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.pavlospt.roundedletterview.RoundedLetterView;
import com.iit.csitapp.R;
import com.iit.csitapp.models.Syllabus;
import com.iit.csitapp.utils.FirebaseHelper;
import com.iit.csitapp.utils.SharedPreferenceHelper;

import java.util.List;

import static com.iit.csitapp.utils.SharedPreferenceHelper.getInstance;


public class SyllabusRecyclerAdapter extends RecyclerView.Adapter<SyllabusRecyclerAdapter.ViewHolder> {

    private static final String TAG = "SyllabusRecyclerAdapter";
    public final SharedPreferenceHelper sharedPreferences;
    private final FirebaseHelper mFirebaseHelper;

    private List<Syllabus> mList;
    private Context context;
    private OnPostClick onClickListener;
    private OnDownloadClick onDownloadClick;

    private String[] colors;

    public SyllabusRecyclerAdapter(Context context, List<Syllabus> mList, OnPostClick onClickListener,
                                   OnDownloadClick onDownloadClick) {

        this.context = context;
        this.mList = mList;
        this.onClickListener = onClickListener;
        this.onDownloadClick = onDownloadClick;

        colors = context.getResources().getStringArray(R.array.mdcolor_500);
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(context);
        sharedPreferences = getInstance(context);

    }

    @NonNull
    @Override
    public SyllabusRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_syllabus_v2, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SyllabusRecyclerAdapter.ViewHolder holder, final int position) {
        final Syllabus post = mList.get(holder.getAdapterPosition());


        holder.rlv_name_view.setTitleText(post.getSubjectName().substring(0, 1));

//        holder.rlv_name_view.setBackgroundColor(RandomColor.getRandomMaterialColor(context,"400"));
//        holder.relMain.setBackgroundColor(Color.parseColor(colors[position]));
        holder.rlv_name_view.setBackgroundColor(Color.parseColor(colors[position+1]));

        holder.title.setText(mList.get(position).getSubjectName());
        holder.semester_name.setText("Sem "+mList.get(position).getSemester());
        holder.subject_code.setText(mList.get(position).getSubjectCode());
        holder.subject_type.setText(mList.get(position).getSubjectType());
    }


    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout relMain;
        private RoundedLetterView rlv_name_view;
        private TextView title, semester_name,subject_code, subject_type;
        private TextView iv_download;


        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            relMain = itemView.findViewById(R.id.relMain);
            rlv_name_view = itemView.findViewById(R.id.rlv_name_view);
            title = itemView.findViewById(R.id.title);
            semester_name = itemView.findViewById(R.id.semester_name);
            subject_code = itemView.findViewById(R.id.subject_code);
            subject_type = itemView.findViewById(R.id.subject_type);
            iv_download = itemView.findViewById(R.id.iv_download);

//         
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (onClickListener != null && position != RecyclerView.NO_POSITION) {
                        onClickListener.onClick(mList.get(position));
                    }
                }
            });


            iv_download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (onClickListener != null && position != RecyclerView.NO_POSITION) {
                        //download the image from the drive
                        Log.d(TAG, "onClick: downloading "+mList.get(position).getFilePath());
                        onDownloadClick.onClick(mList.get(position));

                    }
                }
            });



        }


    }

    public interface OnPostClick {
        void onClick(Syllabus syllabus);
    }

    public interface OnDownloadClick {
        void onClick(Syllabus syllabus);
    }

}
