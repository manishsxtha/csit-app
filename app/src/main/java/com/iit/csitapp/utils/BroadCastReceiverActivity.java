package com.iit.csitapp.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.iit.csitapp.models.Download;
import com.iit.csitapp.network.DownloadService;

import static com.iit.csitapp.MainActivity.MESSAGE_PROGRESS;

public class BroadCastReceiverActivity {

    private static final String TAG = "BroadCastReceiverActivi";
    private LocalBroadcastManager bManager;

    private Context mContext;
    private onClick onClick;

    public BroadCastReceiverActivity(Context mContext, onClick onClick) {
        this.mContext = mContext;
        this.onClick = onClick;
    }

    public void downloadFile(String filePath, String fileName) {

        Intent intent = new Intent(mContext, DownloadService.class);
        intent.putExtra("dFilePath", filePath);
        intent.putExtra("dFileName", fileName);
        mContext.startService(intent);
    }

    public void registerReceiver() {
        bManager = LocalBroadcastManager.getInstance(mContext);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_PROGRESS);
        bManager.registerReceiver(broadcastReceiver, intentFilter);

    }

    public void unregisterReceiver(){

        if (bManager != null){

            Log.d(TAG, "unregisterReceiver: Unregistered Receiver");
            bManager.unregisterReceiver(broadcastReceiver);
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(MESSAGE_PROGRESS)) {

                Download download = intent.getParcelableExtra("download");

                Log.e(TAG, "onReceive: dalkdsfjlkdsajfls downlaod"+download.getProgress() );
                onClick.onResult(download.getProgress());
//                mProgressBar.setProgress(download.getProgress());
                if (download.getProgress() == 100) {

                    Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                    unregisterReceiver();
//                    mProgressText.setText("File Download Complete");

                }
//                else {
//
////                    Toast.makeText(mContext, download.getProgress()+"", Toast.LENGTH_SHORT).show();
////                    mProgressText.setText(String.format("Downloaded (%d/%d) MB",download.getCurrentFileSize(),download.getTotalFileSize()));
//
//                }
            }
        }
    };

    public interface onClick{
        public void onResult(int progress);

    }
}
