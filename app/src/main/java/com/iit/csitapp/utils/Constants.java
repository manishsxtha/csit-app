package com.iit.csitapp.utils;

public class Constants {
    public static final int STORAGE_PERMISSION_CODE = 100;

    public static final String ISQUES = "isQues";
    public static final String SUBJECT = "subject";
    public static final String SEMESTER = "semester";


    public static final int SUBJECT_CODE = 0;
    public static final int SYLLABUS_CODE = 1;
    public static final int SEMESTER_CODE = 2;
    public static final int MODELQ_CODE = 3;

    public static final String[] IMAGES = {
            "http://iitnepal.edu.np/courses/481800_b52d_5.jpg",
            "http://iitnepal.edu.np/courses/djangoimage.png",
            "http://iitnepal.edu.np/courses/googo-1.png",
            "http://iitnepal.edu.np/courses/googo-1.png",
    };
    public static final int NOTES_CODE = 3;
    public static final int QA_CODE = 4;
    public static final String FIREBASE_TOKEN = "firebase";

    public static final int THEME_BLACK = 0;
    public static final int THEME_BLUE = 1;
    public static final int THEME_RED = 2;
}
