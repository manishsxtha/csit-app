package com.iit.csitapp.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.iit.csitapp.models.Event;
import com.iit.csitapp.models.ModelQuestion;
import com.iit.csitapp.models.News;
import com.iit.csitapp.models.Note;
import com.iit.csitapp.models.Project;
import com.iit.csitapp.models.Question;
import com.iit.csitapp.models.Slider;
import com.iit.csitapp.models.Subject;
import com.iit.csitapp.models.Syllabus;
import com.iit.csitapp.models.User;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "DatabaseHelper";

    //database singleton instance
    private static DatabaseHelper instance = null;
    // Database Version
    private static final int DATABASE_VERSION = 15;

    // Database Name
    private static final String DATABASE_NAME = "UserManager.db";

    // User table name
    private static final String TABLE_USER = "user";

    // User Table Columns names
    private static final String COLUMN_USER_ID = "user_id";
    private static final String COLUMN_USER_NAME = "user_name";
    private static final String COLUMN_USER_EMAIL = "user_email";
    private static final String COL_USER_IMAGE = "user_password";


    private static final String COL_USER_TYPE = "type";
    private static final String COL_USER_ID = "user_id";

    //table subjects
    private String DROP_SUBJECT_TABLE = "DROP TABLE IF EXISTS " + TABLE_SUBJECT;
    private String CREATE_SUBJECT_TABLE = String.format("CREATE TABLE %s(" +
            "%s INTEGER PRIMARY KEY AUTOINCREMENT," +
            "%s INTEGER," +
            "%s TEXT," +
            "%s VARCHAR(50)," +
            "%s TEXT)",
            TABLE_SUBJECT,
            COL_SUBJECT_ID,
            COL_SUBJECT_SEM_ID,
            COL_SUBJECT_NAME,
            COL_SUBJECT_CODE,
            COL_SUBJECT_TYPE);

    private static final String TABLE_SUBJECT = "subjects";

    private static final String COL_SUBJECT_ID = "id";
    private static final String COL_SUBJECT_SEM_ID = "semester_id";
    private static final String COL_SUBJECT_NAME = "subject_name";
    private static final String COL_SUBJECT_TYPE = "type";
    private static final String COL_SUBJECT_CODE = "subjectCode";


    //table slider
    private String DROP_SLIDER_TABLE = "DROP TABLE IF EXISTS " + TABLE_SLIDER;
    private String CREATE_SLIDER_TABLE = String.format("CREATE TABLE %s(%s VARCHAR(255) PRIMARY KEY," +
                    "%s VARCHAR(100),%s TEXT,%s TEXT,%s VARCHAR(100),%s TEXT)"
            , TABLE_SLIDER,
            COL_SLIDER_ID,
            COL_SLIDER_DATE,
            COL_SLIDER_DESC,
            COL_SLIDER_IMAGE,
            COL_SLIDER_TITLE,
            COL_SLIDER_URL
            );

    private static final String TABLE_SLIDER = "sliders";

    private static final String COL_SLIDER_ID = "id";
    private static final String COL_SLIDER_DATE = "createdDate";
    private static final String COL_SLIDER_DESC = "desc";
    private static final String COL_SLIDER_IMAGE = "imageUrl";
    private static final String COL_SLIDER_TITLE = "title";
    private static final String COL_SLIDER_URL = "url";

    //table events
    private String DROP_EVENT_TABLE = "DROP TABLE IF EXISTS " + TABLE_EVENT;
    private String CREATE_EVENT_TABLE = String.format("CREATE TABLE %s(%s VARCHAR(255) PRIMARY KEY," +
                    "%s VARCHAR(100),%s VARCHAR(100),%s TEXT,%s TEXT,%s VARCHAR(100),%s TEXT)"
            , TABLE_EVENT,
            COL_EVENT_ID,
            COL_EVENT_CREATED_DATE,
            COL_EVENT_DATE,
            COL_EVENT_IMAGE,
            COL_EVENT_URL,
            COL_EVENT_TITLE,
            COL_EVENT_DESC);


    private static final String TABLE_EVENT = "events";

    private static final String COL_EVENT_ID = "id";
    private static final String COL_EVENT_CREATED_DATE = "createdDate";
    private static final String COL_EVENT_DATE = "date";
    private static final String COL_EVENT_IMAGE = "eventImage";
    private static final String COL_EVENT_URL = "eventUrl";
    private static final String COL_EVENT_TITLE = "title";
    private static final String COL_EVENT_DESC = "description";

    //table news
    private String DROP_NEWS_TABLE = "DROP TABLE IF EXISTS " + TABLE_NEWS;
    private String CREATE_NEWS_TABLE = String.format("CREATE TABLE %s(%s VARCHAR(255) PRIMARY KEY," +
                    "%s VARCHAR(100),%s TEXT,%s TEXT,%s VARCHAR(100),%s TEXT)"
            , TABLE_NEWS,
            COL_NEWS_ID,
            COL_NEWS_CREATED_DATE,
            COL_NEWS_IMAGE,
            COL_NEWS_URL,
            COL_NEWS_TITLE,
            COL_NEWS_DESC);

    private static final String TABLE_NEWS = "news";

    private static final String COL_NEWS_ID = "id";
    private static final String COL_NEWS_CREATED_DATE = "createdDate";
    
    private static final String COL_NEWS_IMAGE = "imageUrl";
    private static final String COL_NEWS_URL = "newsUrl";
    private static final String COL_NEWS_TITLE = "title";
    private static final String COL_NEWS_DESC = "description";

    //table projects
    private String DROP_PROJECT_TABLE = "DROP TABLE IF EXISTS " + TABLE_PROJECT;
    private String CREATE_PROJECT_TABLE = String.format("CREATE TABLE %s(%s VARCHAR(255) PRIMARY KEY," +
                    "%s VARCHAR(100),%s TEXT,%s TEXT,%s TEXT)"
            , TABLE_PROJECT,
            COL_PROJECT_ID,
            COL_PROJECT_CREATED_DATE,
            COL_PROJECT_TITLE,
            COL_PROJECT_DESC,
            COL_PROJECT_IMAGE
    );

    private static final String TABLE_PROJECT = "PROJECT";

    private static final String COL_PROJECT_ID = "id";
    private static final String COL_PROJECT_CREATED_DATE = "createdDate";


    private static final String COL_PROJECT_TITLE = "title";
    private static final String COL_PROJECT_DESC = "description";
    private static final String COL_PROJECT_IMAGE = "pic";


    //table notes
    private String DROP_NOTES_TABLE = "DROP TABLE IF EXISTS " + TABLE_NOTES;
    private String CREATE_NOTES_TABLE = String.format("CREATE TABLE %s(" +
                    "%s VARCHAR(255) PRIMARY KEY," +
                    "%s VARCHAR(100)," +
                    "%s TEXT,%s TEXT," +
                    "%s VARCHAR(100)," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT" +
                    ")"
            , TABLE_NOTES,
            COL_NOTES_ID,
            COL_NOTES_CREATED_DATE,
            COL_NOTES_FILENAME,
            COL_NOTES_FILE_PATH,
            COL_NOTES_CREATED_BY,
            COL_NOTES_SUBJECT_NAME,
            COL_SUBJECT_CODE,
            COL_SUBJECT_TYPE,
            COL_NOTES_SEMESTER
    );

    private static final String TABLE_NOTES = "notes";

    private static final String COL_NOTES_ID = "id";
    private static final String COL_NOTES_CREATED_DATE = "createdDate";

    private static final String COL_NOTES_FILENAME = "fileName";
    private static final String COL_NOTES_FILE_PATH = "filePath";
    private static final String COL_NOTES_CREATED_BY = "createdBy";
    private static final String COL_NOTES_SUBJECT_NAME = "subjectName";
    private static final String COL_NOTES_SEMESTER = "semester";



    //table notes
    private String DROP_SYLLABUS_TABLE = "DROP TABLE IF EXISTS " + TABLE_SYLLABUS;
    private String CREATE_SYLLABUS_TABLE = String.format("CREATE TABLE %s(%s VARCHAR(255) PRIMARY KEY," +
                    "%s VARCHAR(100)," +
                    "%s VARCHAR(100)," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT)"
            , TABLE_SYLLABUS,
            COL_SYLLABUS_IDD,
            COL_SYLLABUS_ID,
            COL_SYLLABUS_CREATED_DATE,
            COL_SYLLABUS_FILE_PATH,
            COL_SYLLABUS_SUBJECT_NAME,
            COL_SUBJECT_CODE,
            COL_SUBJECT_TYPE,
            COL_SYLLABUS_SEMESTER
    );

    private static final String TABLE_SYLLABUS = "syllabus";

    private static final String COL_SYLLABUS_IDD = "idd";
    private static final String COL_SYLLABUS_ID = "id";
    private static final String COL_SYLLABUS_CREATED_DATE = "createdDate";
    private static final String COL_SYLLABUS_FILE_PATH = "filePath";
    private static final String COL_SYLLABUS_SEMESTER = "semester";
    private static final String COL_SYLLABUS_SUBJECT_NAME = "subjectName";


    //table QUES
    private String DROP_QUES_TABLE = "DROP TABLE IF EXISTS " + TABLE_QUES;
    private String CREATE_QUES_TABLE = String.format("CREATE TABLE %s(" +
                    "%s VARCHAR(255) PRIMARY KEY," +
                    "%s VARCHAR(100)," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT)"
            , TABLE_QUES,
            COL_QUES_ID,
            COL_QUES_CREATED_DATE,
            COL_QUES_FILE_PATH,
            COL_QUES_FILENAME,
            COL_QUES_SEMESTER,
            COL_QUES_SUBJECT_NAME,
            COL_SUBJECT_CODE,
            COL_SUBJECT_TYPE,
            COL_QUES_YEAR
    );

    private static final String TABLE_QUES = "question";

    private static final String COL_QUES_ID = "id";
    private static final String COL_QUES_CREATED_DATE = "createdDate";
    private static final String COL_QUES_FILENAME = "fileName";
    private static final String COL_QUES_FILE_PATH = "filePath";
    private static final String COL_QUES_SUBJECT_NAME = "subjectName";
    private static final String COL_QUES_SEMESTER = "semester";
    private static final String COL_QUES_YEAR = "year";

    //table SOLN
    private String DROP_SOLN_TABLE = "DROP TABLE IF EXISTS " + TABLE_SOLN;
    private String CREATE_SOLN_TABLE = String.format("CREATE TABLE %s(" +
                    "%s VARCHAR(255) PRIMARY KEY," +
                    "%s VARCHAR(100)," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT)"
            , TABLE_SOLN,
            COL_SOLN_ID,
            COL_SOLN_CREATED_DATE,
            COL_SOLN_FILE_PATH,
            COL_SOLN_SEMESTER,
            COL_SOLN_SUBJECT_NAME,
            COL_SUBJECT_CODE,
            COL_SUBJECT_TYPE,
            COL_SOLN_YEAR
    );

    private static final String TABLE_SOLN = "solution";

    private static final String COL_SOLN_ID = "id";
    private static final String COL_SOLN_CREATED_DATE = "createdDate";
    private static final String COL_SOLN_FILE_PATH = "filePath";
    private static final String COL_SOLN_SUBJECT_NAME = "subjectName";
    private static final String COL_SOLN_SEMESTER = "semester";
    private static final String COL_SOLN_YEAR = "year";

    //table MODELQS
    private String DROP_MODELQS_TABLE = "DROP TABLE IF EXISTS " + TABLE_MODELQS;
    private String CREATE_MODELQS_TABLE = String.format("CREATE TABLE %s(" +
                    "%s VARCHAR(255) PRIMARY KEY," +
                    "%s VARCHAR(100)," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s TEXT)"
            , TABLE_MODELQS,
            COL_MODELQS_ID,
            COL_MODELQS_CREATED_DATE,
            COL_MODELQS_FILE_PATH,
            COL_MODELQS_SEMESTER,
            COL_MODELQS_SUBJECT_NAME,
            COL_SUBJECT_CODE,
            COL_SUBJECT_TYPE,
            COL_MODELQS_YEAR
    );

    private static final String TABLE_MODELQS = "modelQues";

    private static final String COL_MODELQS_ID = "id";
    private static final String COL_MODELQS_CREATED_DATE = "createdDate";
    private static final String COL_MODELQS_FILE_PATH = "filePath";
    private static final String COL_MODELQS_SUBJECT_NAME = "subjectName";
    private static final String COL_MODELQS_SEMESTER = "semester";
    private static final String COL_MODELQS_YEAR = "year";

    
    // create table user sql query

    private String DROP_USER_TABLE = "DROP TABLE IF EXISTS " + TABLE_USER;
    private String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
            + COLUMN_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_USER_NAME + " TEXT,"
            + COLUMN_USER_EMAIL + " TEXT," + COL_USER_IMAGE + " TEXT" + ")";

    // create table notes sql query
//    private String CREATE_NOTES_TABLE = "CREATE TABLE " + TABLE_NOTE + "("
//            + COLUMN_NOTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_NOTE_TITLE + " TEXT,"
//            + COLUMN_NOTE_DATE + " TEXT," + COLUMN_NOTE_DESC + " TEXT" + ")";

//    // User table name
//    private static final String TABLE_USER = "user";
//
//    // User Table Columns names
//    private static final String COLUMN_USER_ID = "id";
//    private static final String COLUMN_USER_NAME = "name";
//    private static final String COLUMN_USER_EMAIL = "email";
//    private static final String COL_USER_IMAGE = "password";
//    private static final String COLUMN_USER_GENDER = "gender";
//    private static final String COLUMN_USER_BLOODGROUP= "bloodGroup";
//    private static final String COLUMN_USER_CONTACTNUM= "contactNum";
//    private static final String COLUMN_USER_ADDRESS= "bloodGroup";
//    private static final String COLUMN_USER_DOB= "dob";
//    private static final String COLUMN_USER_LASTDONATION= "bloodGroup";
//    private static final String COLUMN_USER_ISAVAILABLE= "is";
//
//
//    // create table user sql query


//    private String CREATE_USER_TABLE = "CREATE TABLE `users_blood_dlist` (\n" +
//            "  `id` int(11) PRIMARY KEY AUTOINCREMENT,\n" +
//            "  `name` varchar(100) NOT NULL,\n" +
//            "  `email` text NOT NULL,\n" +
//            "  `password` varchar(100) NOT NULL,\n" +
//            "  `gender` varchar(10) NOT NULL,\n" +
//            "  `bloodGroup` varchar(10) NOT NULL,\n" +
//            "  `contactNum` bigint(100),\n" +
//            "  `address` text,\n" +
//            "  `dob` VARCHAR(255) NOT NULL,\n" +
//            "  `lastDonationDate` VARCHAR(255),\n" +
//            "  `isAvailable` tinyint(1)\n" +
//            ") ";


    /**
     * singleton instance of database
     */
    public static DatabaseHelper getInstance(Context context) {
        if (instance == null)
            instance = new DatabaseHelper(context);
        return instance;
    }

    /**
     * Constructor
     *
     * @param context
     */
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_SUBJECT_TABLE);

        db.execSQL(CREATE_SLIDER_TABLE);
        db.execSQL(CREATE_EVENT_TABLE);
        db.execSQL(CREATE_NEWS_TABLE);
        db.execSQL(CREATE_NOTES_TABLE);
        db.execSQL(CREATE_SYLLABUS_TABLE);
        db.execSQL(CREATE_PROJECT_TABLE);
        db.execSQL(CREATE_QUES_TABLE);
        db.execSQL(CREATE_SOLN_TABLE);
        db.execSQL(CREATE_MODELQS_TABLE);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.e(TAG, "onUpgrade: upgrading tables" );
        //Drop User Table if exist
        db.execSQL(DROP_USER_TABLE);
        db.execSQL(DROP_SUBJECT_TABLE);
        
        db.execSQL(DROP_SLIDER_TABLE);
        db.execSQL(DROP_EVENT_TABLE);

        db.execSQL(DROP_NEWS_TABLE);
        db.execSQL(DROP_NOTES_TABLE);
        db.execSQL(DROP_SYLLABUS_TABLE);
        db.execSQL(DROP_PROJECT_TABLE);
        db.execSQL(DROP_QUES_TABLE);
        db.execSQL(DROP_SOLN_TABLE);
        db.execSQL(DROP_MODELQS_TABLE);

        // Create tables again
        onCreate(db);

    }


    /**
     * This method is to create user record
     *
     * @param user
     */
    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_USER_ID, user.getUser_id());
        values.put(COLUMN_USER_NAME, user.getUsername());
        values.put(COLUMN_USER_EMAIL, user.getEmail());
        values.put(COL_USER_IMAGE, user.getAvatar_img_link());
        values.put(COL_USER_TYPE, user.getUser_type());

        // Inserting Row
        db.insert(TABLE_USER, null, values);
        db.close();
    }

    /**
     * This method is to fetch all user and return the list of user records
     *
     * @return list
     */
    public List<User> getAllUser() {
        // array of columns to fetch
        String[] columns = {
                COL_USER_ID,
                COLUMN_USER_EMAIL,
                COLUMN_USER_NAME,
                COL_USER_IMAGE,
                COL_USER_TYPE
        };
        // sorting orders
        String sortOrder =
                COLUMN_USER_NAME + " ASC";
        List<User> userList = new ArrayList<User>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setUser_id(cursor.getString(cursor.getColumnIndex(COLUMN_USER_ID)));
                user.setUsername(cursor.getString(cursor.getColumnIndex(COLUMN_USER_NAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_USER_EMAIL)));
                user.setUser_type(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_USER_IMAGE))));
                user.setAvatar_img_link(cursor.getString(cursor.getColumnIndex(COL_USER_IMAGE)));
                // Adding user record to list
                Log.v("USER", userList.toString());
                userList.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return userList;
    }


    /**
     * This method is to fetch all user and return the list of user records
     *
     * @return list
     */


    /**
     * This method to update user record
     *
     * @param user
     */
    public void updateUser(User user, int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, user.getUsername());
        values.put(COLUMN_USER_EMAIL, user.getEmail());


        // updating row
        db.update(TABLE_USER, values, COLUMN_USER_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    /**
     * This method is to delete user record
     *
     * @param user
     */
    public void deleteUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_USER, COLUMN_USER_ID + " = ?",
                new String[]{String.valueOf(user.getUser_id())});
        db.close();
    }

    /**
     * This method to check user exist or not
     *
     * @param email
     * @return true/false
     */
    public boolean checkUser(String email) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();

        // selection criteria
        String selection = COLUMN_USER_EMAIL + " = ?";

        // selection argument
        String[] selectionArgs = {email};

        // query user table with condition
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
         */
        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null);                      //The sort order
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        if (cursorCount > 0) {
            return true;
        }

        return false;
    }

    /**
     * This method to check user exist or not
     *
     * @param email
     * @param password
     * @return true/false
     */
    public boolean checkUser(String email, String password, Context context) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_USER_ID,
                COLUMN_USER_NAME,
                COLUMN_USER_EMAIL
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_USER_EMAIL + " = ?" + " AND " + COL_USER_IMAGE + " = ?";

        // selection arguments
        String[] selectionArgs = {email, password};

        // query user table with conditions
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id, name, email FROM user WHERE user_email = 'manish@manish.com' AND user_password = 'qwerty';
         */
        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();


        if (cursorCount > 0) {

            cursor.moveToLast();
            SharedPreferenceHelper manager = SharedPreferenceHelper.getInstance(context);
            User user = new User();
            user.setUser_id(cursor.getString(cursor.getColumnIndex(COLUMN_USER_ID)));
            user.setUsername(cursor.getString(cursor.getColumnIndex(COLUMN_USER_NAME)));
            user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_USER_EMAIL)));
            user.setUser_type(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_USER_IMAGE))));
            user.setAvatar_img_link(cursor.getString(cursor.getColumnIndex(COL_USER_IMAGE)));
            manager.saveUserInfo(user);
            //                    manager.setName(email);
            //                    manager.setEmail(email);


            cursor.close();
            db.close();
            return true;
        }
        db.close();
        return false;
    }

    /**
     *
     * SUBJECT PART
     */

    /**
     * This method is to create subject record
     *
     * @param subject
     */
    public void setSubject(Subject subject) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_SUBJECT_SEM_ID, subject.getSemester());
        values.put(COL_SUBJECT_NAME, subject.getSubjectName());
        values.put(COL_SUBJECT_CODE, subject.getSubjectCode());
        values.put(COL_SUBJECT_TYPE, subject.getType());



        // Inserting Row
        Log.e("SUBJECT", "setSubject: adding " + subject.getSubjectName());
        db.insert(TABLE_SUBJECT, null, values);
        db.close();
    }

    /**
     * This method is to fetch all user and return the list of user records
     *
     * @param semester
     * @return list
     */
    public List<Subject> getSemesterSubjects(String semester) {
        // array of columns to fetch
        String[] columns = {
                COL_SUBJECT_ID,
                COL_SUBJECT_SEM_ID,
                COL_SUBJECT_NAME,
                COL_SUBJECT_CODE,
                COL_SUBJECT_TYPE
        };
        // sorting orders
        String sortOrder =
                COL_SUBJECT_NAME + " ASC";
        List<Subject> subjectList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_SUBJECT, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Subject subject = new Subject();
                subject.setSemester(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_SEM_ID)));
                subject.setSubjectName(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_NAME)));
                subject.setSubjectCode(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_CODE)));
                subject.setType(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_TYPE)));

                // check subject semester for adding semester subject
                if (String.valueOf(subject.getSemester()).equals(semester)) {
                    subjectList.add(subject);
                }


            } while (cursor.moveToNext());
            Log.v("SUBJECT", subjectList.toString());
        }
        cursor.close();
        db.close();

        // return user list
        return subjectList;
    }

    public boolean isRemoteSubjectSizeSame(int remoteSubjectSize) {
        // array of columns to fetch
        String[] columns = {
                COL_SUBJECT_ID,
                COL_SUBJECT_SEM_ID,
                COL_SUBJECT_NAME,
                COL_SUBJECT_CODE,
                COL_SUBJECT_TYPE
        };

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /*
          Here query function is used to fetch records from user table this function works like we use sql query.
          SQL query equivalent to this query function is
          SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_SUBJECT, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        int counter = 0;
        if (cursor.moveToFirst()) {
            do {
                counter++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return remoteSubjectSize == counter;


    }

    public void clearSubjectData() {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_SUBJECT, null, null);
        db.close();
    }

    /**
     * END SUBJECT
     */


    /**
     *
     * SLIDER PART
     */

    /**
     * This method is to create subject record
     *
     * @param slider
     */
    public void setSlider(Slider slider) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_SLIDER_ID, slider.getId());
        values.put(COL_SLIDER_DATE, slider.getCreatedDate());
        values.put(COL_SLIDER_DESC, slider.getDesc());
        values.put(COL_SLIDER_IMAGE, slider.getImageUrl());
        values.put(COL_SLIDER_TITLE, slider.getTitle());
        values.put(COL_SLIDER_URL, slider.getNewsUrl());


        // Inserting Row
        db.insert(TABLE_SLIDER, null, values);
        db.close();
    }

    /**
     * This method is to fetch all user and return the list of user records
     *
     * @return list
     */
    public List<Slider> getAllSlider() {
        // array of columns to fetch
        String[] columns = {
                COL_SLIDER_ID,
                COL_SLIDER_DATE,
                COL_SLIDER_DESC,
                COL_SLIDER_IMAGE,
                COL_SLIDER_TITLE,
                COL_SLIDER_URL
        };
//        // sorting orders
//        String sortOrder =
//                COL_SLIDER_TITLE + " ASC";
        List<Slider> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_SLIDER, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Slider slider = new Slider();
                slider.setId(cursor.getString(cursor.getColumnIndex(COL_SLIDER_ID)));
                slider.setCreatedDate(cursor.getString(cursor.getColumnIndex(COL_SLIDER_DATE)));
                slider.setDesc(cursor.getString(cursor.getColumnIndex(COL_SLIDER_DESC)));
                slider.setImageUrl(cursor.getString(cursor.getColumnIndex(COL_SLIDER_IMAGE)));
                slider.setTitle(cursor.getString(cursor.getColumnIndex(COL_SLIDER_TITLE)));
                slider.setNewsUrl(cursor.getString(cursor.getColumnIndex(COL_SLIDER_URL)));

                list.add(slider);


            } while (cursor.moveToNext());
            Log.v("SLIDER", list.toString());
        }
        cursor.close();
        db.close();

        // return user list
        return list;
    }

    public boolean isRemoteSliderSizeSame(int remoteSize) {
        // array of columns to fetch
        String[] columns = {
                COL_SLIDER_ID,
        };

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /*
          Here query function is used to fetch records from user table this function works like we use sql query.
          SQL query equivalent to this query function is
          SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_SLIDER, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        int counter = 0;
        if (cursor.moveToFirst()) {
            do {
                counter++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        Log.d(TAG, "isRemoteSliderSizeSame: net total:"+remoteSize);
        Log.d(TAG, "isRemoteSliderSizeSame: pref total:"+counter);
        return remoteSize == counter;


    }

    public void clearSliderData() {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_SLIDER, null, null);
        db.close();
    }

    /**
     * END SLIDER
     */


    /**
     *
     * EVENTS PART
     */

    /**
     * This method is to create subject record
     *
     * @param post
     */
    public void setEvent(Event post) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_EVENT_ID, post.getId());
        values.put(COL_EVENT_CREATED_DATE, post.getCreatedDate());
        values.put(COL_EVENT_DATE, post.getDate());
        values.put(COL_EVENT_IMAGE, post.getEventImage());
        values.put(COL_EVENT_URL, post.getEventUrl());
        values.put(COL_EVENT_TITLE, post.getTitle());
        values.put(COL_EVENT_DESC, post.getDescription());


        // Inserting Row
        db.insert(TABLE_EVENT, null, values);
        db.close();
    }

    /**
     * This method is to fetch all user and return the list of user records
     *
     * 
     * @return list
     */
    public List<Event> getAllEvents() {
        // array of columns to fetch
        String[] columns = {
                COL_EVENT_ID,
                COL_EVENT_CREATED_DATE,
                COL_EVENT_DATE,
                COL_EVENT_IMAGE,
                COL_EVENT_URL,
                COL_EVENT_TITLE,
                COL_EVENT_DESC
        };
//        // sorting orders
//        String sortOrder =
//                COL_SUBJECT_NAME + " ASC";
        List<Event> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_EVENT, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Event post = new Event();
                post.setCreatedDate(cursor.getString(cursor.getColumnIndex(COL_EVENT_CREATED_DATE)));
                post.setDate(cursor.getString(cursor.getColumnIndex(COL_EVENT_DATE)));
                post.setDescription(cursor.getString(cursor.getColumnIndex(COL_EVENT_DESC)));
                post.setEventImage(cursor.getString(cursor.getColumnIndex(COL_EVENT_IMAGE)));
                post.setTitle(cursor.getString(cursor.getColumnIndex(COL_EVENT_TITLE)));
                post.setId(cursor.getString(cursor.getColumnIndex(COL_EVENT_ID)));
                post.setEventUrl(cursor.getString(cursor.getColumnIndex(COL_EVENT_URL)));

                list.add(post);
                


            } while (cursor.moveToNext());
            Log.v("EVENTS", list.toString());
        }
        cursor.close();
        db.close();

        // return user list
        return list;
    }

    public boolean isRemoteEventSizeSame(int remoteListSize) {
        // array of columns to fetch
        String[] columns = {
                COL_EVENT_ID
        };

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /*
          Here query function is used to fetch records from user table this function works like we use sql query.
          SQL query equivalent to this query function is
          SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_EVENT, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        int counter = 0;
        if (cursor.moveToFirst()) {
            do {
                counter++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return remoteListSize == counter;


    }

    public void clearEventsData() {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_EVENT, null, null);
        db.close();
    }

    /**
     * END EVENTS
     */

    /**
     *
     * NEWS PART
     */

    /**
     * This method is to create news record
     *
     * @param post
     */
    public void setNews(News post) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_NEWS_ID, post.getId());
        values.put(COL_NEWS_CREATED_DATE, post.getCreatedDate());
        
        values.put(COL_NEWS_IMAGE, post.getImageUrl());
        values.put(COL_NEWS_URL, post.getNewsUrl());
        values.put(COL_NEWS_TITLE, post.getTitle());
        values.put(COL_NEWS_DESC, post.getDescription());


        // Inserting Row
        db.insert(TABLE_NEWS, null, values);
        db.close();
    }

    /**
     * This method is to fetch all user and return the list of user records
     *
     *
     * @return list
     */
    public List<News> getAllNews() {
        // array of columns to fetch
        String[] columns = {
                COL_NEWS_ID,
                COL_NEWS_CREATED_DATE,
                COL_NEWS_IMAGE,
                COL_NEWS_URL,
                COL_NEWS_TITLE,
                COL_NEWS_DESC
        };
//        // sorting orders
//        String sortOrder =
//                COL_SUBJECT_NAME + " ASC";
        List<News> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_NEWS, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                News post = new News();
                post.setId(cursor.getString(cursor.getColumnIndex(COL_NEWS_ID)));
                post.setCreatedDate(cursor.getString(cursor.getColumnIndex(COL_NEWS_CREATED_DATE)));
                post.setImageUrl(cursor.getString(cursor.getColumnIndex(COL_NEWS_IMAGE)));
                post.setNewsUrl(cursor.getString(cursor.getColumnIndex(COL_NEWS_URL)));
                post.setTitle(cursor.getString(cursor.getColumnIndex(COL_NEWS_TITLE)));
                post.setDescription(cursor.getString(cursor.getColumnIndex(COL_NEWS_DESC)));

                list.add(post);



            } while (cursor.moveToNext());
            Log.v("NEWS", list.toString());
        }
        cursor.close();
        db.close();

        // return user list
        return list;
    }

    public boolean isRemoteNewsSizeSame(int remoteListSize) {
        // array of columns to fetch
        String[] columns = {
                COL_EVENT_ID
        };

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /*
          Here query function is used to fetch records from user table this function works like we use sql query.
          SQL query equivalent to this query function is
          SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_NEWS, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        int counter = 0;
        if (cursor.moveToFirst()) {
            do {
                counter++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return remoteListSize == counter;


    }

    public void clearNewsData() {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_NEWS, null, null);
        db.close();
    }

    /**
     * ////////////////////////////////////////////END NEWS
     */

    /**
     *
     * *******************************************NOTES PART
     */

    /**
     * This method is to create notes record
     * <p>
     * , TABLE_NOTES,
     * COL_NOTES_ID,
     * COL_NOTES_CREATED_DATE,
     * COL_NOTES_FILENAME,
     * COL_NOTES_FILE_PATH,
     * COL_NOTES_CREATED_BY,
     * COL_NOTES_SUBJECT_NAME,
     * COL_NOTES_SEMESTER);
     *
     * @param posts
     */
    public void setNotes(List<Note> posts) {
        SQLiteDatabase db = this.getWritableDatabase();

        for (Note post :
                posts) {
            ContentValues values = new ContentValues();
            values.put(COL_NOTES_ID, post.getId());
            values.put(COL_NOTES_CREATED_DATE, post.getCreatedDate());

            values.put(COL_NOTES_FILENAME, post.getFileName());
            values.put(COL_NOTES_FILE_PATH, post.getFilePath());
            values.put(COL_NOTES_CREATED_BY, post.getCreatedBy());
            values.put(COL_NOTES_SUBJECT_NAME, post.getSubjectName());
            values.put(COL_SUBJECT_CODE, post.getSubjectCode());
            values.put(COL_SUBJECT_TYPE, post.getSubjectType());
            values.put(COL_NOTES_SEMESTER, post.getSemester());



            // Inserting Row
            db.insert(TABLE_NOTES, null, values);
            Log.d(TAG, "setNotes: " + post.toString());
        }

        db.close();

    }

    /**
     * This method is to fetch all user and return the list of notes records
     * TABLE_NOTES,
     * *             COL_NOTES_ID,
     * *             COL_NOTES_CREATED_DATE,
     * *             COL_NOTES_FILENAME,
     * *             COL_NOTES_FILE_PATH,
     * *             COL_NOTES_CREATED_BY,
     * *             COL_NOTES_SUBJECT_NAME,
     * *             COL_NOTES_SEMESTER);
     *
     * @return list
     */
    public List<Note> getAllNotes(String subjectName) {
        // array of columns to fetch
        String[] columns = {
                COL_NOTES_ID,
                COL_NOTES_CREATED_DATE,
                COL_NOTES_FILENAME,
                COL_NOTES_FILE_PATH,
                COL_NOTES_CREATED_BY,
                COL_NOTES_SUBJECT_NAME,
                COL_SUBJECT_CODE,
                COL_SUBJECT_TYPE,
                COL_NOTES_SEMESTER
        };
//        // sorting orders
//        String sortOrder =
//                COL_SUBJECT_NAME + " ASC";
        List<Note> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_NOTES, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Note post = new Note();
                post.setId(cursor.getString(cursor.getColumnIndex(COL_NEWS_ID)));
                post.setCreatedDate(cursor.getString(cursor.getColumnIndex(COL_NEWS_CREATED_DATE)));
                post.setFileName(cursor.getString(cursor.getColumnIndex(COL_NOTES_FILENAME)));
                post.setFilePath(cursor.getString(cursor.getColumnIndex(COL_NOTES_FILE_PATH)));
                post.setCreatedBy(cursor.getString(cursor.getColumnIndex(COL_NOTES_CREATED_BY)));
                post.setSubjectName(cursor.getString(cursor.getColumnIndex(COL_NOTES_SUBJECT_NAME)));
                post.setSubjectCode(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_CODE)));
                post.setSubjectType(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_TYPE)));
                post.setSemester(cursor.getString(cursor.getColumnIndex(COL_NOTES_SEMESTER)));


                if (post.getSubjectName().equals(subjectName)) {
                    list.add(post);
                    Log.d(TAG, "getAllNotes: "+post.toString());
                }


            } while (cursor.moveToNext());
            Log.v("NOTES", list.toString());
        }
        cursor.close();
        db.close();

        // return user list
        return list;
    }

    public boolean isRemoteNotesSizeSame(int remoteSubjectSize) {
        // array of columns to fetch
        String[] columns = {
                COL_NOTES_ID,
                COL_NOTES_CREATED_DATE,
                COL_NOTES_FILENAME,
                COL_NOTES_FILE_PATH,
                COL_NOTES_CREATED_BY,
                COL_NOTES_SUBJECT_NAME,
                COL_SUBJECT_CODE,
                COL_SUBJECT_TYPE,
                COL_NOTES_SEMESTER
        };

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /*
          Here query function is used to fetch records from user table this function works like we use sql query.
          SQL query equivalent to this query function is
          SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_NOTES, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        int counter = 0;
        if (cursor.moveToFirst()) {
            do {
                counter++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return remoteSubjectSize == counter;


    }


    public void clearNotesData() {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_NOTES, null, null);
        db.close();
        Log.d(TAG, "clearNotesData: notes");
    }

    /**
     * ////////////////////////////////////END NOTES
     */

    /**
     *
     * **************************SYLLABUS PART
     */

    /**
     * This method is to create syllbus record
     * , TABLE_SYLLABUS,
     * COL_SYLLABUS_ID,
     * COL_SYLLABUS_CREATED_DATE,
     * COL_SYLLABUS_FILE_PATH,
     * COL_SYLLABUS_SUBJECT_NAME,
     * COL_SYLLABUS_SEMESTER
     *
     * @param post
     */
    public void setSyllabus(Syllabus post) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_SYLLABUS_ID, post.getId());
        values.put(COL_SYLLABUS_CREATED_DATE, post.getCreatedDate());
        values.put(COL_SYLLABUS_FILE_PATH, post.getFilePath());
        values.put(COL_SYLLABUS_SUBJECT_NAME, post.getSubjectName());
        values.put(COL_SUBJECT_CODE, post.getSubjectCode());
        values.put(COL_SUBJECT_TYPE, post.getSubjectType());
        values.put(COL_SYLLABUS_SEMESTER, post.getSemester());


        // Inserting Row
        db.insert(TABLE_SYLLABUS, null, values);
        db.close();
        Log.d(TAG, "setSyllabus: "+post.toString());
    }

    public List<Syllabus> getSubjectSyllabus(String subCode) {
        String[] columns = {
                COL_SYLLABUS_ID,
                COL_SYLLABUS_CREATED_DATE,
                COL_SYLLABUS_FILE_PATH,
                COL_SYLLABUS_SUBJECT_NAME,
                COL_SUBJECT_CODE,
                COL_SUBJECT_TYPE,
                COL_SYLLABUS_SEMESTER
        };
//        // sorting orders
//        String sortOrder =
//                COL_SUBJECT_NAME + " ASC";
        List<Syllabus> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_SYLLABUS, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Syllabus post = new Syllabus();
                post.setId(cursor.getString(cursor.getColumnIndex(COL_SYLLABUS_ID)));
                post.setCreatedDate(cursor.getString(cursor.getColumnIndex(COL_SYLLABUS_CREATED_DATE)));
                post.setFilePath(cursor.getString(cursor.getColumnIndex(COL_SYLLABUS_FILE_PATH)));
                post.setSubjectName(cursor.getString(cursor.getColumnIndex(COL_SYLLABUS_SUBJECT_NAME)));
                post.setSubjectCode(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_CODE)));
                post.setSubjectType(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_TYPE)));
                post.setSemester(cursor.getString(cursor.getColumnIndex(COL_SYLLABUS_SEMESTER)));


                if (post.getSubjectCode().equals(subCode)) {
                    list.add(post);
                    Log.d(TAG, "getAllSyllabus: " + post.toString());
                }


            } while (cursor.moveToNext());
            Log.v("SYLLABUS", list.toString());
        }
        cursor.close();
        db.close();

        // return user list
        return list;
    }

    /**
     * This method is to fetch all user and return the list of notes records
     * , TABLE_SYLLABUS,
     * COL_SYLLABUS_ID,
     * COL_SYLLABUS_CREATED_DATE,
     * COL_SYLLABUS_FILE_PATH,
     * COL_SYLLABUS_SUBJECT_NAME,
     * COL_SYLLABUS_SEMESTER
     *
     * @return list
     */
    public List<Syllabus> getAllSyllabus(String semester) {
        // array of columns to fetch
        String[] columns = {
                COL_SYLLABUS_ID,
                COL_SYLLABUS_CREATED_DATE,
                COL_SYLLABUS_FILE_PATH,
                COL_SYLLABUS_SUBJECT_NAME,
                COL_SUBJECT_CODE,
                COL_SUBJECT_TYPE,
                COL_SYLLABUS_SEMESTER
        };
//        // sorting orders
//        String sortOrder =
//                COL_SUBJECT_NAME + " ASC";
        List<Syllabus> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_SYLLABUS, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Syllabus post = new Syllabus();
                post.setId(cursor.getString(cursor.getColumnIndex(COL_SYLLABUS_ID)));
                post.setCreatedDate(cursor.getString(cursor.getColumnIndex(COL_SYLLABUS_CREATED_DATE)));
                post.setFilePath(cursor.getString(cursor.getColumnIndex(COL_SYLLABUS_FILE_PATH)));
                post.setSubjectName(cursor.getString(cursor.getColumnIndex(COL_SYLLABUS_SUBJECT_NAME)));
                post.setSubjectCode(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_CODE)));
                post.setSubjectType(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_TYPE)));
                post.setSemester(cursor.getString(cursor.getColumnIndex(COL_SYLLABUS_SEMESTER)));


                if (post.getSemester().equals(semester)) {
                    list.add(post);
                    Log.d(TAG, "getAllSyllabus: "+post.toString());
                }


            } while (cursor.moveToNext());
            Log.v("SYLLABUS", list.toString());
        }
        cursor.close();
        db.close();

        // return user list
        return list;
    }

    /**
     * , TABLE_SYLLABUS,
     * COL_SYLLABUS_ID,
     * COL_SYLLABUS_CREATED_DATE,
     * COL_SYLLABUS_FILE_PATH,
     * COL_SYLLABUS_SUBJECT_NAME,
     * COL_SYLLABUS_SEMESTER
     *
     * @param remoteSubjectSize
     * @return
     */
    public boolean isRemoteSyllabusSizeSame(int remoteSubjectSize) {
        // array of columns to fetch
        String[] columns = {
                COL_SYLLABUS_ID,
                COL_SYLLABUS_CREATED_DATE,
                COL_SYLLABUS_FILE_PATH,
                COL_SYLLABUS_SUBJECT_NAME,
                COL_SUBJECT_CODE,
                COL_SUBJECT_TYPE,
                COL_SYLLABUS_SEMESTER
        };

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /*
          Here query function is used to fetch records from user table this function works like we use sql query.
          SQL query equivalent to this query function is
          SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_SYLLABUS, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        int counter = 0;
        if (cursor.moveToFirst()) {
            do {
                counter++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return remoteSubjectSize == counter;


    }


    public void clearSyllabusData() {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_SYLLABUS, null, null);
        db.close();
        Log.d(TAG, "clearSyllabusData: syllabus");
    }

    /**
     * ////////////////////////////////////END NOTES
     */

    /**
     *
     * **************************PROJECT PART
     */

    /**
     * This method is to create project record
     , TABLE_PROJECT,
     COL_PROJECT_ID,
     COL_PROJECT_CREATED_DATE,
     COL_PROJECT_TITLE,
     COL_PROJECT_DESC,
     COL_PROJECT_IMAGE
     *
     * @param post
     */
    public void setProject(Project post) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_PROJECT_ID, post.getId());
        values.put(COL_PROJECT_CREATED_DATE, post.getCreatedDate());
        values.put(COL_PROJECT_TITLE, post.getTitle());
        values.put(COL_PROJECT_DESC, post.getDesc());
        values.put(COL_PROJECT_IMAGE, post.getPic());


        // Inserting Row
        db.insert(TABLE_PROJECT, null, values);
        db.close();
    }

    /**
     * This method is to fetch all user and return the list of notes records
     , TABLE_PROJECT,
     COL_PROJECT_ID,
     COL_PROJECT_CREATED_DATE,
     COL_PROJECT_TITLE,
     COL_PROJECT_DESC,
     COL_PROJECT_IMAGE
     *
     * @return list
     */
    public List<Project> getAllProjects() {
        // array of columns to fetch
        String[] columns = {
                COL_PROJECT_ID,
                COL_PROJECT_CREATED_DATE,
                COL_PROJECT_TITLE,
                COL_PROJECT_DESC,
                COL_PROJECT_IMAGE
        };
//        // sorting orders
//        String sortOrder =
//                COL_SUBJECT_NAME + " ASC";
        List<Project> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_PROJECT, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Project post = new Project();
                post.setId(cursor.getString(cursor.getColumnIndex(COL_PROJECT_ID)));
                post.setCreatedDate(cursor.getString(cursor.getColumnIndex(COL_PROJECT_CREATED_DATE)));
                post.setTitle(cursor.getString(cursor.getColumnIndex(COL_PROJECT_TITLE)));
                post.setDesc(cursor.getString(cursor.getColumnIndex(COL_PROJECT_DESC)));
                post.setPic(cursor.getString(cursor.getColumnIndex(COL_PROJECT_IMAGE)));

                list.add(post);
                


            } while (cursor.moveToNext());
            Log.v("PROJECT", list.toString());
        }
        cursor.close();
        db.close();

        
        return list;
    }

    /**
     , TABLE_PROJECT,
     COL_PROJECT_ID,
     COL_PROJECT_CREATED_DATE,
     COL_PROJECT_TITLE,
     COL_PROJECT_DESC,
     COL_PROJECT_IMAGE
     *
     * @param remoteSize
     * @return
     */
    public boolean isRemoteProjectsSizeSame(int remoteSize) {
        // array of columns to fetch
        String[] columns = {
                COL_PROJECT_ID,
                COL_PROJECT_CREATED_DATE,
                COL_PROJECT_TITLE,
                COL_PROJECT_DESC,
                COL_PROJECT_IMAGE
        };

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /*
          Here query function is used to fetch records from user table this function works like we use sql query.
          SQL query equivalent to this query function is
          SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_PROJECT, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        int counter = 0;
        if (cursor.moveToFirst()) {
            do {
                counter++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return remoteSize == counter;


    }


    public void clearProjectsData() {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_PROJECT, null, null);
        db.close();
    }

    /**
     * ////////////////////////////////////END PROJECTS
     */


    /**
     *
     * **************************QUES PART
     */

    /**
     * This method is to create ques record
     * , TABLE_QUES,
     * COL_QUES_ID,
     * COL_QUES_CREATED_DATE,
     * COL_QUES_FILE_PATH,
     * COL_QUES_FILENAME,
     * COL_QUES_SEMESTER,
     * COL_QUES_SUBJECT_NAME,
     * COL_QUES_YEAR
     *
     * @param post
     */
    public void setQues(Question post) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_QUES_ID, post.getId());
        values.put(COL_QUES_CREATED_DATE, post.getCreatedDate());
        values.put(COL_QUES_FILE_PATH, post.getFilePath());
        values.put(COL_QUES_FILENAME, post.getFileName());
        values.put(COL_QUES_SEMESTER, post.getSemester());
        values.put(COL_SUBJECT_NAME, post.getSubjectName());
        values.put(COL_SUBJECT_CODE, post.getSubjectName());
        values.put(COL_SUBJECT_TYPE, post.getSubjectName());
        values.put(COL_QUES_YEAR, post.getYear());


        // Inserting Row
        db.insert(TABLE_QUES, null, values);
        db.close();
    }

    /**
     * This method is to fetch all user and return the list of notes records
     * , TABLE_QUES,
     * COL_QUES_ID,
     * COL_QUES_CREATED_DATE,
     * COL_QUES_FILE_PATH,
     * COL_QUES_FILENAME,
     * COL_QUES_SEMESTER,
     * COL_QUES_SUBJECT_NAME,
     * COL_QUES_YEAR
     *
     * @return list
     */
    public List<Question> getAllQues(String subCode) {
        // array of columns to fetch
        String[] columns = {
                COL_QUES_ID,
                COL_QUES_CREATED_DATE,
                COL_QUES_FILE_PATH,
                COL_QUES_FILENAME,
                COL_QUES_SEMESTER,
                COL_QUES_SUBJECT_NAME,
                COL_SUBJECT_CODE,
                COL_SUBJECT_TYPE,
                COL_QUES_YEAR
        };
//        // sorting orders
//        String sortOrder =
//                COL_SUBJECT_NAME + " ASC";
        List<Question> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_QUES, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
//                      , TABLE_QUES,
//                        COL_QUES_ID,
//                        COL_QUES_CREATED_DATE,
//                        COL_QUES_FILE_PATH,
//                        COL_QUES_FILENAME,
//                        COL_QUES_SEMESTER,
//                        COL_QUES_SUBJECT_NAME,
//                        COL_QUES_YEAR
                Question post = new Question();
                post.setId(cursor.getString(cursor.getColumnIndex(COL_QUES_ID)));
                post.setCreatedDate(cursor.getString(cursor.getColumnIndex(COL_QUES_CREATED_DATE)));
                post.setFilePath(cursor.getString(cursor.getColumnIndex(COL_QUES_FILE_PATH)));
                post.setFileName(cursor.getString(cursor.getColumnIndex(COL_QUES_FILENAME)));
                post.setSemester(cursor.getString(cursor.getColumnIndex(COL_QUES_SEMESTER)));
                post.setSubjectName(cursor.getString(cursor.getColumnIndex(COL_QUES_SUBJECT_NAME)));
                post.setSubjectCode(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_CODE)));
                post.setSubjectType(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_TYPE)));
                post.setYear(cursor.getString(cursor.getColumnIndex(COL_QUES_YEAR)));

                if (post.getSubjectCode().equals(subCode))
                    list.add(post);


            } while (cursor.moveToNext());
            Log.v("QUES", list.toString());
        }
        cursor.close();
        db.close();


        return list;
    }


    public void clearQuesAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_QUES, null, null);
        db.close();
    }

    /**
     * ////////////////////////////////////END QUES
     */    /**
     *
     * **************************SOLN PART
     */

    /**
     * This method is to create ques record
     * , TABLE_QUES,
     * COL_QUES_ID,
     * COL_QUES_CREATED_DATE,
     * COL_QUES_FILE_PATH,
     * COL_QUES_SEMESTER,
     * COL_QUES_SUBJECT_NAME,
     * COL_QUES_YEAR
     *
     * @param post
     */
    public void setSoln(Question post) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_QUES_ID, post.getId());
        values.put(COL_QUES_CREATED_DATE, post.getCreatedDate());
        values.put(COL_QUES_FILE_PATH, post.getFilePath());
        values.put(COL_QUES_SEMESTER, post.getSemester());
        values.put(COL_SUBJECT_NAME, post.getSubjectName());
        values.put(COL_SUBJECT_CODE, post.getSubjectCode());
        values.put(COL_SUBJECT_TYPE, post.getSubjectType());
        values.put(COL_QUES_YEAR, post.getYear());


        // Inserting Row
        db.insert(TABLE_SOLN, null, values);
        db.close();
    }

    /**
     * This method is to fetch all user and return the list of notes records
     * , TABLE_QUES,
     * COL_QUES_ID,
     * COL_QUES_CREATED_DATE,
     * COL_QUES_FILE_PATH,
     * <p>
     * COL_QUES_SEMESTER,
     * COL_QUES_SUBJECT_NAME,
     * COL_QUES_YEAR
     *
     * @return list
     */
    public List<Question> getAllSoln(String subCode) {
        // array of columns to fetch
        String[] columns = {
                COL_QUES_ID,
                COL_QUES_CREATED_DATE,
                COL_QUES_FILE_PATH,
                COL_QUES_SEMESTER,
                COL_QUES_SUBJECT_NAME,
                COL_SUBJECT_CODE,
                COL_SUBJECT_TYPE,
                COL_QUES_YEAR
        };
//        // sorting orders
//        String sortOrder =
//                COL_SUBJECT_NAME + " ASC";
        List<Question> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_SOLN, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
//                      , TABLE_QUES,
//                        COL_QUES_ID,
//                        COL_QUES_CREATED_DATE,
//                        COL_QUES_FILE_PATH,

//                        COL_QUES_SEMESTER,
//                        COL_QUES_SUBJECT_NAME,
//                        COL_QUES_YEAR
                Question post = new Question();
                post.setId(cursor.getString(cursor.getColumnIndex(COL_QUES_ID)));
                post.setCreatedDate(cursor.getString(cursor.getColumnIndex(COL_QUES_CREATED_DATE)));
                post.setFilePath(cursor.getString(cursor.getColumnIndex(COL_QUES_FILE_PATH)));
                post.setSemester(cursor.getString(cursor.getColumnIndex(COL_QUES_SEMESTER)));
                post.setSubjectName(cursor.getString(cursor.getColumnIndex(COL_QUES_SUBJECT_NAME)));
                post.setSubjectCode(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_CODE)));
                post.setSubjectType(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_TYPE)));
                post.setYear(cursor.getString(cursor.getColumnIndex(COL_QUES_YEAR)));

                if (post.getSubjectCode().equals(subCode))
                list.add(post);


            } while (cursor.moveToNext());
            Log.v("SOLN", list.toString());
        }
        cursor.close();
        db.close();


        return list;
    }


    public void clearSolnAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_SOLN, null, null);
        db.close();
    }

    /**
     * ////////////////////////////////////END SOLN
     */


    /**
     * **************************MODELQS PART
     */

    /**
     * This method is to create ques record
     COL_MODELQS_ID,
     COL_MODELQS_CREATED_DATE,
     COL_MODELQS_FILE_PATH,
     COL_MODELQS_SEMESTER,
     COL_MODELQS_SUBJECT_NAME,
     COL_MODELQS_YEAR
     *
     * @param post
     */
    public void setModelQ(ModelQuestion post) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_MODELQS_ID, post.getId());
        values.put(COL_MODELQS_CREATED_DATE, post.getCreatedDate());
        values.put(COL_MODELQS_FILE_PATH, post.getFilePath());
        values.put(COL_MODELQS_SEMESTER, post.getSemester());
        values.put(COL_SUBJECT_CODE, post.getSubjectCode());
        values.put(COL_SUBJECT_TYPE, post.getSubjectType());
        values.put(COL_MODELQS_YEAR, post.getYear());


        // Inserting Row
        db.insert(TABLE_MODELQS, null, values);
        db.close();
    }

    /**
     * This method is to fetch all user and return the list of notes records
     , TABLE_MODELQS,
     COL_MODELQS_ID,
     COL_MODELQS_CREATED_DATE,
     COL_MODELQS_FILE_PATH,
     COL_MODELQS_SEMESTER,
     COL_MODELQS_SUBJECT_NAME,
     COL_MODELQS_YEAR
     * @return list
     */
    public List<ModelQuestion> getAllModelQ(String sub) {
        // array of columns to fetch
        String[] columns = {
                COL_MODELQS_ID,
                COL_MODELQS_CREATED_DATE,
                COL_MODELQS_FILE_PATH,
                COL_MODELQS_SEMESTER,
                COL_MODELQS_SUBJECT_NAME,
                COL_SUBJECT_CODE,
                COL_SUBJECT_TYPE,
                COL_MODELQS_YEAR
        };
//        // sorting orders
//        String sortOrder =
//                COL_SUBJECT_NAME + " ASC";
        List<ModelQuestion> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_MODELQS, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
//                      , TABLE_QUES,
//                        COL_QUES_ID,
//                        COL_QUES_CREATED_DATE,
//                        COL_QUES_FILE_PATH,

//                        COL_QUES_SEMESTER,
//                        COL_QUES_SUBJECT_NAME,
//                        COL_QUES_YEAR
                ModelQuestion post = new ModelQuestion();
                post.setId(cursor.getString(cursor.getColumnIndex(COL_QUES_ID)));
                post.setCreatedDate(cursor.getString(cursor.getColumnIndex(COL_QUES_CREATED_DATE)));
                post.setFilePath(cursor.getString(cursor.getColumnIndex(COL_QUES_FILE_PATH)));
                post.setSemester(cursor.getString(cursor.getColumnIndex(COL_QUES_SEMESTER)));
                post.setSubjectName(cursor.getString(cursor.getColumnIndex(COL_QUES_SUBJECT_NAME)));
                post.setSubjectCode(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_CODE)));
                post.setSubjectType(cursor.getString(cursor.getColumnIndex(COL_SUBJECT_TYPE)));
                post.setYear(cursor.getString(cursor.getColumnIndex(COL_MODELQS_YEAR)));


                if (sub.equals(post.getSubjectName()))
                    list.add(post);


            } while (cursor.moveToNext());
            Log.v("MODELQA", list.toString());
        }
        cursor.close();
        db.close();


        return list;
    }


    public void clearAllModelQ() {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_MODELQS, null, null);
        db.close();
    }

    /**
     * ////////////////////////////////////END MODELQ
     */

    interface OnNoteSizeData {
        boolean isSizeSame();
    }

}