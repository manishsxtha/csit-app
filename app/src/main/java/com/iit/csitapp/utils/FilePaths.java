package com.iit.csitapp.utils;

import android.net.Uri;
import android.os.Environment;
import androidx.annotation.NonNull;

public class FilePaths {
    public static final String SLIDER = "slider";
    
    public static final String SYLLABUS = "syllabus";
    public static final String SUBJECTS = "subjects";
    public static final String QUESTIONS = "old_questions";
    public static final String SOLUTIONS = "old_questions_solutions";
    public static final String NOTES = "notes";
    public static final String MODELQ = "model_questions";

    public static final String SYLLABUS_NEW = "syllabus_new";
    public static final String SUBJECTS_NEW = "subjects_new";
    public static final String QUESTIONS_NEW = "old_questions_new";
    public static final String SOLUTIONS_NEW = "old_questions_solutions_new";
    public static final String NOTES_NEW = "notes_new";
    public static final String MODELQ_NEW = "model_questions_new";

    
    public static final String EVENTS = "events";
    public static final String NEWS = "news";
    public static final String PROJECTS = "projects";
    public static final String USERS = "users";
    public static final String REG_USERS = "potential_users";
    public static final String SAVEFOLDER = "CSIT";

    @NonNull
    public static String getBaseSaveDir() {
//        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/CSIT/";
        return Environment.getExternalStorageDirectory().toString() + "/CSIT/";
    }

    public static String getSaveDir() {
        SharedPreferenceHelper preferenceHelper = new SharedPreferenceHelper();
        if (preferenceHelper.getSyllabusFormat() == 0)
            return getBaseSaveDir() + "old/";
        else return getBaseSaveDir() + "new/";
    }

    @NonNull
    public static String getNameFromUrl(final String url) {
        return Uri.parse(url).getLastPathSegment();
    }
}
