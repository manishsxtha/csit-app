package com.iit.csitapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iit.csitapp.MainActivity;
import com.iit.csitapp.models.User;
import com.iit.csitapp.startup.LoginActivity;

import static com.iit.csitapp.utils.SharedPreferenceHelper.getInstance;


public class FirebaseHelper {
    private static final String TAG = "FirebaseHelper";
    private static FirebaseHelper instance;


    private String user_id;
    private Context mContext;
    private FirebaseAuth mAuth;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;

    private int mPhotoUploadProgress = 0;

    public static FirebaseHelper getFirebaseInstance(Context context) {
        if (instance == null) {
            instance = new FirebaseHelper(context);
        }

        return instance;
    }

    public FirebaseHelper(Context mContext) {
        this.mContext = mContext;
        mAuth = FirebaseAuth.getInstance();
        //inititalize storage

//        mStorageReference = FirebaseStorage.getInstance().getReference();
//        initialize the firebase database
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();


        if (mAuth.getCurrentUser() != null) {
            this.user_id = mAuth.getCurrentUser().getUid();
            Log.e(TAG, "FirebaseHelper: "+this.user_id );
        }

        Log.e(TAG, "FirebaseHelper: "+myRef.toString() );
    }


    public DatabaseReference getMyRef() {
        return myRef;
    }

    public void setUserID(String userID) {
        this.user_id = userID;
    }


    public void signOut() {
        mAuth.signOut();

        SharedPreferenceHelper helper = SharedPreferenceHelper.getInstance(mContext);
        helper.saveUserInfo(new User("", "", "", UserTypes.NORMAL, ""));
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        mContext.startActivity(intent);
    }

    public void checkLogin(String email, String password, Activity activity) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            user_id = mAuth.getCurrentUser().getUid();
                            myRef.child("users").child(user_id)
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            User user = dataSnapshot.getValue(User.class);
                                            SharedPreferenceHelper preferenceHelper = getInstance(mContext);
                                            preferenceHelper.saveUserInfo(user);
                                            Intent intent = new Intent(mContext, MainActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            mContext.startActivity(intent);
//                                            ((HomeActivity) mContext).finish();
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            ((LoginActivity) mContext).hideProgressBar();
                                        }
                                    });


                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            String[] exception = task.getException().toString().split(":");
                            Toast.makeText(mContext, exception[1],
                                    Toast.LENGTH_SHORT).show();
                            ((LoginActivity) mContext).hideProgressBar();


                        }


                    }
                });
    }

    public FirebaseAuth getAuth() {
        return mAuth;
    }

    /**
     * sets the selected user by admin to staff
     *
     * @param selected_user_id - user_id of the user selected by the admin in profile activity
     */
    public void setUserAsNormal(String selected_user_id) {
        myRef.child("users")
                .child(selected_user_id)
                .child("user_type")
                .setValue(UserTypes.NORMAL);
    }

    public void setUserAsPremium(String selected_user_id) {
        myRef.child("users")
                .child(selected_user_id)
                .child("user_type")
                .setValue(UserTypes.PREMIUM);
    }



}
