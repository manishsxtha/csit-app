package com.iit.csitapp.utils;

/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.iit.csitapp.R;
import com.iit.csitapp.event_news.PostDetailActivity;

import java.util.Random;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private static final int BROADCAST_NOTIFICATION_ID = 1;

    private static final String NOTIFICATION_ID_EXTRA = "notificationId";
    private static final String IMAGE_URL_EXTRA = "imageUrl";
    private static final String ADMIN_CHANNEL_ID ="admin_channel";
    private static final String POST_ID = "post_id";
    private NotificationManager notificationManager;


    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

//        String notificationData = "";
//        try {
//            notificationData = remoteMessage.getData().toString();
//            notificationTitle = remoteMessage.getNotification().getTitle();
//            notificationBody = remoteMessage.getNotification().getBody();
//        } catch (NullPointerException e) {
//            Log.e(TAG, "onMessageReceived: NullPointerException: " + e.getMessage());
//        }
//        Log.d(TAG, "onMessageReceived: data: " + notificationData);
//        Log.d(TAG, "onMessageReceived: notification body: " + notificationBody);
//        Log.d(TAG, "onMessageReceived: notification title: " + notificationTitle);
        String notificationBody = "";
        String notificationTitle = "";
        String post_id = "";
        String post_type = "";

        String dataType = remoteMessage.getData().get(getString(R.string.data_type));
        if (dataType.equals(getString(R.string.direct_message))) {
            Log.d(TAG, "onMessageReceived: new incoming message.");
            post_id = remoteMessage.getData().get(getString(R.string.data_post_id));
            post_type = remoteMessage.getData().get(getString(R.string.data_post_type));
            notificationTitle = remoteMessage.getData().get(getString(R.string.data_title));
            notificationBody = remoteMessage.getData().get(getString(R.string.data_body));
//            String message = remoteMessage.getData().get(getString(R.string.data_message));
//            String messageId = remoteMessage.getData().get(getString(R.string.data_message_id));
//            sendMessageNotification(title, message, messageId);
            sendMessageNotification(notificationTitle, notificationBody,post_id,post_type,"123");
        }else if (dataType.equals(getString(R.string.admin_p_user))){
            post_type = remoteMessage.getData().get(getString(R.string.data_post_type));
            notificationTitle = remoteMessage.getData().get(getString(R.string.data_title));
            notificationBody = remoteMessage.getData().get(getString(R.string.data_body));
            sendMessageNotification(notificationTitle, notificationBody,post_id,post_type,"123");
        }
    }


    /**
     * Build a push notification for a chat message
     *  @param title
     * @param message
     * @param messageId
     */
    private void sendMessageNotification(String title, String message, String news_id,String postType,
                                         String messageId) {
        Log.d(TAG, "sendChatmessageNotification: building a chatmessage notification");
//        Intent notificationIntent = new Intent(this, MainActivity.class);
//        if(TestActivity.isAppRunning){
//            //Some action
//        }else{
//            //Show notification as usual
//        }

//        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

//        final PendingIntent pendingIntent = PendingIntent.getActivity(this,
//                0 /* Request code */, notificationIntent,
//                PendingIntent.FLAG_ONE_SHOT);

        //You should use an actual ID instead
        int notificationId = new Random().nextInt(60000);



        Intent likeIntent = new Intent(this, PostDetailActivity.class);
        likeIntent.putExtra(getString(R.string.calling_notification),news_id);
        likeIntent.putExtra(getString(R.string.data_post_id),news_id);
        likeIntent.putExtra(getString(R.string.data_post_type),postType);
        final PendingIntent likePendingIntent = PendingIntent.getActivity(this,
                notificationId+1,likeIntent,PendingIntent.FLAG_ONE_SHOT);


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels();
        }

            NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
//                        .setLargeIcon(bitmap)
                        .setSmallIcon(R.mipmap.ic_launcher)
//                        .setContentTitle(remoteMessage.getData().get("title"))
//                        .setStyle(new NotificationCompat.BigPictureStyle()
//                                .setSummaryText(remoteMessage.getData().get("message"))
//                                .bigPicture(bitmap))/*Notification with Image*/
//                        .setContentText(remoteMessage.getData().get("message"))
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setSound(defaultSoundUri)
                        .addAction(R.drawable.ic_home,
                                getString(R.string.notification_details),likePendingIntent)
                        .setContentIntent(likePendingIntent);

        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_notifications)
                .setContentTitle(title)
                .setContentText(message)
                .setContentInfo("Info");

        notificationManager.notify(notificationId, notificationBuilder.build());

    }


    private int buildNotificationId(String id) {
        Log.d(TAG, "buildNotificationId: building a notification id.");

        int notificationId = 0;
        for (int i = 0; i < 9; i++) {
            notificationId = notificationId + id.charAt(0);
        }
        Log.d(TAG, "buildNotificationId: id: " + id);
        Log.d(TAG, "buildNotificationId: notification id:" + notificationId);
        return notificationId;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels(){
        CharSequence adminChannelName = getString(R.string.notifications_admin_channel_name);
        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

}