package com.iit.csitapp.utils;

public class NavigationTabs {
    public static final int HOME = 0;
    public static final int QNA= 1;
    public static final int SYLLABUS = 2;
    public static final int NOTES = 3;
    public static final int POSTS = 4;
}
