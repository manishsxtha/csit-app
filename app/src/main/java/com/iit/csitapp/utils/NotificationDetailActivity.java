package com.iit.csitapp.utils;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;

import com.iit.csitapp.R;

public class NotificationDetailActivity extends AppCompatActivity {

    private Intent in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_detail);

        in= getIntent();

        if (in.hasExtra("syllabus")){
            String filePath = in.getStringExtra("syllabus");
            Toast.makeText(this, filePath, Toast.LENGTH_SHORT).show();
        }

    }
}
