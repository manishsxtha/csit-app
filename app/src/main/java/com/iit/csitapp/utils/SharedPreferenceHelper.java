package com.iit.csitapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.iit.csitapp.models.User;


public class SharedPreferenceHelper {
    private static final String CATEGORYKEY = "categorykey";
    private static final String SHARE_KEY_SYLL_FORMAT = "format";
    private static SharedPreferenceHelper instance = null;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;
    private static final String SHARE_KEY_USERTYPE = "userType";
    private static String SHARE_USER_INFO = "userinfo";
    private static String SHARE_KEY_NAME = "name";
    private static String SHARE_KEY_EMAIL = "email";
    private static String SHARE_KEY_AVATA = "avata";
    private static String SHARE_KEY_UID = "uid";

    private static String SHARE_KEY_SEMESTER= "sem";

    public static String SHARE_KEY_NEWS_SIZE = "news";
    public static String SHARE_KEY_EVENT_SIZE = "events";

    private static String SHARE_KEY_SUBJECT_SIZE = "subject_size";
    private static String SHARE_KEY_SLIDER_SIZE = "slider_size";

    private static String SHARE_KEY_NOTES_SIZE = "notes_size";
    private static String SHARE_KEY_SYLLABUS_SIZE = "syllabus_size";
    private static String SHARE_KEY_PROJECT_SIZE = "project_size";

    //theme color
    public static final String SHARE_KEY_THEME_COLOR = "theme_color";

    //notification broadcast
    public static final String SHARE_KEY_NOTIFY_NEWS = "notify_news";
    public static final String SHARE_KEY_NOTIFY_EVENT = "notify_event";

    //first launch
    public static final String SHARE_KEY_FIRST_LAUNCH = "first_launch";

    public boolean getFirstLaunch() {
        return preferences.getBoolean(SHARE_KEY_FIRST_LAUNCH, true);
    }

    public void setFirstLaunch(boolean isTrue) {
        editor.putBoolean(SHARE_KEY_FIRST_LAUNCH, isTrue);
        editor.apply();
    }


    public boolean getNotifyNews() {
        return preferences.getBoolean(SHARE_KEY_NOTIFY_NEWS, true);
    }

    public void setNotifyNews(boolean isNotify) {
        editor.putBoolean(SHARE_KEY_NOTIFY_NEWS, isNotify);
        editor.apply();
    }

    public boolean getNotifyevent() {
        return preferences.getBoolean(SHARE_KEY_NOTIFY_EVENT, true);
    }

    public void setNotifyevent(boolean isNotify) {
        editor.putBoolean(SHARE_KEY_NOTIFY_EVENT, isNotify);
        editor.apply();
    }


    public int getThemeColor() {
        return preferences.getInt(SHARE_KEY_THEME_COLOR, 0);
    }

    public void setThemeColor(int color) {
        editor.putInt(SHARE_KEY_THEME_COLOR, color);
        editor.apply();
    }

    public int getShareKeySyllabusSize() {
        return preferences.getInt(SHARE_KEY_SYLLABUS_SIZE, 0);
    }

    public void setShareKeySyllabusSize(int shareKeySyllabusSize) {
        editor.putInt(SHARE_KEY_SYLLABUS_SIZE, shareKeySyllabusSize);
        editor.apply();
    }

    public int getShareKeyNotesSize() {
        return preferences.getInt(SHARE_KEY_NOTES_SIZE, 0);
    }

    public void setShareKeyNotesSize(int shareKeyNotesSize) {
        editor.putInt(SHARE_KEY_NOTES_SIZE, shareKeyNotesSize);
        editor.apply();
    }

    public void setShareKeyEventSize(int shareKeyEventSize) {
        editor.putInt(SHARE_KEY_EVENT_SIZE, shareKeyEventSize);
        editor.apply();
    }

    public int getShareKeyEventSize() {
        return preferences.getInt(SHARE_KEY_EVENT_SIZE, 0);
    }

    public void setShareKeyNewsSize(int shareKeyNewsSize) {
        editor.putInt(SHARE_KEY_NEWS_SIZE, shareKeyNewsSize);
        editor.apply();
    }

    public int getShareKeyNewsSize() {
        return preferences.getInt(SHARE_KEY_NEWS_SIZE, 0);
    }

    public int getShareKeySubjectSize() {
        return preferences.getInt(SHARE_KEY_SUBJECT_SIZE, 0);
    }

    public void setShareKeySubjectSize(int shareKeySubjectSize) {
        editor.putInt(SHARE_KEY_SUBJECT_SIZE, shareKeySubjectSize);
        editor.apply();
    }

    public void setShareKeySliderSize(int shareKeySliderSize) {
        editor.putInt(SHARE_KEY_SLIDER_SIZE, shareKeySliderSize);
        editor.apply();
    }

    public int getShareKeySliderSize() {
        return preferences.getInt(SHARE_KEY_SLIDER_SIZE, 0);
    }

    public SharedPreferenceHelper() {
    }

    public static SharedPreferenceHelper getInstance(Context context) {
//        if (instance == null) {
        instance = new SharedPreferenceHelper();
        preferences = context.getSharedPreferences(SHARE_USER_INFO, Context.MODE_PRIVATE);
        editor = preferences.edit();
//        }
        return instance;
    }

    public void saveUserInfo(User user) {
        editor.putString(SHARE_KEY_UID, user.getUser_id());
        editor.putString(SHARE_KEY_NAME, user.getUsername());
        editor.putString(SHARE_KEY_EMAIL, user.getEmail());
        editor.putString(SHARE_KEY_AVATA, user.getAvatar_img_link());
        editor.putLong(SHARE_KEY_USERTYPE, user.getUser_type());
        editor.apply();
        Log.d("user", "saveUserInfo: " + getUserInfo().getAvatar_img_link());
    }

    public void saveUserType(int userType) {
        editor.putInt(SHARE_KEY_USERTYPE, userType);
        editor.apply();
    }

    public long getUserType() {
        return preferences.getLong(SHARE_KEY_USERTYPE, 4);
    }

    public User getUserInfo() {
        String user_id = preferences.getString(SHARE_KEY_UID, "default");
        String userName = preferences.getString(SHARE_KEY_NAME, "");
        String avatar = preferences.getString(SHARE_KEY_AVATA, "default");
        String email = preferences.getString(SHARE_KEY_EMAIL, "");
        long userType = preferences.getLong(SHARE_KEY_USERTYPE, UserTypes.NORMAL);


        return new User(user_id, userName, avatar, userType, email);
    }

    public String getUID() {
        return preferences.getString(SHARE_KEY_UID, "");
    }

    public void setAvatar(String avatar_img_link) {
        editor.putString(SHARE_KEY_AVATA, avatar_img_link);
        editor.apply();

    }

    public void setSemester(int sem){
        editor.putInt(SHARE_KEY_SEMESTER, sem);
        editor.apply();
    }

    public String getSemester(){
        return preferences.getInt(SHARE_KEY_SEMESTER,1)+"";
    }

//


    public void setSyllabusFormat(int format){
        editor.putInt(SHARE_KEY_SYLL_FORMAT, format);
        editor.apply();
    }

    public int getSyllabusFormat(){

        return preferences.getInt(SHARE_KEY_SYLL_FORMAT,1);
    }
}
