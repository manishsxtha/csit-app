package com.iit.csitapp.utils;

public class UserTypes {
    public static final int ADMIN = 0;
    public static final int PREMIUM = 1;
    public static final int NORMAL = 2;

    public static final String STR_ADMN = "Admin";
    public static final String STR_PREMIUM = "Premium";
    public static final String STR_NORMAL = "Normal User";
}
